var React = require('react');
var ReactDOM = require('react-dom');
var Parse = require('parse').Parse;
var ParseReact = require('parse-react');

Parse.initialize("6yuex2nX7Fy2TQ5ZQkZGKH9XV0HWTCvCVdTz49Qf","bgEr4WhGnFARrlX3uIfMc5XkEsxTVdVcW7MqWmrU");

var mainPageMixin = require('./mixins/main-page-mixin.js');

var NavTopBar = require('./components/nav-topbar.js');
var NavSideBar = require('./components/nav-sidebar.js');
var PageContent = require('./components/page-content.js');

var ClassList = require('./components/class-list.js');
var CampusPage = require('./views/campus.js');

var NavItem = require('react-bootstrap').NavItem;
var Alert = require('react-bootstrap').Alert;
var Modal = require('react-bootstrap').Modal;
var Button = require('react-bootstrap').Button;

var MainCampusPage = React.createClass({

  mixins: [mainPageMixin],
  
  dispName : "Sprach",
  parseClassName: "Language",
  components: [
    { description: "Campus-Liste", id: "campusList" },
    { description: "Campus-Name", id: "CampusName" },
    { description: "Campus-Bereich", id: "campusArea" },
    { description: "Campus-Stockwerke", id: "campusFloors" },
  ],
  
  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});
    
    if(this.state.createClass == true) {
      this.setState({createClass: false});
      this.createLanguage();
    }
    if(this.state.changeClass == true) {
      this.setState({changeClass: false});
      this.changeLanguageElements();
    }
  },
  
  onLanguageElementTypeSelected(index) {
    this.setState({
      changes: undefined,
      
      editClassDescription: " editieren",
      updatePage: true,
    });
  },
  
  onCampusCreateClick() {
    this.setState({
      language: undefined,
      languageName: undefined,
      languageAbbreviation: undefined,
      
      editClassDescription: " hinzufügen",
      updatePage: true,
    });
  },
  
  createLanguage() {
    ParseReact.Mutation.Create("Language", {
      name: this.state.languageNameChanges,
      abbreviation: this.state.languageAbbreviationChanges,
    }).dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },
  
  changeLanguageElements() {
    //for each element
      //for each language change of that element
        //create a change entry for that language
      //set the language changes via parse react
  },
  
  handleSaveClick() {
    // this.setState({
      // showAlert: true,
      // alertStyle: "danger",
      // alertDismissAfter: undefined,
      // alertMessage: 
        // (this.state.hasOwnProperty("campusAreaChanges") ? "campusAreaChanges: " + JSON.stringify(this.state.campusAreaChanges) : ""),
      // updatePage: false,
    // });
    // return;
  
    if(this.state.editClassDescription == " hinzufügen") {
      if(this.state.languageName == undefined && this.state.languageNameChanges == undefined) {
        this.setState({
          showModal: true,
          modalTitle: this.dispName + "-Name fehlt",
          modalText: "Ohne " + this.dispName + "-Name - ohne mich.",
          showSecondButton: false
        });
        return;
      }
      if(this.state.languageAbbreviation == undefined && this.state.languageAbbreviationChanges == undefined) {
        this.setState({
          showModal: true,
          modalTitle: this.dispName + "-Abkürzung fehlt",
          modalText: "Ohne " + this.dispName + "-Abkürzung - ohne mich.",
          showSecondButton: false
        });
        return;
      }
      this.setState({ createClass: true });
    } else if(this.state.editClassDescription == " editieren") {
      this.setState({ changeClass: true });
    }
    
    this.alertBeforeMutationDispatch("Speichern...");
  },
  
  render() {
    return (
      <div className="LocalizationPage">
        <NavTopBar
          onSaveClick={this.handleSaveClick}
          onCancelClick={this.handleCancelClick}
          onLogoutClick={this.handleLogoutClick}
        />
        <div style={ {position: "fixed", top:"56px", bottom:0, overflowY:"scroll" } } >
          <ClassList
            ref="classList"
            id={this.components[0].id}
            displayName={this.dispName}
            columnName="Campus-Name"
            parseClassName={this.parseClassName}
            includes="name"
            onClassChecked={this.onClassesSelected}
            onClassClick={this.onCampusClicked} 
            onAddClick={this.onCampusCreateClick}
            onDeleteClick={this.onDeleteClick.bind(this,"Campusse")}
          />
          {
            this.state.editClassDescription ?
              <CampusPage
                header={this.dispName + this.state.editClassDescription}
                components={this.components}
                campus={this.state.campus}
                visibility={this.state.campusVisibility}
                campusName={this.state.campusName}
                geoPoints={this.state.campusArea}
                onChange={this.handleChange}
                updateCampusPage={this.state.updatePage}
              />
            : undefined
          }
          {
            this.state.showAlert ?
              <Alert 
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={{ position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" }}
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </div>
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton ?
                <Button bsStyle="primary" onClick={this.execute.bind(this,"Campus")} >
                  {this.state.secondButtonText}
                </Button>
              : undefined
            }
            <Button onClick={this.closeModal}>
              { this.state.firstButtonText ? this.state.firstButtonText : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

ReactDOM.render(
  <MainCampusPage />,
  document.getElementById('container')
);