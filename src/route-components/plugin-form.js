import React from 'react';
import {Parse} from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import PluginPage from '../views/plugin';
import PluginSettings from '../views/pluginSettings';

import {
    Panel, NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import {LinkContainer} from 'react-router-bootstrap';

var PluginForm = React.createClass({

    mixins: [formMixin, ParseReact.Mixin],

    getDefaultProps() {
        return {
            parseClassName: "Plugin",
            dispName: "Plugin",
            classRoute: "/plugins",

            components: [
                {description: "Plugin", id: "pluginName"},
                {description: "Konfiguration", id: "pluginSettings"},
            ],
        }
    },

    observe(props, state) {
        if (this.props.params.id != "new") {
            return {
                plugin: (new Parse.Query(this.props.parseClassName))
                    .equalTo("objectId", this.props.params.id)
            };
        } else {
            return;
        }
    },

    componentWillReceiveProps(nextProps) {
        if (nextProps.location.pathname != this.props.location.pathname)
            this.setState({updatePage: true});
    },

    componentDidMount() {
        if (this.props.params.id == "new") this.setDefaultPluginValues();
    },

    componentDidUpdate() {
        if (this.state.updatePage == true)
            this.setState({updatePage: false});
        if (this.state.updatePluginPage == true)
            this.setState({updatePluginPage: false});

        if (this.state.createClass == true) {
            this.setState({createClass: false});
            this.createPlugin();
        }
        if (this.state.changeClass == true) {
            this.setState({changeClass: false});
            this.changePlugin();
        }

        if (this.state.initializeClassPageData == true
            && this.props.params.id != "new"
            && this.pendingQueries().length == 0
        )
            this.onPluginFetched(this.data.plugin[0]);
    },

    onPluginFetched(plugin) {
        this.setState({
            plugin: plugin,
            pluginVisibility: plugin.aktiv,
            pluginName: plugin.name,


            initializeClassPageData: false,
            updatePage: true,
            updatePluginPage: true,


            pluginFile: plugin.file,
            delete: false,
        });
    },

    setDefaultPluginValues() {
        this.setState({
            plugin: undefined,
            pluginVisibility: false,
            pluginName: undefined,

            updatePage: true,
            updatePluginPage: true,

            pluginFile: undefined,
            delete: false,
        });
    },

    createPointerAttributes(batch) {
        var pluginNameMutation = this.toLocalizedStringCreateMutation(this.state.pluginName, this.state.pluginNameChanges);
        if (pluginNameMutation)
            pluginNameMutation.dispatch({batch: batch})
                .then(function (localizedString) {
                    this.setState({pluginNameChanges: localizedString});
                }.bind(this));
    },

    createPlugin() {
        ParseReact.Mutation.Create(this.props.parseClassName, {
            aktiv: this.state.visibilityChanges ? this.state.visibilityChanges : false,
            name: this.state.pluginNameChanges,
            file: this.state.pluginFileChanges,
        }).dispatch().then(this.mutationSaveSuccess, this.mutationSaveError);
    },

    changePlugin() {
        var changes = {};

        if (this.state.hasOwnProperty("visibilityChanges")) changes.aktiv = this.state.visibilityChanges;
        if (this.state.pluginNameChanges) {
        }
        if (this.state.pluginNameChanges && this.state.pluginNameChanges.objectId) changes.name = this.state.pluginNameChanges;

        ParseReact.Mutation.Set(this.state.plugin, changes).dispatch().then(this.mutationSaveSuccess, this.mutationSaveError);
    },
    handleSaveFile() {
        console.log("File: " + this.state.pluginNameChanges + " save start");

        var file = new Parse.File(this.state.pluginNameChanges, this.state.pluginFileChanges, 'application/zip');
        var that = this;

        return file.save().then(function () {
            console.log("File: " + that.state.pluginNameChanges + " saved");

            that.state.pluginFile = file;
            that.state.pluginFileChanges = file;
            //that.handleSavePlugin();
            that.createPlugin();
            //return ParseReact.Mutation.Set(that.state.plugin, {'file': file}).dispatch().then(that.mutationSaveSuccess,that.mutationSaveError);
        }, function (error) {
            console.log(error);
        });

    },
    handleSaveClick() {
        this.closeModal();
        if (this.props.params.id == "new") {
            if (this.state.filename == undefined && this.state.pluginFileChanges == undefined) {
                this.setState({
                    showModal: true,
                    modalTitle: this.props.dispName + "-Pluginfile fehlt",
                    modalText: "Ohne " + this.props.dispName + "-File - ohne mich.",
                    showSecondButton: false
                });
                return;
            }
            this.handleSaveFile();
        }
        else {
            if (this.state.delete != undefined && this.state.delete == true) {
                this.state.plugin.destroy();
                return;
            }
            else if (this.state.pluginName == undefined && this.state.pluginNameChanges == undefined) {
                this.setState({
                    showModal: true,
                    modalTitle: this.props.dispName + "-Name fehlt",
                    modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
                    showSecondButton: false
                });
                return;
            }
            this.handleSavePlugin();
        }
    },
    handleSavePlugin()
    {
        this.alertBeforeMutationDispatch("Speichern...");

        var batch = new ParseReact.Mutation.Batch();
        this.createPointerAttributes(batch);

        if (this.state.plugin == undefined) {
            if (batch.getNumberOfRequests() != 0) {
                batch.dispatch().then(function () {
                    this.setState({createClass: true});
                }.bind(this), this.mutationSaveError);
            } else {
                this.setState({createClass: true});
            }
        } else {
            var pluginNameMutation;

            if (this.state.pluginNameSet != true)
                pluginNameMutation = this.toLocalizedStringSetMutation(this.state.pluginName, this.state.pluginNameChanges);
            if (pluginNameMutation != undefined)
                pluginNameMutation.dispatch({batch: batch}).then(function () {
                    this.setState({pluginNameSet: true});
                }.bind(this));

            if (batch.getNumberOfRequests() != 0) {
                batch.dispatch().then(function () {
                    this.setState({changeClass: true});
                }.bind(this), this.mutationSaveError);
            } else {
                this.setState({changeClass: true});
            }
        }
    },

    render() {
        var sidebarItems = this.props.components.map(function (sidebarItem) {
            return (
                <NavItem
                    key={sidebarItem.id}
                    href={"#" + sidebarItem.id}
                    className="sidebar-item"
                >
                    {sidebarItem.description}
                </NavItem>
            );
        });

        return (
            <div className="PluginPage">
                <NavSideBar>
                    {sidebarItems}
                </NavSideBar>
                <PageContent>
                    <ButtonToolbar style={ {marginBottom: "10px"} }>
                        <Button bsStyle='primary' onClick={this.handleSaveClick}>
                            <Glyphicon glyph="cloud-upload"/> Speichern und zurück zur Liste
                        </Button>
                        <LinkContainer to={ {pathname: this.props.classRoute} }>
                            <Button bsStyle='default'>
                                <Glyphicon glyph="arrow-left"/> Zurück zur Liste
                            </Button>
                        </LinkContainer>
                    </ButtonToolbar>
                    <PluginPage
                        header={
                            this.props.dispName + (this.props.params.id == "new"
                                ? " hinzufügen"
                                : " editieren")
                        }
                        components={this.props.components}
                        plugin={this.state.plugin}
                        pluginVisibility={this.state.pluginVisibility}
                        pluginFile={this.state.pluginFile}
                        pluginName={this.state.pluginName}
                        onChange={this.handleChange}
                        updatePluginPage={this.state.updatePluginPage}
                    />
                    {
                        this.state.showAlert ?
                            <Alert
                                bsStyle={this.state.alertStyle}
                                onDismiss={this.handleAlertDismiss}
                                dismissAfter={this.state.alertDismissAfter}
                                style={{position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%"}}
                            >
                                {this.state.alertMessage}
                            </Alert>
                            : undefined
                    }
                    {
                        <PluginSettings
                            header={
                                this.state.pluginName +" anpassen"
                            }
                            components={this.props.components}
                            plugin={this.state.plugin}
                            pluginVisibility={this.state.pluginVisibility}
                            pluginFile={this.state.pluginFile}
                            pluginName={this.state.pluginName}
                            onChange={this.handleChange}
                            updatePluginPage={this.state.updatePluginPage}
                        />
                    }
                </PageContent>
                <Modal show={this.state.showModal} onHide={this.closeModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>
                            {this.state.modalTitle}
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.state.modalText}
                    </Modal.Body>
                    <Modal.Footer>
                        {
                            this.state.showSecondButton &&
                            <Button bsStyle="primary" onClick={this.handleSaveClick}>
                                Nochmal versuchen
                            </Button>
                        }
                        <Button onClick={this.closeModal}>
                            { this.state.showSecondButton ? "Abbrechen" : "OK" }
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }

});

module.exports = PluginForm;
