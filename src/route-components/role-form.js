import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import RolePage from '../views/role';

import {
  NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var RoleForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "_Role",
      userClassName: "User",
      dispName : "Rolle",
      classRoute: "/roles",

      components: [
        { description: "Rollen-Name", id: "roleName" },
      ],
    }
  },

  observe(props,state) {
    if(props.params.id != "new") {
      return {
        role: (new Parse.Query(props.parseClassName))
                  .equalTo("objectId",props.params.id)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultRoleValues();
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({ updatePage: false });

    if(this.state.createClass == true)
      this.setState({ createClass: false });

    if(this.state.changeClass == true)
      this.setState({ changeClass: false });


    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onRoleFetched(this.data.role[0]);
  },

  onRoleFetched(role) {
    this.setState({
      role: role,
      roleName: role.name,
      roleUsers: role.users,
      roleRoles: role.roles,

      initializeClassPageData: false,
      updatePage: true,
    });
  },

  setDefaultRoleValues() {
    this.setState({
      role: undefined,
      roleName: undefined,
      roleUsers: undefined,
      roleRoles: undefined,

      updatePage: true,
    });
  },

  createRole() {
    if(this.state.role == undefined) {
      var ACL = new Parse.ACL();
      ACL.setPublicReadAccess(true);
      ACL.setRoleReadAccess("Admin",true);
      ACL.setRoleWriteAccess("Admin",true);
      ParseReact.Mutation.Create(this.props.parseClassName, {
        name: this.state.roleNameChanges,
        ACL: ACL
      }).dispatch().then(function(role) {
          this.setState({ role: role },this.mutateRelationsAfterCreation);
        }.bind(this),this.mutationSaveError);
    } else this.mutateRelationsAfterCreation();
  },

  mutateUsersRelation(batch) {
    if(
      this.state.roleUsersChanges.usersToAdd
      && this.state.usersAdded != true
    ) {
      var usersToAdd = this.toPointerArray(this.state.roleUsersChanges.usersToAdd, this.props.userClassName)
      ParseReact.Mutation.AddRelation(this.state.role,"users",usersToAdd)
      .dispatch({ batch: batch })
      .then(function() {
        this.setState({ usersAdded: true });
      }.bind(this));
    } else if(
      this.state.roleUsersChanges.usersToRemove
      && this.state.usersRemoved != true
    ) {
      var usersToRemove = this.toPointerArray(this.state.roleUsersChanges.usersToRemove, this.props.userClassName)
      ParseReact.Mutation.RemoveRelation(this.state.role,"users",usersToRemove)
      .dispatch({ batch: batch })
      .then(function() {
        this.setState({ usersRemoved: true });
      }.bind(this));
    }
  },

  mutateRolesRelation(batch) {
    if(
      this.state.roleRolesChanges.rolesToAdd
      && this.state.rolesAdded != true
    ) {
      var rolesToAdd = this.toPointerArray(this.state.roleRolesChanges.rolesToAdd, this.props.parseClassName)
      ParseReact.Mutation.AddRelation(this.state.role,"roles",rolesToAdd)
      .dispatch({ batch: batch })
      .then(function() {
        this.setState({ rolesAdded: true });
      }.bind(this));
    } else if(
      this.state.roleRolesChanges.rolesToRemove
      && this.state.rolesRemoved != true
    ) {
      var rolesToRemove = this.toPointerArray(this.state.roleRolesChanges.rolesToRemove, this.props.parseClassName)
      ParseReact.Mutation.RemoveRelation(this.state.role,"roles",rolesToRemove)
      .dispatch({ batch: batch })
      .then(function() {
        this.setState({ rolesRemoved: true });
      }.bind(this));
    }
  },

  mutateRelationsAfterCreation() {
    var batch = new ParseReact.Mutation.Batch();

    this.mutateRelations(batch);

    if(batch.getNumberOfRequests() != 0) {
      batch.dispatch().then(this.mutationSaveSuccess, this.mutationSaveError);
    } else {
      this.mutationSaveSuccess();
    }
  },

  mutateRelations(batch) {
    if(
      this.state.roleUsersChanges &&
      (this.state.roleUsersChanges.usersToAdd || this.state.roleUsersChanges.usersToRemove)
    ) {
      this.mutateUsersRelation(batch);
    }

    if(
      this.state.roleRolesChanges &&
      (this.state.roleRolesChanges.rolesToAdd || this.state.roleRolesChanges.rolesToRemove)
    ) {
      this.mutateRolesRelation(batch);
    }
  },

  changeRole() {
    var batch = new ParseReact.Mutation.Batch();
    var changes = {};

    if(this.state.roleChanged != true) {
      if(this.state.roleNameChanges) changes.name = this.state.roleNameChanges;

      ParseReact.Mutation.Set(this.state.role,changes)
      .dispatch({ batch: batch })
      .then(function() {
        this.setState({ roleChanged: true });
      }.bind(this));
    }

    this.mutateRelations(batch);

    if(batch.getNumberOfRequests() != 0) {
      batch.dispatch().then(this.mutationSaveSuccess, this.mutationSaveError);
    } else {
      this.mutationSaveSuccess();
    }
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.roleName == undefined && this.state.roleNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.props.dispName + "-Name fehlt",
        modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    if(this.state.role == undefined) {
      this.setState({ createClass: true }, this.createRole);
    } else {
      this.setState({ changeClass: true }, this.changeRole);
    }
  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="RolePage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <div>
            <ButtonToolbar style={ { marginBottom: "10px" } } >
              <Button bsStyle='primary' onClick={this.handleSaveClick} >
                <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
              </Button>
              <LinkContainer to={ { pathname: this.props.classRoute } } >
                <Button bsStyle='default' >
                  <Glyphicon glyph="arrow-left" /> Zurück zur Liste
                </Button>
              </LinkContainer>
            </ButtonToolbar>
            <RolePage
              header={
                this.props.dispName + (this.props.params.id == "new"
                ? " hinzufügen"
                : " editieren")
              }
              components={this.props.components}
              roleName={this.state.roleName}
              roleUsers={this.state.roleUsers}
              roleRoles={this.state.roleRoles}
              onChange={this.handleChange}
              updateRolePage={this.state.updatePage}
            />
          </div>
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.handleSaveClick}>
              Nochmal versuchen
            </Button>
            <Button onClick={this.closeModal}>
              Abbrechen
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports = RoleForm;
