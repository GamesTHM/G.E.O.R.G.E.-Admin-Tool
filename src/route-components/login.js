import React from 'react';
import {Parse} from 'parse';

import { Panel, Row, Col, Input, Button, Alert } from 'react-bootstrap';

var Login = React.createClass({

  contextTypes: {
    router: React.PropTypes.object.isRequired
  },

  getInitialState() {
    return {
      userName: undefined,
      password: undefined
    };
  },

  shouldComponentUpdate(nextProps,nextState) {
    return nextState.alertMessage != this.state.alertMessage;
  },

  onUserNameChange(e) {
    this.setState({
      userName: e.target.value
    });
  },

  onPasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  },

  onLoginClick(e) {
    var me = this;

    this.setState({ alertMessage: undefined });
    Parse.User.logIn(this.state.userName,this.state.password,{
      success: function(user) {
        const { location } = me.props;

        if (location.state && location.state.nextPathname) {
          me.context.router.replace(location.state.nextPathname)
        } else {
          me.context.router.replace('/')
        }
      },
      error: function(user,error) {
        me.setState({ alertMessage: error.message });
      }
    });
  },

  onKeyPress(e) {
    if(e.which == 13 || e.keyCode == 13) this.onLoginClick();
  },

  render() {
    return (
      <Col
        md={4}
        mdOffset={4}
        style={
          {
            position: "absolute",
            top: "50%",
            marginTop: "-108px"
          }
        }
      >
        <Panel
          bsStyle='primary'
          header='Login'
          style={ { margin: "0 auto 0 auto" } }
        >
          <Input
            type="text"
            label="Benutzername"
            onChange={this.onUserNameChange}
          />
          <Input
            type="password"
            label="Passwort"
            onChange={this.onPasswordChange}
            onKeyPress={this.onKeyPress}
          />
          <Button
            bsStyle="primary"
            bsSize="large"
            block
            onClick={this.onLoginClick}
          >
            Login
          </Button>
          {
            this.state.alertMessage ?
              <Alert bsStyle='danger' style={ { marginTop: "10px", marginBottom: "0" } } >
                <b>{this.state.alertMessage}</b>
              </Alert>
            : undefined
          }
        </Panel>
      </Col>
    );
  },
});

module.exports = Login;
