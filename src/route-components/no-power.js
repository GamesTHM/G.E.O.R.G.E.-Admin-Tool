import React from 'react';

var NoPower = React.createClass({
  render() {
    return (
      <img
        src="/img/no-power.jpg"
        style={
          {
            display: "block",
            margin: "100px auto 0 auto"
          }
        }
      />
    );
  },
});

module.exports = NoPower;
