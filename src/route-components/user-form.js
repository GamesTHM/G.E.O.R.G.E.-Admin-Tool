import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import UserPage from '../views/user';

import {
  NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var UserForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "_User",
      dispName : "Benutzer",
      classRoute: "/users",

      components: [
        { description: "Benutzername", id: "userName" },
      ],
    }
  },

  observe(props,state) {
    if(props.params.id != "new") {
      return {
        user: (new Parse.Query(props.parseClassName))
                  .equalTo("objectId",props.params.id)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultUserValues();
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({ updatePage: false });

    if(this.state.createClass == true)
      this.setState({ createClass: false });

    if(this.state.changeClass == true)
      this.setState({ changeClass: false });


    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onUserFetched(this.data.user[0]);
  },

  onUserFetched(user) {
    this.setState({
      user: user,
      userName: user.username,

      initializeClassPageData: false,
      updatePage: true,
    });
  },

  setDefaultUserValues() {
    this.setState({
      user: undefined,
      userName: undefined,

      updatePage: true,
    });
  },

  createUser() {
    var sessionToken = Parse.User.current()
    ? Parse.User.current().getSessionToken()
    : undefined;

    Parse.User.signUp(
      this.state.userNameChanges,
      this.state.passwordChanges
    ).then(function(user) {
      if(sessionToken) Parse.User.become(sessionToken);
      this.mutationSaveSuccess();
    }.bind(this),this.mutationSaveError);
  },

  changeUser() {
    var changes = {};

    if(this.state.userNameChanges) changes.username = this.state.userNameChanges;
    if(this.state.passwordChanges) changes.password = this.state.passwordChanges;

    Parse.Cloud.run("changeUser",{
      userId: this.state.user.objectId,
      changes: changes
    }).then(this.mutationSaveSuccess,this.mutationSaveError);

  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.user == undefined &&
      ( this.state.userNameChanges == undefined ||
        this.state.passwordChanges == undefined )
    ) {
      this.setState({
        showModal: true,
        modalTitle: "Benutzername und/oder Passwort fehlt",
        modalText: "Beim Anlegen eines neuen Benutzers muss Benutzername und Passwort angegeben werden",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    if(this.state.user == undefined) {
      this.setState({ createClass: true }, this.createUser);
    } else {
      this.setState({ changeClass: true }, this.changeUser);
    }
  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="UserPage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <div>
            <ButtonToolbar style={ { marginBottom: "10px" } } >
              <Button bsStyle='primary' onClick={this.handleSaveClick} >
                <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
              </Button>
              <LinkContainer to={ { pathname: this.props.classRoute } } >
                <Button bsStyle='default' >
                  <Glyphicon glyph="arrow-left" /> Zurück zur Liste
                </Button>
              </LinkContainer>
            </ButtonToolbar>
            <UserPage
              header={
                this.props.dispName + (this.props.params.id == "new"
                ? " hinzufügen"
                : " editieren")
              }
              components={this.props.components}
              userName={this.state.userName}
              onChange={this.handleChange}
              updateUserPage={this.state.updatePage}
            />
          </div>
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports = UserForm;
