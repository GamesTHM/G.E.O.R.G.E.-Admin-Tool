import React from 'react';

import { Parse } from 'parse';
Parse.initialize("george.games.thm.de","N4%-_ACs=sAnZr[2");
Parse.serverURL = "http://george.mnd.thm.de:1337/parse";

import { IndexRoute, Route } from 'react-router';

import App from './app';
import Index from './index';
import Login from './login';
import NoPower from './no-power';
import FactionTable from './faction-table';
import FactionForm from './faction-form';
import CampusTable from './campus-table';
import CampusForm from './campus-form';
import LevelTable from './level-table';
import LevelForm from './level-form';
import PoiTable from './poi-table';
import PoiForm from './poi-form';
import QuestTable from './quest-table';
import QuestForm from './quest-form';
import JobTable from './job-table';
import JobForm from './job-form';
import StoryPageTable from './storypage-table';
import StoryPageForm from './storypage-form';
import RoleTable from './role-table';
import RoleForm from './role-form';
import UserTable from './user-table';
import UserForm from './user-form';
import PluginTable from './plugin-table';
import PluginForm from './plugin-form';

function checkPermitted(roleName,locationPathName) {
  var locationPathNameForLocationAdmin = locationPathName &&
    (locationPathName.indexOf("campusses") != -1 ||
    locationPathName.indexOf("pois") != -1);
  var locationPathNameForQuestAdmin = locationPathName &&
    (locationPathName.indexOf("quests") != -1 ||
    locationPathName.indexOf("jobs") != -1);
  var locationPathNameForAdmin = locationPathName &&
    (locationPathName.indexOf("roles") != -1 ||
    locationPathName.indexOf("users") != -1 ||
    locationPathName.indexOf("factions") != -1 ||
    locationPathName.indexOf("plugins") != -1 ||
    locationPathName.indexOf("images") != -1);
  var permitted = false;

  if(locationPathNameForLocationAdmin) {
    if(roleName == "Location Admin" || roleName == "Admin") permitted = true;
  } else if (locationPathNameForQuestAdmin) {
    if(roleName == "Quest Admin" || roleName == "Admin") permitted = true;
  } else if (locationPathNameForAdmin) {
    if(roleName == "Admin") permitted = true;
  }

  return permitted;
}

function requireAuth(nextState, replace,callback) {
  if( Parse.User.current() == undefined) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    });
    callback();
  } else {
    var roleQuery = new Parse.Query("_Role");

    roleQuery.equalTo("users",Parse.User.current()).find().then(function(roles) {
      var locationPathName = nextState.location.pathname;
      var permitted = false;

      roles.forEach(function(role) {
        permitted = checkPermitted(role.get("name"),locationPathName)
      });

      if(locationPathName && !permitted) {
        replace('/notPermitted');
      }
      callback();
    },function(error){
      callback(error);
    });
  }
}

module.exports = (
  <Route path="/" component={App}>
    <IndexRoute component={Index} />
    <Route path="login" component={Login} />
    <Route path="notPermitted" component={NoPower} />
    <Route path="factions" component={FactionTable} onEnter={requireAuth} />
    <Route path="factions/:id" component={FactionForm} onEnter={requireAuth} />
    <Route path="campusses" component={CampusTable} onEnter={requireAuth} />
    <Route path="campusses/:id" component={CampusForm} onEnter={requireAuth} >
      <IndexRoute component={LevelTable} />
      <Route path="levels/:lid" component={LevelForm} />
    </Route>
    <Route path="pois" component={PoiTable} onEnter={requireAuth} />
    <Route path="pois/:id" component={PoiForm} onEnter={requireAuth} />
    <Route path="quests" component={QuestTable} onEnter={requireAuth} />
    <Route path="quests/:id" component={QuestForm} onEnter={requireAuth} />
    <Route path="jobs" component={JobTable} onEnter={requireAuth} />
    <Route path="jobs/:id" component={JobForm} onEnter={requireAuth} >
      <IndexRoute component={StoryPageTable} />
      <Route path="storypages/:spid" component={StoryPageForm} />
    </Route>
    <Route path="roles" component={RoleTable} onEnter={requireAuth} />
    <Route path="roles/:id" component={RoleForm} onEnter={requireAuth} />
    <Route path="users" component={UserTable} onEnter={requireAuth} />
    <Route path="users/:id" component={UserForm} onEnter={requireAuth} />
    <Route path="plugins" component={PluginTable} onEnter={requireAuth} />
    <Route path="plugins/:id" component={PluginForm} onEnter={requireAuth} />
  </Route>
)
