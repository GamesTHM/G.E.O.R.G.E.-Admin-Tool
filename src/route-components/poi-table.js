import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import tableMixin from '../mixins/table-mixin';

import ClassTable from '../components/class-table';

import {
  Col, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

var PoiTable = React.createClass({

  mixins: [tableMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Location",
      parseAttributeName: "Locations",
      dispName : "POI",
      pluralName: "POIs",
      classRoute: "/pois",
      columnNames: ["Freigeschaltet","Angreifbar","POI-Name","Campus","Stockwerk"],
      id: "poiList"
    }
  },

  observe(props,state) {
    return {
      pois: (new Parse.Query(props.parseClassName))
              .include(["name","Campus.name"])
              .descending("updatedAt"),
      levels: (new Parse.Query("CampusFloorLevel")).include("name")
    };
  },

  componentDidUpdate(){
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.initializeClassTableData == true)
      this.initializeClassTableData();
  },

  createPoiCampusFloorDictionary() {
    var campusFloorDictionary = {};

    for(var i = 0; i < this.data.levels.length; i++) {
      if(this.data.levels[i].Locations != undefined) {
        for (var j = 0; j < this.data.levels[i].Locations.length; j++) {
          campusFloorDictionary[this.data.levels[i].Locations[j].objectId] = this.data.levels[i];
        }
      }
    }

    return campusFloorDictionary;
  },

  preparePoiData() {
    var preparedPoiData = [], poiDataSet = {};
    var poiCampusFloorDict = this.createPoiCampusFloorDictionary();

    this.setState({ poiCampusFloorDict: poiCampusFloorDict });
    for (var i = 0; i < this.data.pois.length; i++) {
      poiDataSet = {};
      for (var attr in this.data.pois[i]) {
        if (this.data.pois[i].hasOwnProperty(attr)) {
          poiDataSet[attr] = this.data.pois[i][attr];
        }
      }
      if(poiCampusFloorDict[this.data.pois[i].objectId]) {
        poiDataSet["CampusFloorLevel"] = {};
        poiDataSet["CampusFloorLevel"]["objectId"] = poiCampusFloorDict[this.data.pois[i].objectId].objectId;
        poiDataSet["CampusFloorLevel"]["name"] = poiCampusFloorDict[this.data.pois[i].objectId].name;
        poiDataSet["CampusFloorLevel"]["parentId"] = this.data.pois[i].Campus
          ? this.data.pois[i].Campus.objectId
          : undefined;
      }
      preparedPoiData.push(poiDataSet);
    }
    return preparedPoiData;
  },

  initializeClassTableData() {
    if(this.pendingQueries().length == 0) {
      var classTableColProps = [
        { name: "visibility", type: "bool" },
        { name: "LocationCaptureData", type: "bool" },
        { name: "name", type: "object" },
        { name: "Campus", type: "pointer", classRoute: "/campusses/:id" },
        { name: "CampusFloorLevel", type: "pointer", classRoute: "/campusses/:pid/levels/:id" },
      ];
      var preparedDataSource = this.preparePoiData();
      var classTableData = this.generateClassTableData(classTableColProps,preparedDataSource);

      this.setState({
        classTableData: classTableData,
        initializeClassTableData: false,
        updatePage: true
      });
    }
  },

  poiDelete() {
    if(this.state.poisRemoved != true) {
      var batch = new ParseReact.Mutation.Batch();
      var classesToRemove = this.state.classesToRemove != undefined
        ? this.state.classesToRemove.slice()
        : this.state.selectedClasses.slice();

      for(var i = 0; i < classesToRemove.length; i++) {
        if( classesToRemove[i] != undefined &&
            this.state.poiCampusFloorDict[classesToRemove[i]] != undefined)
        {
          ParseReact.Mutation.Remove(
            this.state.poiCampusFloorDict[classesToRemove[i]],
            this.props.parseAttributeName,
            { className: this.props.parseClassName, objectId: classesToRemove[i] }
          ).dispatch({ batch: batch })
          .then(function(index) {
            classesToRemove[index] = undefined;
          }.bind(this,i));
        }
      }
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({poisRemoved: true});
          this.deleteSelectedClasses();
        }.bind(this),function() {
          this.setState({ classesToRemove: classesToRemove });
          this.mutationDeleteError(this.state.selectedClasses);
        }.bind(this));
      } else {
        // none of the pois to delete were associated with a campus floor
        this.setState({poisRemoved: true});
        this.deleteSelectedClasses();
      }
    } else {
      // pois were already removed from the campus floor list, but may not be deleted
      this.deleteSelectedClasses();
    }
  },

  render() {
    return (
      <Col md={12} style={ {position: "fixed", top:"56px", bottom:0, overflowY:"scroll" } } >
        <ClassTable
          id={this.props.id}
          classRoute={this.props.classRoute}
          displayName={this.props.dispName}
          columnNames={this.props.columnNames}
          data={this.state.classTableData}
          selectedClasses={this.state.selectedClasses}
          onClassChecked={this.handleClassesSelected}
          onDeleteClick={this.handleDeleteClick}
        />
        {
          this.state.showAlert ?
            <Alert
              bsStyle={this.state.alertStyle}
              onDismiss={this.handleAlertDismiss}
              dismissAfter={this.state.alertDismissAfter}
              style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
            >
              {this.state.alertMessage}
            </Alert>
          : undefined
        }
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.poiDelete}>
              {this.state.secondButtonText}
            </Button>
            <Button onClick={this.closeModal}>
              Abbrechen
            </Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }

});

module.exports = PoiTable;
