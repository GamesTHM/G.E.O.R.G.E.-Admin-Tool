import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import JobPage from '../views/job';

import {
  NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon, Panel
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var JobForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Job",
      dispName : "Job",
      classRoute: "/jobs",

      actionClassName: "Action",
      localizedStringClassName: "LocalizedString",
      parameterClassName: "Parameter",
      jobParamsAttrName: "Parameters",

      components: [
        { description: "Job-Name", id: "jobName" },
        { description: "Job-Abgabe-Parameter", id: "jobParameters" },
        { description: "Job-Abgabe-Aktion", id: "jobAction" },
        { description: "Job-Icon", id: "jobIcon" },
        { description: "Job-Story", id: "jobStory" },
      ],
    }
  },

  observe(props,state) {
    if(props.params.id != "new") {
      return {
        job: (new Parse.Query(props.parseClassName))
                .include(["name","shortDescription","description","icon","jobFinishedAction"])
                .equalTo("objectId",props.params.id)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultJobValues();
    if(this.props.params.spid) location.href="#jobStory";
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.location.pathname != this.props.location.pathname)
      this.setState({ updatePage: true});
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});
    if(this.state.updateJobPage == true)
      this.setState({updateJobPage: false});

    if(this.state.createClass == true) {
      this.setState({createClass: false});
      this.setAttributesAndCreateJob();
    }

    if(this.state.changeClass == true) {
      this.setState({changeClass: false});
      this.setAttributesAndChangeJob();
    }

    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onJobFetched(this.data.job[0]);
  },

  onJobFetched(job) {
    this.setState({
      job: job,
      jobPointGain: job.pointGain,
      jobEnergyGain: job.energyGain,
      jobName: job.name,
      jobShortDescription: job.shortDescription,
      jobDescription: job.description,
      jobParameters: job.Parameters,
      jobFinishedAction: job.jobFinishedAction,
      jobIcon: job.icon,
      jobFinishTimeFrame: {
        startDate: job.finishStartDate,
        endDate: job.finishEndDate,
        compareTimeOnly: job.compareTimeOnly
      },

      initializeClassPageData: false,
      updatePage: true,
      updateJobPage: true
    });
  },

  setDefaultJobValues() {
    this.setState({
      job: undefined,
      jobPointGain: 0,
      jobEnergyGain: 0,
      jobName: undefined,
      jobShortDescription: undefined,
      jobDescription: undefined,
      jobParameters: undefined,
      jobFinishedAction: undefined,
      jobIcon: undefined,
      jobFinishTimeFrame: {
        startDate: undefined,
        endDate: undefined,
        compareTimeOnly: false
      },

      updatePage: true,
      updateJobPage: true
    });
  },

  createPointerAttributes(batch) {
    var jobNameMutation = this.toLocalizedStringCreateMutation(this.state.jobName,this.state.jobNameChanges);

    if(jobNameMutation)
      jobNameMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({
            jobNameChanges: localizedString
          });
        }.bind(this));

    var jobShortDescriptionMutation =
      this.toLocalizedStringCreateMutation(this.state.jobShortDescription,this.state.jobShortDescriptionChanges);

    if(jobShortDescriptionMutation)
      jobShortDescriptionMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({
            jobShortDescriptionChanges: localizedString
          });
        }.bind(this));

    var jobDescriptionMutation =
      this.toLocalizedStringCreateMutation(this.state.jobDescription,this.state.jobDescriptionChanges);

    if(jobDescriptionMutation)
      jobDescriptionMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({
            jobDescriptionChanges: localizedString
          });
        }.bind(this));

    var createActionOrParamAttributes = false;
    var jobFinishedActionMutation = this.toActionCreateMutation(this.state.jobFinishedActionChanges);

    if(jobFinishedActionMutation) {
      createActionOrParamAttributes = true;
      jobFinishedActionMutation.dispatch({batch: batch})
        .then(function(action) {
          this.setState({
            jobFinishedActionChanges: action
          });
        }.bind(this));
    }

    if(this.state.jobParametersChanges) {
      if(this.state.jobParametersCodeUserInfoChanges == undefined) {
        var jobParametersCodeUserInfoMutation =
            this.toLocalizedStringCreateMutation(undefined,this.state.jobParametersChanges.codeUserInfo);

        if(jobParametersCodeUserInfoMutation) {
          createActionOrParamAttributes = true;
          jobParametersCodeUserInfoMutation.dispatch({batch: batch})
            .then(function(localizedString) {
              this.setState({ jobParametersCodeUserInfoChanges: localizedString });
            }.bind(this));
        } else {
          this.setState({ jobParametersCodeUserInfoChanges: this.state.jobParametersChanges.codeUserInfo });
        }
      }

      if(this.state.jobParametersPoiUserInfoChanges == undefined) {
        var jobParametersPoiUserInfoMutation =
            this.toLocalizedStringCreateMutation(undefined,this.state.jobParametersChanges.poiUserInfo);

        if(jobParametersPoiUserInfoMutation) {
          createActionOrParamAttributes = true;
          jobParametersPoiUserInfoMutation.dispatch({batch: batch})
            .then(function(localizedString) {
              this.setState({ jobParametersPoiUserInfoChanges: localizedString });
            }.bind(this));
        } else {
          this.setState({ jobParametersPoiUserInfoChanges: this.state.jobParametersChanges.poiUserInfo });
        }
      }

      if( this.state.jobCodeParameter == undefined && this.state.jobParametersChanges.codeParam == undefined &&
          this.state.jobParametersChanges.correctCode != undefined) {
        var codeIncorrectValueAction;

        if(this.state.jobParametersChanges.codeIncorrectValueAction == undefined) {
          var Action = Parse.Object.extend(this.props.actionClassName);
          codeIncorrectValueAction = new Action();
          codeIncorrectValueAction.id = "jawKUhXGIg";
        }

        var codeUserInfo;

        if(this.state.jobParametersChanges.codeUserInfo == undefined) {
          var LocalizedString = Parse.Object.extend(this.props.localizedStringClassName);

          codeUserInfo = new LocalizedString();
          codeUserInfo.id = "vexrfryI3v";
        }

        ParseReact.Mutation.Create(this.props.parameterClassName,{
          name: "usercode",
          type: 1001,
          LocalizedString: codeUserInfo,
          optional: this.state.jobParametersChanges.isCodeOptional != undefined ? this.state.jobParametersChanges.isCodeOptional : false,
          correctValue: this.state.jobParametersChanges.correctCode,
          incorrectValueAction: codeIncorrectValueAction
        }).dispatch({batch: batch})
          .then(function(parameter) {
            this.setState({ jobCodeParameter: parameter });
          }.bind(this));
      }

      if(this.state.codeIncorrectValueAction == undefined) {
        var jobCodeIncorrectValueActionMutation = this.toActionCreateMutation(this.state.jobParametersChanges.codeIncorrectValueAction);

        if(jobCodeIncorrectValueActionMutation) {
          createActionOrParamAttributes= true;
          jobCodeIncorrectValueActionMutation.dispatch({batch: batch})
            .then(function(action) {
              this.setState({
                codeIncorrectValueAction: action
              });
            }.bind(this));
        }
      }

      if(this.state.poiIncorrectValueAction == undefined) {
        var jobPoiIncorrectValueActionMutation = this.toActionCreateMutation(this.state.jobParametersChanges.poiIncorrectValueAction);

        if(jobPoiIncorrectValueActionMutation) {
          createActionOrParamAttributes = true;
          jobPoiIncorrectValueActionMutation.dispatch({batch: batch})
            .then(function(action) {
              this.setState({
                poiIncorrectValueAction: action
              });
            }.bind(this));
        }
      }
    }
    return createActionOrParamAttributes;
  },

  toActionCreateMutation(actionChanges) {
    if(actionChanges && actionChanges.objectId == undefined && actionChanges.selectedAction == undefined) {
      if(actionChanges.type == 0 && actionChanges.localizedStringChanges != undefined) {
        return (
          ParseReact.Mutation.Create(this.props.actionClassName,{
            information: actionChanges.localizedString && actionChanges.localizedString.objectId ?
              actionChanges.localizedString.objectId : undefined,
            type: 0,
            description: actionChanges.localizedString && actionChanges.localizedString.objectId ?
              actionChanges.localizedString.deu : localizedStringChanges
          })
        );
      } else if(actionChanges.type == 1 && actionChanges.webElement != undefined) {
        return (
          ParseReact.Mutation.Create(this.props.actionClassName,{
            information: undefined,
            type: 1,
            description: undefined
          })
        );
      }
    }
    return undefined;
  },

  setActionInformation(batch, action, actionString) {
    if(this.state[actionString+"Set"] == true) return;

    if(this.state[actionString] || action ) {
      var action = action ? action : this.state[actionString];
      var information = action && action.localizedString ? action.localizedString : this.state[actionString+"Element"];

      if(action.selectedAction) return;

      if(information) {
        ParseReact.Mutation.Set(action,{
          information: information.objectId
        }).dispatch({batch: batch})
          .then(function() {
            this.setState({ [actionString+"Set"]: true });
          }.bind(this));
      }
    }
  },

  setCodeParamAttributes(batch) {
    if(this.state.jobCodeParameterSet == true) return;

    if(this.state.jobCodeParameter || (this.state.jobParametersChanges && this.state.jobParametersChanges.codeParam) ) {
      var codeParam = this.state.jobParametersChanges && this.state.jobParametersChanges.codeParam ?
        this.state.jobParametersChanges.codeParam : this.state.jobCodeParameter;
      var changes = {};

      if(this.state.jobParametersCodeUserInfoChanges) changes.LocalizedString = this.state.jobParametersCodeUserInfoChanges;
      if(this.state.jobParametersChanges) {
        if(this.state.jobParametersChanges.isCodeOptional) changes.optional = this.state.jobParametersChanges.isCodeOptional;
        if(this.state.jobParametersChanges.correctCode) changes.correctValue = this.state.jobParametersChanges.correctCode;
        if(this.state.jobParametersChanges.codeIncorrectValueAction && this.state.jobParametersChanges.codeIncorrectValueAction.selectedAction)
          changes.incorrectValueAction = this.state.jobParametersChanges.codeIncorrectValueAction.selectedAction;
        else if(this.state.codeIncorrectValueAction) changes.incorrectValueAction = this.state.codeIncorrectValueAction;
      }

      ParseReact.Mutation.Set(codeParam,changes)
        .dispatch({batch: batch})
        .then(function() {
          this.setState({ jobCodeParameterSet: true });
        }.bind(this));
    }
  },

  setPoiParamAttributes(batch) {
    if(this.state.jobPoiParameterSet == true) return;

    if(this.state.jobParametersChanges && this.state.jobParametersChanges.poiParam) {
      var changes = {};

      if(this.state.jobParametersPoiUserInfoChanges) changes.LocalizedString = this.state.jobParametersPoiUserInfoChanges;
      if(this.state.jobParametersChanges) {
        if(this.state.jobParametersChanges.isPoiOptional) changes.optional = this.state.jobParametersChanges.isPoiOptional;
        if(this.state.jobParametersChanges.poiIncorrectValueAction && this.state.jobParametersChanges.poiIncorrectValueAction.selectedAction)
          changes.incorrectValueAction = this.state.jobParametersChanges.poiIncorrectValueAction.selectedAction;
        else if(this.state.poiIncorrectValueAction) changes.incorrectValueAction = this.state.poiIncorrectValueAction;
      }

      ParseReact.Mutation.Set(this.state.jobParametersChanges.poiParam,changes)
        .dispatch({batch: batch})
        .then(function() {
          this.setState({ jobPoiParameterSet: true });
        }.bind(this));
    }
  },

  setAttributes(batch) {
    this.setCodeParamAttributes(batch);
    this.setPoiParamAttributes(batch);

    if(this.state.jobParametersChanges) {
      this.setActionInformation(batch,this.state.jobParametersChanges.codeIncorrectValueAction,"codeIncorrectValueAction");
      this.setActionInformation(batch,this.state.jobParametersChanges.poiIncorrectValueAction,"poiIncorrectValueAction");
    }
    this.setActionInformation(batch,this.state.jobFinishedActionChanges,"jobFinishedActionChanges");
  },

  setAttributesAndCreateJob() {
    var batch = new ParseReact.Mutation.Batch();

    this.setAttributes(batch);
    this.createJob(batch);
    batch.dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  createJob(batch) {
    if(this.state.jobCreated != true) {
      var Parameters = [];

      if(this.state.jobCodeParameter && this.state.jobCodeParameter.objectId)
        Parameters.push({objectId: this.state.jobCodeParameter.objectId});
      if(this.state.jobParametersChanges && this.state.jobParametersChanges.poiParam && this.state.jobParametersChanges.poiParam.objectId)
        Parameters.push({objectId: this.state.jobParametersChanges.poiParam.objectId});

      ParseReact.Mutation.Create(this.props.parseClassName, {
        visibility: this.state.hasOwnProperty("visibilityChanges") ? this.state.visibilityChanges : false,
        pointGain: this.state.jobPointGainChanges ? this.state.jobPointGainChanges : 0,
        energyGain: this.state.jobEnergyGainChanges ? this.state.jobEnergyGainChanges : 0,
        finishStartDate: this.state.jobFinishTimeFrameChanges ? this.state.jobFinishTimeFrameChanges.startDate : undefined,
        finishEndDate: this.state.jobFinishTimeFrameChanges ? this.state.jobFinishTimeFrameChanges.endDate : undefined,
        compareTimeOnly: this.state.jobFinishTimeFrameChanges ? this.state.jobFinishTimeFrameChanges.compareTimeOnly : undefined,
        name: this.state.jobNameChanges,
        shortDescription: this.state.jobShortDescriptionChanges,
        description: this.state.jobDescriptionChanges,
        jobFinishedAction: this.state.jobFinishedActionChanges ? this.state.jobFinishedActionChanges.selectedAction : undefined,
        icon: this.state.jobIconChanges,
        Parameters: Parameters.length != 0 ? this.toPointerArray(Parameters,this.props.parameterClassName) : undefined,
        functionname: "finishJob",
      }).dispatch({batch: batch}).then(function() {
        this.setState({jobCreated: true});
      }.bind(this));
    }
  },

  setAttributesAndChangeJob() {
    var batch = new ParseReact.Mutation.Batch();

    this.addArrayFields(batch);
    this.setAttributes(batch);
    this.changeJob(batch);

    batch.dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  changeJob(batch) {
    if(this.state.jobChanged != true) {
      var changes = {};

      if(this.state.hasOwnProperty("visibilityChanges")) changes.visibility = this.state.visibilityChanges;
      if(this.state.hasOwnProperty("jobPointGainChanges")) changes["pointGain"] = this.state.jobPointGainChanges;
      if(this.state.hasOwnProperty("jobEnergyGainChanges")) changes["energyGain"] = this.state.jobEnergyGainChanges;
      if(this.state.jobFinishTimeFrameChanges && this.state.jobFinishTimeFrameChanges.startDate)
        changes["finishStartDate"] = this.state.jobFinishTimeFrameChanges.startDate;
      if(this.state.jobFinishTimeFrameChanges && this.state.jobFinishTimeFrameChanges.endDate)
        changes["finishEndDate"] = this.state.jobFinishTimeFrameChanges.endDate;
      if(this.state.jobFinishTimeFrameChanges && this.state.jobFinishTimeFrameChanges.hasOwnProperty("compareTimeOnly"))
        changes["compareTimeOnly"] = this.state.jobFinishTimeFrameChanges.compareTimeOnly;
      if(this.state.jobNameChanges && this.state.jobNameChanges.objectId) changes["name"] = this.state.jobNameChanges;
      if(this.state.jobShortDescriptionChanges && this.state.jobShortDescriptionChanges.objectId)
        changes["shortDescription"] = this.state.jobShortDescriptionChanges;
      if(this.state.jobDescriptionChanges && this.state.jobDescriptionChanges.objectId)
        changes["description"] = this.state.jobDescriptionChanges;
      if(this.state.jobFinishedActionChanges && this.state.jobFinishedActionChanges.selectedAction)
        changes["jobFinishedAction"] = this.state.jobFinishedActionChanges.selectedAction;
      if(this.state.jobIconChanges) changes["icon"] = this.state.jobIconChanges;
      if(this.state.jobParametersChanges && this.state.jobParametersChanges.unsetFields.length != 0) changes["Parameters"] = undefined;

      ParseReact.Mutation.Set(this.state.job, changes)
      .dispatch({batch: batch}).then(function() {
        this.setState({jobChanged: true});
      }.bind(this));
    }
  },

  addArrayFields(batch){
    if(this.state.jobCodeParameter && this.state.jobCodeParameterAdded != true)
      ParseReact.Mutation.Add(this.state.job, this.props.jobParamsAttrName, this.state.jobCodeParameter)
        .dispatch({batch: batch}).then(function() {
          this.setState({jobCodeParameterAdded: true});
        }.bind(this));

    if(this.state.jobParametersChanges && this.state.jobParametersChanges.poiParam && this.state.jobPoiParameterAdded != true)
      ParseReact.Mutation.AddUnique(this.state.job, this.props.jobParamsAttrName, this.state.jobParametersChanges.poiParam)
        .dispatch({batch: batch}).then(function() {
          this.setState({jobPoiParameterAdded: true});
        }.bind(this));
  },

  createActionMutations(batch) {
    var codeIncorrectValueActionElementMutation, poiIncorrectValueActionElementMutation, jobFinishedActionElementMutation;
    var actionsToChange = [];

    if(this.state.jobParametersChanges) {
      if(this.state.jobParametersChanges.correctCode && this.state.codeIncorrectValueActionElement == undefined)
        codeIncorrectValueActionElementMutation =
          this.handleActionChanges(batch,this.state.jobParametersChanges.codeIncorrectValueAction);
      if(this.state.poiIncorrectValueActionElement == undefined)
        poiIncorrectValueActionElementMutation =
          this.handleActionChanges(batch,this.state.jobParametersChanges.poiIncorrectValueAction);
    }

    if(this.state.jobFinishedActionElement == undefined)
      jobFinishedActionElementMutation = this.handleActionChanges(batch,this.state.jobFinishedActionChanges);

    if(codeIncorrectValueActionElementMutation)
      actionsToChange.push({
        mutationString: "codeIncorrectValueActionElement",
        mutation: codeIncorrectValueActionElementMutation
      });

    if(poiIncorrectValueActionElementMutation)
      actionsToChange.push({
        mutationString: "poiIncorrectValueActionElement",
        mutation: poiIncorrectValueActionElementMutation
      });

    if(jobFinishedActionElementMutation)
      actionsToChange.push({
        mutationString: "jobFinishedActionChangesElement",
        mutation: jobFinishedActionElementMutation
      });

    return actionsToChange;
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.jobName == undefined && this.state.jobNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.dispName + "-Name fehlt",
        modalText: "Ohne " + this.dispName + "-Name - ohne mich.",
        showRetryButton: false
      });
      return;
    }

    var batch = new ParseReact.Mutation.Batch();
    var createIncorrectValueAction = this.createPointerAttributes(batch);
    var actionsToChange = this.createActionMutations(batch);

    for(var i=0;i<actionsToChange.length; i++) {
      actionsToChange[i].mutation.dispatch({ batch: batch }).then(function(mutationString,newLStringOrWElement) {
        this.setState({[mutationString] : newLStringOrWElement});
      }.bind(this,actionsToChange[i].mutationString));
    }

    this.alertBeforeMutationDispatch("Speichern...");

    if(this.state.job == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ createClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({ createClass: true });
      }
    } else {
      var jobNameMutation, jobShortDescriptionMutation, jobDescriptionMutation;

      if(this.state.jobNameSet != true)
        jobNameMutation = this.toLocalizedStringSetMutation(this.state.jobName,this.state.jobNameChanges);
      if(jobNameMutation != undefined)
        jobNameMutation.dispatch({batch: batch}).then(function() {
          this.setState({jobNameSet: true});
        }.bind(this));

      if(this.state.jobShortDescriptionSet != true)
        jobShortDescriptionMutation =
          this.toLocalizedStringSetMutation(this.state.jobShortDescription,this.state.jobShortDescriptionChanges);
      if(jobShortDescriptionMutation != undefined)
        jobShortDescriptionMutation.dispatch({batch: batch}).then(function() {
          this.setState({jobShortDescriptionSet: true});
        }.bind(this));

      if(this.state.jobDescriptionSet != true)
        jobDescriptionMutation =
          this.toLocalizedStringSetMutation(this.state.jobDescription,this.state.jobDescriptionChanges);
      if(jobDescriptionMutation)
        jobDescriptionMutation.dispatch({batch: batch}).then(function() {
          this.setState({jobDescriptionSet: true});
        }.bind(this));

      if(this.state.jobParametersChanges) {
        if(this.state.fieldsUnset != true)
          for(var i = 0; i < this.state.jobParametersChanges.unsetFields.length; i++)
            ParseReact.Mutation.Unset(this.state.job, this.state.jobParametersChanges.unsetFields[i])
              .dispatch({batch: batch}).then(function() {
                this.setState({fieldsUnset: true});
              }.bind(this));

        if(this.state.arrayItemsRemoved != true)
          for(var i = 0; i < this.state.jobParametersChanges.removeArrayItems.length; i++)
            ParseReact.Mutation.Remove(this.state.job,
              this.state.jobParametersChanges.removeArrayItems[i].field,
              this.state.jobParametersChanges.removeArrayItems[i].items)
              .dispatch({batch: batch}).then(function(){
                this.setState({arrayItemsRemoved: true});
              }.bind(this));
      }

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ changeClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({ changeClass: true });
      }
    }
  },

  handleActionChanges(batch,action) {
    if( action && action.type == 0 && action.localizedStringChanges) {
      if(action.localizedString && action.localizedString.objectId) {
        ParseReact.Mutation.Set(action.localizedString, {
          // "default": action.localizedStringChanges,
          deu: action.localizedStringChanges,
          // en: action.localizedStringChanges
        }).dispatch({ batch: batch });
      } else {
        return (
          ParseReact.Mutation.Create(this.props.localizedStringClassName, {
            "default": action.localizedStringChanges,
            deu: actionn.localizedStringChanges,
            en: action.localizedStringChanges
          })
        );
      }
    } else if(action && action.type == 1 && action.webElement) {
      return (
        ParseReact.Mutation.Create("WebElement", {
          content: action.webElement.content,
          // contentStyle: action.webElement.contentStyle
        })
      );
    }
    return undefined;
  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="JobPage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <ButtonToolbar style={ { marginBottom: "10px" } } >
            <Button bsStyle='primary' onClick={this.handleSaveClick} >
              <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
            </Button>
            <LinkContainer to={ { pathname: this.props.classRoute } } >
              <Button bsStyle='default' >
                <Glyphicon glyph="arrow-left" /> Zurück zur Liste
              </Button>
            </LinkContainer>
          </ButtonToolbar>
          <JobPage
            header={
              this.props.dispName + (this.props.params.id == "new"
                ? " hinzufügen"
                : " editieren")
            }
            components={this.props.components}
            jobPointGain={this.state.jobPointGain}
            jobEnergyGain={this.state.jobEnergyGain}
            jobName={this.state.jobName}
            jobShortDescription={this.state.jobShortDescription}
            jobDescription={this.state.jobDescription}
            jobParameters={this.state.jobParameters}
            jobFinishTimeFrame={this.state.jobFinishTimeFrame}
            jobFinishedAction={this.state.jobFinishedAction}
            jobIcon={this.state.jobIcon}
            onChange={this.handleChange}
            updateJobPage={!!this.state.updateJobPage}
          />
          <Panel
            id={this.props.components[4].id}
            header={this.props.components[4].description}
            bsStyle='primary'
          >
            {this.props.children && React.cloneElement(this.props.children, {
              classRoute: "/jobs/" + (this.state.job ? this.state.job.objectId : ""),
              job: this.data.job && this.data.job[0] ? this.data.job[0] : undefined,
            })}
          </Panel>
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } } >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

module.exports =  JobForm;
