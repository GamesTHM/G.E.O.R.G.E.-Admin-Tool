import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import tableMixin from '../mixins/table-mixin';

import ClassTable from '../components/class-table';

import {
  Col, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

var JobTable = React.createClass({

  mixins: [tableMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Job",
      dispName : "Job",
      pluralName: "Jobs",
      parameterClassName: "Parameter",
      locationClassName: "Location",
      questClassName: "Quest",
      classRoute: "/jobs",
      columnNames: ["Job-Name","Quest","Code-Parameter","POI-Parameter"],
      id: "jobList",
    }
  },

  observe(props,state) {
    return {
      jobs: (new Parse.Query(props.parseClassName))
              .include("name")
              .descending("updatedAt"),
      params: (new Parse.Query(props.parameterClassName))
                .containedIn("type",[1000,1001,1002])
                .limit(1000),//.include("name")
      pois: (new Parse.Query(props.locationClassName)).include("name"),
      quests: (new Parse.Query(props.questClassName)).include("name")
    };
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.initializeClassTableData == true)
      this.initializeClassTableData();
  },

  createJobQuestDictionary() {
    var jobQuestDictionary = {};

    for(var i = 0; i < this.data.quests.length; i++) {
      if(this.data.quests[i].Jobs != undefined) {
        for (var j = 0; j < this.data.quests[i].Jobs.length; j++) {
          jobQuestDictionary[this.data.quests[i].Jobs[j].objectId] = this.data.quests[i];
        }
      }
    }

    return jobQuestDictionary;
  },

  createDictionary(data) {
    var dict = {};

    for(var i = 0; i < data.length; i++) {
      dict[data[i].objectId] = data[i];
    }

    return dict;
  },

  prepareJobData() {
    var preparedJobData = [], jobDataSet = {};
    var jobQuestDict = this.createJobQuestDictionary();
    var paramDict = this.createDictionary(this.data.params);
    var poiDict = this.createDictionary(this.data.pois);

    for (var i = 0; i < this.data.jobs.length; i++) {
      jobDataSet = {};
      for (var attr in this.data.jobs[i]) {
        if (this.data.jobs[i].hasOwnProperty(attr)) {
          jobDataSet[attr] = this.data.jobs[i][attr];
        }
      }
      jobDataSet["Quest"] = jobQuestDict[this.data.jobs[i].objectId];

      if(this.data.jobs[i].Parameters != undefined) {
        for (var j = 0, paramObject; j < this.data.jobs[i].Parameters.length; j++) {
          paramObject = paramDict[this.data.jobs[i].Parameters[j].objectId];
          if(paramObject && paramObject.type == 1000) {
            jobDataSet["poiParam"] = poiDict[paramObject.correctValue];
          } else if(paramObject && paramObject.type == 1001) {
            jobDataSet["codeParam"] = paramObject.correctValue;
          }
        }
      }
      preparedJobData.push(jobDataSet);
    }
    return preparedJobData;
  },

  initializeClassTableData() {
    if(this.pendingQueries().length == 0) {
      var classTableColProps = [
        { name: "name", type: "object" },
        { name: "Quest", type: "pointer", classRoute: "/quests/:id" },
        { name: "codeParam", type: "string" },
        { name: "poiParam", type: "pointer", classRoute: "/pois/:id" }
      ];
      var preparedDataSource = this.prepareJobData();
      var classTableData = this.generateClassTableData(classTableColProps,preparedDataSource);

      this.setState({
        classTableData: classTableData,
        initializeClassTableData: false,
        updatePage: true
      });
    }
  },

  render() {
    return (
      <Col md={12} style={ {position: "fixed", top:"56px", bottom:0, overflowY:"scroll" } } >
        <ClassTable
          id={this.props.id}
          classRoute={this.props.classRoute}
          displayName={this.props.dispName}
          columnNames={this.props.columnNames}
          data={this.state.classTableData}
          selectedClasses={this.state.selectedClasses}
          onClassChecked={this.handleClassesSelected}
          onDeleteClick={this.handleDeleteClick}
        />
        {
          this.state.showAlert ?
            <Alert
              bsStyle={this.state.alertStyle}
              onDismiss={this.handleAlertDismiss}
              dismissAfter={this.state.alertDismissAfter}
              style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
            >
              {this.state.alertMessage}
            </Alert>
          : undefined
        }
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.deleteSelectedClasses}>
              {this.state.secondButtonText}
            </Button>
            <Button onClick={this.closeModal}>
              Abbrechen
            </Button>
          </Modal.Footer>
        </Modal>
      </Col>
    );
  }
});

module.exports = JobTable;
