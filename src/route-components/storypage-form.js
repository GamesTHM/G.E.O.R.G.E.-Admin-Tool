import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import StoryPagePage from '../views/storypage';

import {
  Panel, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { IndexLinkContainer } from 'react-router-bootstrap';

var StoryPageForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "StoryPage",
      parseAttributeName: "StoryPages",
      dispName : "StoryPage",

      components: [
        { description: "Texte", id: "storyPageTexts" },
        { description: "Hintergrundbild", id: "storyPageBackground" },
        { description: "Erzählerbild", id: "storyPageNarrator" },
      ],
    }
  },

  observe(props,state) {
    if(props.params.spid != "new") {
      return {
        storyPage: (new Parse.Query(props.parseClassName))
          .include(["bubbleText","boxText"])
          .equalTo("objectId",props.params.spid)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.spid == "new") this.setDefaultStoryPageValues();
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.createClass == true)
      this.setState({createClass: false});

    if(this.state.changeClass == true)
      this.setState({changeClass: false});

    if( this.state.initializeClassPageData == true
        && this.props.params.spid != "new"
        && this.pendingQueries().length == 0
      )
      this.onStoryPageFetched(this.data.storyPage[0]);
  },

  onStoryPageFetched(storyPage) {
    this.setState({
      storyPage: storyPage,
      storyPageBubbleText: storyPage.bubbleText,
      storyPageBoxText: storyPage.boxText,
      storyPageBackground: storyPage.background,
      storyPageNarrator: storyPage.narrator,

      initializeClassPageData: false,
      updatePage: true,
    });
  },

  setDefaultStoryPageValues() {
    this.setState({
      storyPage: undefined,
      storyPageBubbleText: undefined,
      storyPageBoxText: undefined,
      storyPageBackground: undefined,
      storyPageNarrator: undefined,

      updatePage: true,
    });
  },

  createPointerAttributes(batch) {
    var storyPageBubbleTextMutation = this.toLocalizedStringCreateMutation(
      this.state.storyPageBubbleText,this.state.storyPageBubbleTextChanges);
    var storyPageBoxTextMutation = this.toLocalizedStringCreateMutation(
      this.state.storyPageBoxText,this.state.storyPageBoxTextChanges);

    if(storyPageBubbleTextMutation)
      storyPageBubbleTextMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ storyPageBubbleTextChanges: localizedString });
        }.bind(this));

    if(storyPageBoxTextMutation)
      storyPageBoxTextMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ storyPageBoxTextChanges: localizedString });
        }.bind(this));
  },

  createStoryPage() {
    if(this.state.storyPage == undefined) {
      ParseReact.Mutation.Create(this.props.parseClassName, {
        bubbleText: this.state.storyPageBubbleTextChanges,
        boxText: this.state.storyPageBoxTextChanges,
        background: this.state.storyPageBackgroundChanges,
        narrator: this.state.storyPageNarratorChanges
      }).dispatch().then(function(storyPage) {
          this.setState({storyPage: storyPage});
          this.associateWithJob(storyPage);
        }.bind(this),this.mutationSaveError);
    } else this.associateWithJob(this.state.storyPage);
  },

  associateWithJob(storyPage) {
    ParseReact.Mutation.AddUnique(this.props.job,this.props.parseAttributeName,storyPage)
      .dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  changeStoryPage() {
    var changes = {};

    if(
      this.state.storyPageBubbleTextChanges
      && this.state.storyPageBubbleTextChanges.objectId
    ) changes.bubbleText = this.state.storyPageBubbleTextChanges;
    if(
      this.state.storyPageBoxTextChanges
      && this.state.storyPageBoxTextChanges.objectId
    ) changes.boxText = this.state.storyPageBoxTextChanges;

    if(this.state.storyPageBackgroundChanges)
      changes.background = this.state.storyPageBackgroundChanges;
    if(this.state.storyPageNarratorChanges)
      changes.narrator = this.state.storyPageNarratorChanges;

    ParseReact.Mutation.Set(this.state.storyPage,changes)
      .dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  handleSaveClick() {
    this.closeModal();
    if(
      (this.state.storyPageBubbleText == undefined
      && this.state.storyPageBubbleTextChanges == undefined)
      && (this.state.storyPageBoxText == undefined
      && this.state.storyPageBoxTextChanges == undefined)
    )
    {
      var modalTitle, modalText;
      modalTitle = "Es muss mindestens ein Text eingegeben werden";
      modalText = "Es muss entweder ein Sprechblasen- oder ein Kastentext eingegeben werden";

      this.setState({
        showModal: true,
        modalTitle: modalTitle,
        modalText: modalText,
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    var batch = new ParseReact.Mutation.Batch();
    this.createPointerAttributes(batch);

    if(this.state.storyPage == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ createClass: true },this.createStoryPage);
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({createClass: true},this.createStoryPage);
      }
    } else {
      var storyPageBubbleTextMutation, storyPageBoxTextMutation;

      if(this.state.storyPageBubbleTextSet != true)
        storyPageBubbleTextMutation = this.toLocalizedStringSetMutation(
          this.state.storyPageBubbleText,this.state.storyPageBubbleTextChanges);
      if(storyPageBubbleTextMutation != undefined)
        storyPageBubbleTextMutation.dispatch({batch: batch}).then(function() {
          this.setState({storyPageBubbleTextSet: true});
        }.bind(this));

      if(this.state.storyPageBoxTextSet != true)
        storyPageBoxTextMutation = this.toLocalizedStringSetMutation(
          this.state.storyPageBoxText,this.state.storyPageBoxTextChanges);
      if(storyPageBoxTextMutation != undefined)
        storyPageBoxTextMutation.dispatch({batch: batch}).then(function() {
          this.setState({storyPageBoxTextSet: true});
        }.bind(this));

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ changeClass: true },this.changeStoryPage);
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({changeClass: true},this.changeStoryPage);
      }
    }
  },

  render() {
    return (
      <div>
        <ButtonToolbar style={ { marginBottom: "10px" } } >
          <Button bsStyle='primary' onClick={this.handleSaveClick} >
            <Glyphicon glyph="cloud-upload" /> StoryPage speichern und zurück zur Liste
          </Button>
          <IndexLinkContainer to={ { pathname: "/jobs/" + this.props.params.id } } >
            <Button bsStyle='default' >
              <Glyphicon glyph="arrow-left" /> Zurück zur StoryPage-Liste
            </Button>
          </IndexLinkContainer>
        </ButtonToolbar>
        <StoryPagePage
          header={
            this.props.dispName + (this.props.params.spid == "new"
            ? " hinzufügen"
            : " editieren")
          }
          components={this.props.components}
          storyPageBubbleText={this.state.storyPageBubbleText}
          storyPageBoxText={this.state.storyPageBoxText}
          storyPageBackground={this.state.storyPageBackground}
          storyPageNarrator={this.state.storyPageNarrator}
          onChange={this.handleChange}
          updateStoryPagePage={this.state.updatePage}
        />
        {
          this.state.showAlert ?
            <Alert
              bsStyle={this.state.alertStyle}
              onDismiss={this.handleAlertDismiss}
              dismissAfter={this.state.alertDismissAfter}
              style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
            >
              {this.state.alertMessage}
            </Alert>
          : undefined
        }
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton >
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton
              ? <Button bsStyle="primary" onClick={this.handleSaveClick} >
                  Nochmal versuchen
                </Button>
              : undefined
            }
            <Button onClick={this.closeModal} >
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

module.exports = StoryPageForm;
