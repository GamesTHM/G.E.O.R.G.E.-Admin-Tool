import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import tableMixin from '../mixins/table-mixin';

import ClassTable from '../components/class-table';

import {
  Panel, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

var LevelTable = React.createClass({

  mixins: [tableMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "CampusFloorLevel",
      parseAttributeName: "CampusFloorLevels",
      dispName : "Stockwerk",
      pluralName: "Stockwerke",
      columnNames: ["Freigeschaltet","Stockwerksname","Point of Interests"],
      listId: "levelList"
    }
  },

  getCampusLevelIds(campus) {
    var campusLevelIds = [];

    if(campus && campus.CampusFloorLevels) {
      for (var i = 0; i < campus.CampusFloorLevels.length; i++) {
        campusLevelIds.push(campus.CampusFloorLevels[i].objectId);
      }
    }
    return campusLevelIds;
  },

  observe(props,state) {
    if(props.campus) {
      return {
        pois: (new Parse.Query("Location")).include("name"),
        campusFloors: (new Parse.Query("CampusFloorLevel"))
                      .include("name")
                      .containedIn("objectId",this.getCampusLevelIds(props.campus))
                      .ascending("createdAt"),
      };
    } else {
      return;
    }
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.campus) {
      this.setState({ updatePage: true });
    }
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(!this.state.classRoute)
      this.setState({
        classRoute: "/campusses/" + this.props.params.id + "/levels",
        updatePage: true
      });

    if(this.state.initializeClassTableData
      && this.data.campusFloors != undefined
    )
      this.initializeClassTableData();
  },

  initializeClassTableData() {
    if(this.pendingQueries().length == 0) {
      var classTableColProps = [
        { name: "visibility", type: "bool" },
        { name: "name", type: "object" },
        { name: "Locations", type: "array", classRoute: "/pois/:id" },
      ];
      var classTableData = this.generateClassTableData(classTableColProps,this.data.campusFloors,this.data.pois);

      this.setState({
        classTableData: classTableData,
        initializeClassTableData: false,
        updatePage: true
      });
    }
  },

  levelDelete() {
    this.closeModal();
    if(this.state.selectedClasses) {
      var batch = new ParseReact.Mutation.Batch();
      var classesToRemove = [];
      var selectedClasses = [];

      this.alertBeforeMutationDispatch("Löschen...");

      for(var i = 0; i < this.state.selectedClasses.length; i++) {
        selectedClasses.push(this.state.selectedClasses[i]);
        if(selectedClasses[i] != undefined) {
          classesToRemove.push({
            className: this.props.parseClassName,
            objectId: this.state.selectedClasses[i]
          });
          ParseReact.Mutation.Destroy(classesToRemove[i])
          .dispatch({batch: batch})
          .then(function(selectedClasses,index) {
            selectedClasses[index] = undefined;
          }.bind(this,selectedClasses,i));
        }
      }
      if(this.state.levelRemoved != true) {
        ParseReact.Mutation.Remove(this.props.campus,this.props.parseAttributeName,classesToRemove)
          .dispatch({batch: batch})
          .then(function() {
            this.setState({levelRemoved: true});
          }.bind(this));
      }

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(
          this.mutationDeleteSuccess,
          this.mutationDeleteError.bind(this,selectedClasses)
        );
      } else {
        this.mutationDeleteSuccess();
      }
    }
  },

  render() {
    return (
      <div>
        {
          this.props.campus ?
            <ClassTable
              id={this.props.listId}
              classRoute={this.state.classRoute}
              displayName={this.props.dispName}
              columnNames={this.props.columnNames}
              data={this.state.classTableData}
              selectedClasses={this.state.selectedClasses}
              onClassChecked={this.handleClassesSelected}
              onDeleteClick={this.handleDeleteClick}
            />
          : <Alert bsStyle='info' style={ { marginBottom: "0" } } >
              <b><Glyphicon glyph="info-sign" />&nbsp;Stockwerke können erst nach der Erstellung des Campus angelegt werden.</b>
            </Alert>
        }
        {
          this.state.showAlert ?
            <Alert
              bsStyle={this.state.alertStyle}
              onDismiss={this.handleAlertDismiss}
              dismissAfter={this.state.alertDismissAfter}
              style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
            >
              {this.state.alertMessage}
            </Alert>
          : undefined
        }
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton >
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.levelDelete} >
              {this.state.secondButtonText}
            </Button>
            <Button onClick={this.closeModal} >
              Abbrechen
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

module.exports = LevelTable;
