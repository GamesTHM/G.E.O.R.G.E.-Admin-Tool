import React from 'react';
import ReactDOM from 'react-dom';

import NavTopBar from '../components/nav-topbar';
import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import { Label, ListGroup, ListGroupItem, NavItem } from 'react-bootstrap';

var Index = React.createClass({

  getDefaultProps() {
    return {
      components: [
        { description: "Änderungen vom 10.08.16", id: "changes10" },
        { description: "Änderungen vom 02.08.16", id: "changes9" },
        { description: "Änderungen vom 08.06. - 10.06.16", id: "changes8" },
        { description: "Änderungen vom 11.04.16", id: "changes7" },
        { description: "Änderungen vom 16.03.16", id: "changes6" },
        { description: "Änderungen vom 14.03.16", id: "changes5" },
        { description: "Änderungen vom 09.03.16", id: "changes4" },
        { description: "Änderungen vom 03.02.16", id: "changes3" },
        { description: "Änderungen vom 27.01.16", id: "changes2" },
        { description: "Änderungen vom 07.12.15", id: "changes1" },
      ],
    }
  },

  render() {
    var sidebarItemData = this.props.components;

    var sidebarItems = sidebarItemData.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div>
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <h2>Changelog</h2>
          <h3 id={this.props.components[0].id}>
            {this.props.components[0].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem><b>Es wurde eine Seite zum Administrieren von Benutzern hinzugefügt</b><br/>
            - Es können nun Benutzer erstellt, editiert und gelöscht werden<br/>
            - Der Name und das Passwort eines Nutzers kann editiert werden<br/>
            </ListGroupItem>
            <ListGroupItem>Der Name des angemeldeten Nutzers wird angezeigt</ListGroupItem>
            <ListGroupItem>Ein Login-Button wird angezeigt, wenn kein Nutzer angemeldet ist</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[1].id}>
            {this.props.components[1].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem><b>Es wurde eine Seite zum Administrieren von Rollen hinzugefügt</b><br/>
            - Es können nun Rollen erstellt, editiert und gelöscht werden<br/>
            - Der Name der Rolle kann editiert werden<br/>
            - Einer Rolle können Benutzer zugewiesen werden<br/>
            - Einer Rolle können Rollen zugewiesen werden (zugewiesene Rollen bekommen die Rechte der editierten Rolle)
            </ListGroupItem>
            <ListGroupItem>In Parse wurden Sicherheitsaspekte konfiguriert (Class Level Permissions, CLP)</ListGroupItem>
            <ListGroupItem>Unbefugte User werden auf eine entsprechende Seite umgeleitet</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[2].id}>
            {this.props.components[2].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem>Die Links zur Statistik- und Lokalisierungsseite werden vorerst nicht mehr angezeigt</ListGroupItem>
          </ListGroup>
          <h4>Bugfixes</h4>
          <ListGroup>
            <ListGroupItem><b>Image-Picker</b>: Es kam eine Nullpointer-Exception, wenn kein aktives Bild existierte</ListGroupItem>
            <ListGroupItem><b>POI-Formular</b>: Die auswählbaren, vorhandenen Adressen wurden nicht korrekt angezeigt</ListGroupItem>
            <ListGroupItem><b>Job-Formular</b>:<br/>
            - Es wurde nicht der richtige Job von Parse geladen<br/>
            - Die vorhandenen Beschreibungen wurden nicht der Auswahlliste (Combobox) hinzugefügt
            </ListGroupItem>
            <ListGroupItem><b>Job- und Klassentabelle</b>: Es kamen Nullpointer-Exceptions</ListGroupItem>
            <ListGroupItem>Das Query-Limit für Parameter-Queries musste auf 1000 hochgesetzt werden</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[3].id}>
            {this.props.components[3].description}
          </h3>
          <h4>Technische Änderungen</h4>
          <ListGroup>
            <ListGroupItem>Das Tool ist nun an den eigenen Parse-Server angebunden</ListGroupItem>
            <ListGroupItem>Dem Readme wurden Commands zum Starten des Servers auf Linux hinzugefügt</ListGroupItem>
            <ListGroupItem>Die Parse Version wurde fest auf 1.6.14 gesetzt (letzte Version mit der Parse-React funktioniert)</ListGroupItem>
            <ListGroupItem>Die Parse Version wurde fest auf 0.5.0 gesetzt</ListGroupItem>
            <ListGroupItem>Der Image-Picker wurde überarbeitet<br/>
            - Der Hochladeprozess wird nun anders dargestellt<br/>
            - Der Abstand zwischen den Bildern wurde verschmälert<br/>
            - Das Error-Handling wurde verbessert<br/>
            </ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[4].id}>
            {this.props.components[4].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem>Die Technologie wurde auf den neuesten Stand gebracht (Single Page App, Routing, Server, Webpack)</ListGroupItem>
            <ListGroupItem>Die Tabellen- und Formularseiten von Quests und Jobs wurden erstellt. Damit sind nun alle Tabellen- und Formularseiten erstellt.</ListGroupItem>
            <ListGroupItem>Die selektierten Tabellenzeilen werden nun in einer anderen Farbe (Grauton) hervorgehoben</ListGroupItem>
            <ListGroupItem>Nach dem Speichern wird man zur Tabellen-Seite weitergeleitet</ListGroupItem>
          </ListGroup>
          <h4>Technische Änderungen</h4>
          <ListGroup>
            <ListGroupItem>Der (restliche) Code wurde zu ECMAScript6 refactored</ListGroupItem>
            <ListGroupItem>class list wurde an allen Stellen in class table umbenannt</ListGroupItem>
            <ListGroupItem>Die alten HTML-Dateien wurden entfernt, da nur noch eine index.html benötigt wird</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[5].id}>
            {this.props.components[5].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem>Die Tabellen- und Formularseiten von Campussen, Stockwerken und POIs wurden erstellt.</ListGroupItem>
          </ListGroup>
          <h4>Technische Änderungen</h4>
          <ListGroup>
            <ListGroupItem>
              Den Links in der Klassentabelle musste eine optionale parentId bereitgestellt werden,
              da für die Route zu den Stockwerken die Campus ID benötigt wird.<br/>
              (Die Route zu den Stockwerken hat die Form /campusses/:pid/levels/:id)
            </ListGroupItem>
            <ListGroupItem>Der Code wird zu ECMAScript6 refactored (Work in progress)</ListGroupItem>
            <ListGroupItem>Veralteter, auskommentierter Code wurde teilweise entfernt.</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[6].id}>
            {this.props.components[6].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem>Die Technologie wird auf den neuesten Stand gebracht (Work in progress)</ListGroupItem>
            <ListGroupItem>
              Die Tabellen und Formulare von Fraktionen, etc. wurden auf jeweils eine separate Seite aufgetrennt.<br/>
              Dies wurde zuerst nur für die Fraktionsseite umgesetzt. Die restlichen Seiten folgen.
            </ListGroupItem>
            <ListGroupItem>Die Buttons zum Speichern und Abbrechen wurden (aus technischen Gründen) auf die Formularseiten verschoben</ListGroupItem>
            <ListGroupItem>Es wurden provisorisch Icon-Dateien erstellt</ListGroupItem>
          </ListGroup>
          <h4>Technische Änderungen</h4>
          <ListGroup>
            <ListGroupItem>Browserify wurde durch Webpack ersetzt</ListGroupItem>
            <ListGroupItem>
              Es wird Routing integriert, wodurch das Tool zu einer Single Page App wird.<br/>
              Dies hat Änderungen an der Datei- und Ordnerstruktur zur Folge.
            </ListGroupItem>
            <ListGroupItem>Die Authentifizierung wurde an das Routing angepasst</ListGroupItem>
            <ListGroupItem>Zur Entwicklung wurde ein Webpack-Development-Server erstellt</ListGroupItem>
            <ListGroupItem>Für den Produktivbetrieb wurde ein Express-Server erstellt</ListGroupItem>
            <ListGroupItem>Der Code wird zu ECMAScript6 refactored (Work in progress)</ListGroupItem>
            <ListGroupItem>Das Readme wurde den Änderungen entsprechend erweitert</ListGroupItem>
          </ListGroup>
          <h4>Bugfixes</h4>
          <ListGroup>
            <ListGroupItem>Die Logout-Funktion von Parse funktionierte nicht mehr</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[7].id}>
            {this.props.components[7].description}
          </h3>
          <h4>POIs</h4>
          <ListGroup>
            <ListGroupItem>Speichern-Funktionalität für die Zuweisung eines POIs zu einem Campus und einem Stockwerk implementiert</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[8].id}>
            {this.props.components[8].description}
          </h3>
          <h4>Allgemein</h4>
          <ListGroup>
            <ListGroupItem>Login-Funktionalität hinzugefügt</ListGroupItem>
            <ListGroupItem>Jeder Tabelle wurden sinnvolle Spalten/Attribute zur besseren Übersicht hinzugefügt</ListGroupItem>
            <ListGroupItem>In den Spalten wurden u. a. Links zur schnelleren und besseren Navigation hinzugefügt.<br/> Die Links funktionieren aber erst, wenn React-Router integriert wurde.</ListGroupItem>
          </ListGroup>
          <h4>POIs</h4>
          <ListGroup>
            <ListGroupItem>Es wurden Dropdowns hinzugefügt, um einem Poi einen Campus und ein Stockwerk zuweisen zu können.<br/> Allerdings fehlt noch die Speichern-Funktionialität, die die Änderungen schließlich übernimmt.</ListGroupItem>
          </ListGroup>
          <h4>Technische Änderungen</h4>
          <ListGroup>
            <ListGroupItem>Es wurde ein Readme erstellt. YAY! :-)</ListGroupItem>
            <ListGroupItem>Sämtlicher Code in /src/views wurde refactored</ListGroupItem>
          </ListGroup>

          <h3 id={this.props.components[9].id}>
            {this.props.components[9].description}
          </h3>
          <h4>POIs</h4>
          <ListGroup>
            <ListGroupItem>Beim Erstellen eines POIs wird nun automatisch der Job-Parameter angelegt</ListGroupItem>
            <ListGroupItem>Man kann einen POI nun als angreifbar oder nicht angreifbar konfigurieren</ListGroupItem>
          </ListGroup>
          <h4>Technische Änderungen</h4>
          <ListGroup>
            <ListGroupItem>Migration auf React 0.14.2</ListGroupItem>
            <ListGroupItem>Migration auf React-Bootstrap 0.27.3</ListGroupItem>
            <ListGroupItem>Migration auf React-Widgets 3.1.0</ListGroupItem>
            <ListGroupItem>Umstellung von JSXTransformer auf Babel 6</ListGroupItem>
            <ListGroupItem>Austausch des tinyMCE-Pakets</ListGroupItem>
            <ListGroupItem>Austausch des Color-Pickers</ListGroupItem>
          </ListGroup>
          <h4>Bugfixes</h4>
          <ListGroup>
            <ListGroupItem>Nach Löschen von Feld-Inhalten war die Anzeige inkonsistent</ListGroupItem>
            <ListGroupItem>
              Wenn ein Link angeklickt wurde sprang die App immer wieder
              zur Position des Links anstatt die eigentliche Funktionalität auszuführen<br/>
              (Button-Klicks, Anzeige von Tooltipps, etc.)
            </ListGroupItem>
          </ListGroup>
        </PageContent>
      </div>
    );
  }

});

module.exports = Index;
