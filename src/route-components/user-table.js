import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import tableMixin from '../mixins/table-mixin';

import ClassTable from '../components/class-table';

import {
  Col, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

var UserTable = React.createClass({

  mixins: [tableMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "_User",
      dispName : "Benutzer",
      pluralName: "Benutzer",
      classRoute: "/users",
      columnNames: ["Benutzername"],
      id: "userList",
    }
  },

  observe(props,state) {
    return {
      users: (new Parse.Query(props.parseClassName))
              .descending("updatedAt")
    };
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.initializeClassTableData == true)
      this.initializeClassTableData();
  },

  initializeClassTableData() {
    if(this.pendingQueries().length == 0) {
      var classTableColProps = [
        { name: "username", type: "object" },
      ];
      var classTableData = this.generateClassTableData(classTableColProps,this.data.users);

      this.setState({
        classTableData: classTableData,
        initializeClassTableData: false,
        updatePage: true
      });
    }
  },

  userDeleteSuccess() {
    this.refreshQueries();
    this.mutationDeleteSuccess();
  },

  userDeleteError(response) {
    if(response.selectedClasses) {
      this.mutationDeleteError(response.selectedClasses,response.error);
    } else {
      this.mutationDeleteError(this.state.selectedClasses);
    }
  },

  deleteSelectedUsers() {
    Parse.Cloud.run("deleteUsers",{
      userIds: this.state.selectedClasses,
    }).then(this.userDeleteSuccess,this.userDeleteError);
  },

  render() {
    return (
      <div className="UserPage">
        <Col md={12} style={ {position: "fixed", top:"56px", bottom:0, overflowY:"scroll" } } >
          <ClassTable
            id={this.props.id}
            classRoute={this.props.classRoute}
            displayName={this.props.dispName}
            columnNames={this.props.columnNames}
            data={this.state.classTableData}
            selectedClasses={this.state.selectedClasses}
            onClassChecked={this.handleClassesSelected}
            onDeleteClick={this.handleDeleteClick}
          />
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
          <Modal show={this.state.showModal} onHide={this.closeModal}>
            <Modal.Header closeButton>
              <Modal.Title>
                {this.state.modalTitle}
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {this.state.modalText}
            </Modal.Body>
            <Modal.Footer>
              <Button bsStyle="primary" onClick={this.deleteSelectedUsers}>
                {this.state.secondButtonText}
              </Button>
              <Button onClick={this.closeModal}>
                Abbrechen
              </Button>
            </Modal.Footer>
          </Modal>
        </Col>
      </div>
    );
  }

});

module.exports = UserTable;
