import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import QuestPage from '../views/quest';

import {
  NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var QuestForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Quest",
      dispName : "Quest",
      classRoute: "/quests",
      jobClassName: "Job",
      pointerAttributes: ["Faction","Image","Icon"],

      components: [
        { description: "Quest-Name", id: "questName" },
        { description: "Quest-Freischalt-Zeitraum", id: "questTimeFrame" },
        { description: "Quest-Beschreibung", id: "questDescription" },
        { description: "Quest-Bild", id: "questImage" },
        { description: "Quest-Icon", id: "questIcon" },
      ],
    }
  },

  observe(props,state) {
    if(props.params.id != "new") {
      return {
        quest: (new Parse.Query(props.parseClassName))
                .include(["campus.name","faction.name","name","shortDescription","description.contentStyle","image","icon"])
                .equalTo("objectId",this.props.params.id)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultQuestValues();
  },

  componentDidUpdate() {
    if(this.state.updateQuestPage == true && this.state.updatePage == true)
      this.setState({updateQuestPage: false});
    else if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.createClass == true) {
      this.setState({createClass: false});
    }

    if(this.state.changeClass == true) {
      this.setState({changeClass: false});
    }

    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onQuestFetched(this.data.quest[0]);
  },

  onQuestFetched(quest) {
    this.setState({
      quest: quest,
      questVisibility: quest.visibility,
      questFeatured: quest.featured,
      questCycleTime: quest.cycleTime,
      questCampus: quest.campus,
      questFaction: quest.faction,
      questName: quest.name,
      questShortDescription: quest.shortDescription,
      questJobs: quest.Jobs,
      questTimeFrame: {
        startDate: quest.startdate,
        endDate: quest.enddate
      },
      questDescription: {
        content: quest.description && quest.description.content ? quest.description.content : " ",
        contentStyle: quest.description && quest.description.contentStyle ? quest.description.contentStyle : undefined
      },
      questImage: quest.image,
      questIcon: quest.icon,

      initializeClassPageData: false,
      updateQuestPage: true,
      updatePage: true,
    });
  },

  setDefaultQuestValues() {
    this.setState({
      quest: undefined,
      questVisibility: false,
      questFeatured: false,
      questCycleTime: 0,
      questCampus: undefined,
      questFaction: undefined,
      questName: undefined,
      questShortDescription: undefined,
      questJobs: undefined,
      questTimeFrame: {
        startDate: undefined,
        endDate: undefined
      },
      questDescription: undefined,
      questImage: undefined,
      questIcon: undefined,

      updateQuestPage: true,
      updatePage: true,
    });
  },

  createPointerAttributes(batch) {
    var questNameMutation = this.toLocalizedStringCreateMutation(this.state.questName,this.state.questNameChanges);
    if(questNameMutation)
      questNameMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({
            questNameChanges: localizedString
          });
        }.bind(this));

    var questShortDescriptionMutation =
      this.toLocalizedStringCreateMutation(this.state.questShortDescription,this.state.questShortDescriptionChanges);
    if(questShortDescriptionMutation)
      questShortDescriptionMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({
            questShortDescriptionChanges: localizedString
          });
        }.bind(this));

    var questDescriptionMutation =
      this.toWebElementCreateMutation(this.state.quest ? this.state.quest.description : undefined, this.state.questDescriptionChanges);
    if(questDescriptionMutation)
      questDescriptionMutation.dispatch({batch: batch})
        .then(function(webElement) {
          this.setState({
            questDescriptionChanges: webElement
          });
        }.bind(this));
  },

  createQuest() {
    ParseReact.Mutation.Create(this.props.parseClassName, {
      visibility: this.state.hasOwnProperty("visibilityChanges") ? this.state.visibilityChanges : false,
      featured: this.state.hasOwnProperty("questFeaturedChanges") ? this.state.questFeaturedChanges : false,
      cycleTime: this.state.questCycleTimeChanges ? this.state.questCycleTimeChanges : 0,
      timeToLive: this.state.questCycleTimeChanges ? this.state.questCycleTimeChanges : 0,
      faction: this.state.questFactionChanges,
      campus: this.state.questCampusChanges,
      Jobs: this.toPointerArray(this.state.questJobsChanges,this.props.jobClassName),
      startdate: this.state.questTimeFrameChanges ? this.state.questTimeFrameChanges.startDate : undefined,
      enddate: this.state.questTimeFrameChanges ? this.state.questTimeFrameChanges.endDate : undefined,
      image: this.state.questImageChanges,
      icon: this.state.questIconChanges,
      name: this.state.questNameChanges,
      shortDescription: this.state.questShortDescriptionChanges,
      description: this.state.questDescriptionChanges,
      minAPI: 0
    }).dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  changeQuest() {
    var batch = new ParseReact.Mutation.Batch();
    var changes = {};

    if(this.state.questChangesSet != true) {
      if(this.state.hasOwnProperty("visibilityChanges")) changes.visibility = this.state.visibilityChanges;
      if(this.state.hasOwnProperty("questFeaturedChanges")) changes.featured = this.state.questFeaturedChanges;
      if(this.state.hasOwnProperty("questCycleTimeChanges")) changes.cycleTime = this.state.questCycleTimeChanges;
      if(this.state.hasOwnProperty("questCycleTimeChanges")) changes.timeToLive = this.state.questCycleTimeChanges;
      if(this.state.questFactionChanges) changes.faction = this.state.questFactionChanges;
      if(this.state.questCampusChanges) changes.campus = this.state.questCampusChanges;
      if(this.state.questJobsChanges) changes.Jobs = this.toPointerArray(this.state.questJobsChanges,this.props.jobClassName);
      if(this.state.questTimeFrameChanges && this.state.questTimeFrameChanges.startDate)
      changes.startdate = this.state.questTimeFrameChanges.startDate;
      if(this.state.questTimeFrameChanges && this.state.questTimeFrameChanges.endDate)
      changes.enddate = this.state.questTimeFrameChanges.endDate;
      if(this.state.questImageChanges) changes.image = this.state.questImageChanges;
      if(this.state.questIconChanges) changes.icon = this.state.questIconChanges;
      if(this.state.questNameChanges && this.state.questNameChanges.objectId) changes.name = this.state.questNameChanges;
      if(this.state.questShortDescriptionChanges && this.state.questShortDescriptionChanges.objectId)
      changes.shortDescription = this.state.questShortDescriptionChanges;
      if(this.state.questDescriptionChanges && this.state.questDescriptionChanges.objectId)
      changes.description = this.state.questDescriptionChanges;

      ParseReact.Mutation.Set(this.state.quest,changes)
      .dispatch({batch: batch})
      .then(function() {
        this.setState({"questChangesSet": true});
      }.bind(this));
    }

    for (var i = 0; i < this.props.pointerAttributes.length; i++) {
      if(this.checkUnsetPointerAttr(this.props.pointerAttributes[i]))
        this.unsetPointerAttr(this.props.pointerAttributes[i],batch);
    }

    if(batch.getNumberOfRequests() != 0) {
      batch.dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
    } else {
      this.mutationSaveSuccess();
    }

  },

  checkUnsetPointerAttr(pointerAttr) {
    return (
      this.state["quest"+pointerAttr+"Unset"] != true
      && this.state.hasOwnProperty("quest"+pointerAttr+"Changes")
      && this.state["quest"+pointerAttr+"Changes"] == undefined
    )
  },

  unsetPointerAttr(pointerAttr,batch) {
    ParseReact.Mutation.Unset(this.state.quest,pointerAttr.toLowerCase())
    .dispatch({batch: batch})
    .then(function() {
      this.setState({["quest"+pointerAttr+"Unset"]: true});
    }.bind(this));
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.questName == undefined && this.state.questNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.props.dispName + "-Name fehlt",
        modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    var batch = new ParseReact.Mutation.Batch();
    this.createPointerAttributes(batch);

    if(this.state.quest == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({createClass: true},this.createQuest);
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({createClass: true},this.createQuest);
      }
    } else {
      var questNameMutation, questShortDescriptionMutation, questDescriptionMutation;

      if(this.state.questNameSet != true) {
        questNameMutation = this.toLocalizedStringSetMutation(
          this.state.questName,
          this.state.questNameChanges
        );
      }
      if(questNameMutation != undefined) {
        questNameMutation.dispatch({batch: batch})
        .then(function() {
          this.setState({questNameSet: true});
        }.bind(this));
      }

      if(this.state.questShortDescriptionSet != true) {
        questShortDescriptionMutation = this.toLocalizedStringSetMutation(
          this.state.questShortDescription,
          this.state.questShortDescriptionChanges
        );
      }
      if(questShortDescriptionMutation != undefined) {
        questShortDescriptionMutation.dispatch({batch: batch})
        .then(function() {
          this.setState({questShortDescriptionSet: true});
        }.bind(this));
      }

      if(this.state.questDescriptionSet != true) {
        questDescriptionMutation = this.toWebElementSetMutation(
          this.state.quest
          ? this.state.quest.description
          : undefined,
          this.state.questDescriptionChanges
        );
      }
      if(questDescriptionMutation) {
        questDescriptionMutation.dispatch({batch: batch})
        .then(function() {
          this.setState({questDescriptionSet: true});
        }.bind(this));
      }

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({changeClass: true},this.changeQuest);
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({changeClass: true},this.changeQuest);
      }
    }
  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="QuestPage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <ButtonToolbar style={ { marginBottom: "10px" } } >
            <Button bsStyle='primary' onClick={this.handleSaveClick} >
              <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
            </Button>
            <LinkContainer to={ { pathname: this.props.classRoute } } >
              <Button bsStyle='default' >
                <Glyphicon glyph="arrow-left" /> Zurück zur Liste
              </Button>
            </LinkContainer>
          </ButtonToolbar>
          <QuestPage
            header={
              this.props.dispName + (this.props.params.id == "new"
                ? " hinzufügen"
                : " editieren")
            }
            components={this.props.components}
            visibility={this.state.questVisibility}
            questFeatured={this.state.questFeatured}
            questCycleTime={this.state.questCycleTime}
            questCampus={this.state.questCampus}
            questFaction={this.state.questFaction}
            questName={this.state.questName}
            questShortDescription={this.state.questShortDescription}
            questJobs={this.state.questJobs}
            questTimeFrame={this.state.questTimeFrame}
            questDescription={this.state.questDescription}
            questImage={this.state.questImage}
            questIcon={this.state.questIcon}
            onChange={this.handleChange}
            updateQuestPage={this.state.updateQuestPage}
          />
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={{ position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" }}
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports = QuestForm;
