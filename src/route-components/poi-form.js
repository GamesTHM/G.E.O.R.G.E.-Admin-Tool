import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import PoiPage from '../views/poi';

import {
  NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var PoiForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Location",
      parseAttributeName: "Locations",
      dispName : "POI",
      classRoute: "/pois",

      captureDataClassName: "LocationCaptureData",
      actionClassName: "Action",
      localizedStringClassName: "LocalizedString",
      parameterClassName: "Parameter",

      components: [
        { description: "POI-Name", id: "poiName" },
        { description: "POI-Position", id: "poiPosition" },
        { description: "POI-Geschichte", id: "poiLore" },
        { description: "POI-Informationen", id: "poiInformation" },
        { description: "POI-Bild", id: "poiImage" },
        { description: "POI-Mini-Bild", id: "poiMiniImage" },
      ],
    }
  },

  observe(props,state) {
    if(props.params.id != "new") {
      return {
        poi: (new Parse.Query(props.parseClassName))
                .include(["name","address","lore.contentStyle","information.contentStyle","headerImage","miniImage","Campus.name"])
                .equalTo("objectId",this.props.params.id)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultPoiValues();
  },

  componentDidUpdate(){
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.createClass == true)
      this.setState({createClass: false});

    if(this.state.changeClass == true)
      this.setState({changeClass: false});

    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onPoiFetched(this.data.poi[0]);
  },

  onPoiFetched(poi) {
    this.setState({
      poi: poi,
      poiVisibility: poi.visibility,
      poiAttackable: poi.LocationCaptureData != undefined,
      poiName: poi.name,
      poiAddress: poi.address,
      poiBeaconData: {
        beaconuuid: poi.beaconuuid,
        beaconmayor: poi.beaconmayor,
        beaconminor: poi.beaconminor
      },
      poiPosition: poi.geopoint ?
        [{ lat: poi.geopoint.latitude, lng: poi.geopoint.longitude }]
      : undefined,
      poiLore: {
        content: poi.lore && poi.lore.content ? poi.lore.content : " ",
        contentStyle: poi.lore && poi.lore.contentStyle ? poi.lore.contentStyle : undefined
      },
      poiInformation: {
        content: poi.information && poi.information.content ? poi.information.content : " ",
        contentStyle: poi.information && poi.information.contentStyle ? poi.information.contentStyle : undefined
      },
      poiImage: poi.headerImage,
      poiIcon: poi.miniImage,

      initializeClassPageData: false,
      updatePage: true,
    });

  },

  setDefaultPoiValues() {
    this.setState({
      poi: undefined,
      poiVisibility: false,
      poiAttackable: false,
      poiName: undefined,
      poiAddress: undefined,
      poiBeaconData: undefined,
      poiPosition: undefined,
      poiLore: undefined,
      poiInformation: undefined,
      poiImage: undefined,
      poiIcon: undefined,

      updatePage: true,
    });
  },

  createPointerAttributes(batch) {
    if( this.state.poiAttackable == false && this.state.poiAttackableChanges == true && this.state.poiLocationCaptureData == undefined)
      ParseReact.Mutation.Create(this.props.captureDataClassName, { energy: 0 }).dispatch({batch: batch})
        .then(function(poiLocationCaptureData) {
          this.setState({ poiLocationCaptureData: poiLocationCaptureData });
        }.bind(this));

    var poiNameMutation = this.toLocalizedStringCreateMutation(this.state.poiName,this.state.poiNameChanges);
    if(poiNameMutation)
      poiNameMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ poiNameChanges: localizedString });
        }.bind(this));

    var poiAddressMutation =
      this.toLocalizedStringCreateMutation(this.state.poiAddress,this.state.poiAddressChanges);
    if(poiAddressMutation)
      poiAddressMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ poiAddressChanges: localizedString });
        }.bind(this));

    var poiLoreMutation = this.toWebElementCreateMutation(this.state.poi ? this.state.poi.lore : undefined,this.state.poiLoreChanges);
    if(poiLoreMutation)
      poiLoreMutation.dispatch({batch: batch})
        .then(function(webElement) {
          this.setState({ poiLoreChanges: webElement });
        }.bind(this));

    var poiInformationMutation =
      this.toWebElementCreateMutation(this.state.poi ? this.state.poi.information : undefined,this.state.poiInformationChanges);
    if(poiInformationMutation)
      poiInformationMutation.dispatch({batch: batch})
        .then(function(webElement) {
          this.setState({ poiInformationChanges: webElement });
        }.bind(this));
  },

  applyPointerObjectChanges(batch) {
    var poiNameMutation, poiAddressMutation, poiLoreMutation, poiInformationMutation;

    if(this.state.poiNameSet != true)
      poiNameMutation = this.toLocalizedStringSetMutation(this.state.poiName,this.state.poiNameChanges);
    if(poiNameMutation != undefined)
      poiNameMutation.dispatch({batch: batch}).then(function() {
        this.setState({poiNameSet: true});
      }.bind(this));

    if(this.state.poiAddressSet != true)
      poiAddressMutation = this.toLocalizedStringSetMutation(this.state.poiAddress,this.state.poiAddressChanges);
    if(poiAddressMutation != undefined)
      poiAddressMutation.dispatch({batch: batch}).then(function() {
        this.setState({poiAddressSet: true});
      }.bind(this));

    if(this.state.poiLoreSet != true)
      poiLoreMutation = this.toWebElementSetMutation(this.state.poi ? this.state.poi.lore : undefined,this.state.poiLoreChanges);
    if(poiLoreMutation)
      poiLoreMutation.dispatch({batch: batch}).then(function() {
        this.setState({poiLoreSet: true});
      }.bind(this));

    if(this.state.poiInformationSet != true)
      poiInformationMutation =
        this.toWebElementSetMutation(this.state.poi ? this.state.poi.information : undefined,this.state.poiInformationChanges);
    if(poiInformationMutation)
      poiInformationMutation.dispatch({batch: batch}).then(function() {
        this.setState({poiInformationSet: true});
      }.bind(this));
  },

  deleteLocationCaptureData(batch) {
    if(this.state.poiLocationCaptureDataDeleted != true)
      ParseReact.Mutation.Destroy(this.state.poi.LocationCaptureData).dispatch({batch: batch}).then(function() {
        this.setState({poiLocationCaptureDataDeleted: true});
      }.bind(this));

    if(this.state.poiLocationCaptureDataUnset != true)
      ParseReact.Mutation.Unset(this.state.poi,this.props.captureDataClassName)
        .dispatch({batch: batch})
        .then(function() {
          this.setState({poiLocationCaptureDataUnset: true});
        }.bind(this));
  },

  swapPoiCampusFloor(poi,batch) {
    if( this.state.poiLocationDataChanges.currentCampusFloorLevel != undefined
        && this.state.poiRemoved != true) {
      ParseReact.Mutation.Remove(
        this.state.poiLocationDataChanges.currentCampusFloorLevel,
        this.props.parseAttributeName,
        poi
      ).dispatch({ batch: batch })
      .then(function() {
        this.setState({ poiRemoved: true });
      }.bind(this));
    }

    if( this.state.poiLocationDataChanges.campusFloorLevel != undefined
        && this.state.poiAdded != true) {
      ParseReact.Mutation.AddUnique(
        this.state.poiLocationDataChanges.campusFloorLevel,
        this.props.parseAttributeName,
        poi
      ).dispatch({ batch: batch })
      .then(function() {
        this.setState({ poiAdded: true });
      }.bind(this));
    }
  },

  executeMutationsAfterPoiCreated(poi) {
    var batch = new ParseReact.Mutation.Batch();

    this.createPoiParam(poi,batch);
    if( this.state.poiLocationDataChanges != undefined &&
        this.state.poiLocationDataChanges.campusFloorLevelChanged) {
      this.swapPoiCampusFloor(poi,batch);
    }

    if(batch.getNumberOfRequests() != 0) {
      batch.dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
    }
  },

  createPoi() {
    if(this.state.createdPoi == undefined) {
      var locationACL = new Parse.ACL();
      locationACL.setPublicReadAccess(true);
      locationACL.setRoleReadAccess("Admin",true);
      locationACL.setRoleWriteAccess("Admin",true);
      ParseReact.Mutation.Create(this.props.parseClassName, {
        visibility: this.state.visibilityChanges ? this.state.visibilityChanges : false,
        Campus: this.state.poiLocationDataChanges ? this.state.poiLocationDataChanges.campus : undefined,
        beaconuuid: this.state.poiBeaconDataChanges ? this.state.poiBeaconDataChanges.beaconuuid : undefined,
        beaconmayor: this.state.poiBeaconDataChanges ? this.state.poiBeaconDataChanges.beaconmayor : undefined,
        beaconminor: this.state.poiBeaconDataChanges ? this.state.poiBeaconDataChanges.beaconminor : undefined,
        geopoint: this.state.poiPositionChanges ?
          new Parse.GeoPoint(this.state.poiPositionChanges.lat,this.state.poiPositionChanges.lng) : undefined,
        headerImage: this.state.poiImageChanges,
        miniImage: this.state.poiIconChanges,
        LocationCaptureData: this.state.poiLocationCaptureData,
        name: this.state.poiNameChanges,
        address: this.state.poiAddressChanges,
        lore: this.state.poiLoreChanges,
        information: this.state.poiInformationChanges,
        ACL: locationACL
      }).dispatch().then(function(poi) {
        this.setState({createdPoi: poi});
        this.executeMutationsAfterPoiCreated(poi);
      }.bind(this),this.mutationSaveError);
    } else {
      this.executeMutationsAfterPoiCreated(this.state.createdPoi);
    }
  },

  createPoiParam(poi,batch) {
    if(this.state.poiParamCreated != true) {
      var poiIncorrectValueAction;
      var Action = Parse.Object.extend(this.props.actionClassName);
      poiIncorrectValueAction = new Action();
      poiIncorrectValueAction.id = "6Tz10UBh66";

      var poiUserInfo;
      var LocalizedString = Parse.Object.extend(this.props.localizedStringClassName);
      poiUserInfo = new LocalizedString();
      poiUserInfo.id = "vaQuEXjVD7";

      ParseReact.Mutation.Create(this.props.parameterClassName, {
        name: "locations",
        type: 1000,
        LocalizedString: poiUserInfo,
        optional: false,
        correctValue: poi.objectId,
        incorrectValueAction: poiIncorrectValueAction
      }).dispatch({ batch: batch }).then(function() {
        this.setState({ poiParamCreated: true });
      }.bind(this))
    }
  },

  changePoi() {
    var changes = {};

    if(this.state.hasOwnProperty("visibilityChanges")) changes.visibility = this.state.visibilityChanges;
    if(this.state.poiLocationDataChanges && this.state.poiLocationDataChanges.campusChanged)
      changes.Campus = this.state.poiLocationDataChanges.campus;
    if(this.state.poiBeaconDataChanges && this.state.poiBeaconDataChanges.beaconuuid)
      changes.beaconuuid = this.state.poiBeaconDataChanges.beaconuuid;
    if(this.state.poiBeaconDataChanges && this.state.poiBeaconDataChanges.beaconmayor)
      changes.beaconmayor = this.state.poiBeaconDataChanges.beaconmayor;
    if(this.state.poiBeaconDataChanges && this.state.poiBeaconDataChanges.beaconminor)
      changes.beaconminor = this.state.poiBeaconDataChanges.beaconminor;
    if(this.state.poiPositionChanges)
      changes.geopoint = new Parse.GeoPoint(this.state.poiPositionChanges.lat,this.state.poiPositionChanges.lng);
    if(this.state.poiImageChanges) changes.headerImage = this.state.poiImageChanges;
    if(this.state.poiIconChanges) changes.miniImage = this.state.poiIconChanges;
    if(this.state.poiLocationCaptureData) changes.LocationCaptureData = this.state.poiLocationCaptureData;
    if(this.state.poiNameChanges && this.state.poiNameChanges.objectId) changes.name = this.state.poiNameChanges;
    if(this.state.poiAddressChanges && this.state.poiAddressChanges.objectId) changes.address = this.state.poiAddressChanges;
    if(this.state.poiLoreChanges && this.state.poiLoreChanges.objectId) changes.lore = this.state.poiLoreChanges;
    if(this.state.poiInformationChanges && this.state.poiInformationChanges.objectId) changes.information = this.state.poiInformationChanges;

    ParseReact.Mutation.Set(this.state.poi,changes).dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.poiName == undefined && this.state.poiNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.props.dispName + "-Name fehlt",
        modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    var batch = new ParseReact.Mutation.Batch();
    this.createPointerAttributes(batch);

    if(this.state.poi == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({createClass: true},this.createPoi);
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({createClass: true},this.createPoi);
      }
    } else {
      if( this.state.poi.LocationCaptureData != undefined
          && this.state.poiAttackableChanges == false) {
        this.deleteLocationCaptureData(batch);
      }
      this.applyPointerObjectChanges(batch);

      if( this.state.poiLocationDataChanges != undefined &&
          this.state.poiLocationDataChanges.campusFloorLevelChanged) {
        this.swapPoiCampusFloor(this.state.poi,batch);
      }

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({changeClass: true},this.changePoi);
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({changeClass: true},this.changePoi);
      }
    }
  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="PoiPage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <ButtonToolbar style={ { marginBottom: "10px" } } >
            <Button bsStyle='primary' onClick={this.handleSaveClick} >
              <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
            </Button>
            <LinkContainer to={ { pathname: this.props.classRoute } } >
              <Button bsStyle='default' >
                <Glyphicon glyph="arrow-left" /> Zurück zur Liste
              </Button>
            </LinkContainer>
          </ButtonToolbar>
          <PoiPage
            header={
              this.props.dispName + (this.props.params.id == "new"
                ? " hinzufügen"
                : " editieren")
            }
            components={this.props.components}
            visibility={this.state.poiVisibility}
            poiAttackable={this.state.poiAttackable}
            poiName={this.state.poiName}
            poiId={this.state.poi ? this.state.poi.objectId : undefined}
            campusId={this.state.poi && this.state.poi.Campus ? this.state.poi.Campus.objectId : undefined}
            poiAddress={this.state.poiAddress}
            poiBeaconData={this.state.poiBeaconData}
            poiPosition={this.state.poiPosition}
            poiLore={this.state.poiLore}
            poiInformation={this.state.poiInformation}
            poiImage={this.state.poiImage}
            poiIcon={this.state.poiIcon}
            onChange={this.handleChange}
            updatePoiPage={this.state.updatePage}
          />
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports =  PoiForm;
