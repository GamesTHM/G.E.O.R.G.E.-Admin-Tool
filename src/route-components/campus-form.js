import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import CampusPage from '../views/campus';

import {
  Panel, NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var CampusForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Campus",
      dispName : "Campus",
      classRoute: "/campusses",

      components: [
        { description: "Campus-Name", id: "campusName" },
        { description: "Campus-Bereich", id: "campusArea" },
        { description: "Campus-Stockwerke", id: "campusFloors" },
      ],
    }
  },

  observe(props,state) {
    if(this.props.params.id != "new") {
      return {
        campus: (new Parse.Query(this.props.parseClassName))
                  .include("name")
                  .equalTo("objectId",this.props.params.id)
      };
    } else {
      return;
    }
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.location.pathname != this.props.location.pathname)
      this.setState({ updatePage: true});
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultCampusValues();
    if(this.props.params.lid) location.href="#campusFloors";
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});
    if(this.state.updateCampusPage == true)
      this.setState({updateCampusPage: false});

    if(this.state.createClass == true) {
      this.setState({createClass: false});
      this.createCampus();
    }
    if(this.state.changeClass == true) {
      this.setState({changeClass: false});
      this.changeCampus();
    }

    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onCampusFetched(this.data.campus[0]);
  },

  onCampusFetched(campus) {
    this.setState({
      campus: campus,
      campusVisibility: campus.visibility,
      campusName: campus.name,
      campusArea: [
        { lat: campus.NE_lat, lng: campus.NE_long },
        { lat: campus.SW_lat, lng: campus.SW_long },
      ],

      initializeClassPageData: false,
      updatePage: true,
      updateCampusPage: true,
    });
  },

  setDefaultCampusValues() {
    this.setState({
      campus: undefined,
      campusVisibility: false,
      campusName: undefined,
      campusArea: undefined,

      updatePage: true,
      updateCampusPage: true,
    });
  },

  createPointerAttributes(batch) {
    var campusNameMutation = this.toLocalizedStringCreateMutation(this.state.campusName,this.state.campusNameChanges);
    if(campusNameMutation)
      campusNameMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ campusNameChanges: localizedString });
        }.bind(this));
  },

  createCampus() {
    ParseReact.Mutation.Create(this.props.parseClassName, {
      visibility: this.state.visibilityChanges ? this.state.visibilityChanges : false,
      NE_lat: this.state.campusAreaChanges ? this.state.campusAreaChanges[0].lat : undefined,
      NE_long: this.state.campusAreaChanges ? this.state.campusAreaChanges[0].lng : undefined,
      SW_lat: this.state.campusAreaChanges ? this.state.campusAreaChanges[1].lat : undefined,
      SW_long: this.state.campusAreaChanges ? this.state.campusAreaChanges[1].lng : undefined,
      name: this.state.campusNameChanges,
    }).dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  changeCampus() {
    var changes = {};

    if(this.state.hasOwnProperty("visibilityChanges")) changes.visibility = this.state.visibilityChanges;
    if(this.state.campusAreaChanges) {
      changes.NE_lat = this.state.campusAreaChanges[0].lat;
      changes.NE_long = this.state.campusAreaChanges[0].lng;
      changes.SW_lat = this.state.campusAreaChanges[1].lat;
      changes.SW_long = this.state.campusAreaChanges[1].lng;
    }
    if(this.state.campusNameChanges && this.state.campusNameChanges.objectId) changes.name = this.state.campusNameChanges;

    ParseReact.Mutation.Set(this.state.campus,changes).dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.campusName == undefined && this.state.campusNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.props.dispName + "-Name fehlt",
        modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    var batch = new ParseReact.Mutation.Batch();
    this.createPointerAttributes(batch);

    if(this.state.campus == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ createClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({ createClass: true });
      }
    } else {
      var campusNameMutation;

      if(this.state.campusNameSet != true)
        campusNameMutation = this.toLocalizedStringSetMutation(this.state.campusName,this.state.campusNameChanges);
      if(campusNameMutation != undefined)
        campusNameMutation.dispatch({batch: batch}).then(function() {
          this.setState({ campusNameSet: true });
        }.bind(this));

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ changeClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({ changeClass: true });
      }
    }
  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="CampusPage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <ButtonToolbar style={ { marginBottom: "10px" } } >
            <Button bsStyle='primary' onClick={this.handleSaveClick} >
              <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
            </Button>
            <LinkContainer to={ { pathname: this.props.classRoute } } >
              <Button bsStyle='default' >
                <Glyphicon glyph="arrow-left" /> Zurück zur Liste
              </Button>
            </LinkContainer>
          </ButtonToolbar>
          <CampusPage
            header={
              this.props.dispName + (this.props.params.id == "new"
              ? " hinzufügen"
              : " editieren")
            }
            components={this.props.components}
            campus={this.state.campus}
            visibility={this.state.campusVisibility}
            campusName={this.state.campusName}
            geoPoints={this.state.campusArea}
            onChange={this.handleChange}
            updateCampusPage={this.state.updateCampusPage}
          />
          <Panel
            id={this.props.components[2].id}
            header={this.props.components[2].description}
            bsStyle='primary'
          >
            {this.props.children && React.cloneElement(this.props.children, {
              classRoute: "/campusses/" + (this.state.campus ? this.state.campus.objectId : ""),
              campus: this.data.campus && this.data.campus[0] ? this.data.campus[0] : undefined,
            })}
          </Panel>
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={{ position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" }}
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports = CampusForm;
