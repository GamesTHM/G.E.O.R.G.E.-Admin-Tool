import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import LevelPage from '../views/level';

import {
  Panel, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { IndexLinkContainer } from 'react-router-bootstrap';

var LevelForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "CampusFloorLevel",
      parseAttributeName: "CampusFloorLevels",
      locationClassName: "Location",
      dispName : "Stockwerk",

      components: [
        { description: "Stockwerksname", id: "levelName" },
      ],
    }
  },

  observe(props,state) {
    if(this.props.params.lid != "new") {
      return {
        level: (new Parse.Query(this.props.parseClassName))
                .include("name")
                .equalTo("objectId",this.props.params.lid)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.lid == "new") this.setDefaultLevelValues();
  },

  componentDidUpdate() {
    if(this.state.updateLevelPage == true && this.state.updatePage == true)
      this.setState({updateLevelPage: false});
    else if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.createClass == true) {
      this.setState({createClass: false});
      this.createLevel();
    }

    if(this.state.changeClass == true) {
      this.setState({changeClass: false});
      this.changeLevel();
    }

    if( this.state.initializeClassPageData == true
        && this.props.params.lid != "new"
        && this.pendingQueries().length == 0
      )
      this.onLevelFetched(this.data.level[0]);
  },

  onLevelFetched(level) {
    this.setState({
      level: level,
      levelVisibility: level.visibility,
      levelName: level.name,
      levelPOIs: level.Locations,

      initializeClassPageData: false,
      updateLevelPage: true,
      updatePage: true,
    });
  },

  setDefaultLevelValues() {
    this.setState({
      level: undefined,
      levelVisibility: false,
      levelName: undefined,
      levelPOIs: undefined,

      updateLevelPage: true,
      updatePage: true,
    });
  },

  createPointerAttributes(batch) {
    var levelNameMutation = this.toLocalizedStringCreateMutation(this.state.levelName,this.state.levelNameChanges);

    if(levelNameMutation)
      levelNameMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ levelNameChanges: localizedString });
        }.bind(this));
  },

  createLevel() {
    if(this.state.campusFloorLevel == undefined) {
      ParseReact.Mutation.Create(this.props.parseClassName, {
        visibility: this.state.visibilityChanges ? this.state.visibilityChanges : false,
        name: this.state.levelNameChanges,
        Locations: this.toPointerArray(this.state.levelPOIsChanges,this.props.locationClassName),
      }).dispatch().then(function(level) {
          this.setState({campusFloorLevel: level});
          this.associateWithCampus(level);
        }.bind(this),this.mutationSaveError);
    } else this.associateWithCampus(this.state.campusFloorLevel);
  },

  associateWithCampus(level) {
    ParseReact.Mutation.AddUnique(this.props.campus,this.props.parseAttributeName,level)
      .dispatch().then(this.mutationSaveLocationSuccess.bind(this,level),this.mutationSaveError);
  },

  changeLevel() {
    var changes = {};

    if(this.state.hasOwnProperty("visibilityChanges")) changes.visibility = this.state.visibilityChanges;
    if(this.state.levelNameChanges && this.state.levelNameChanges.objectId) changes.name = this.state.levelNameChanges;
    if(this.state.levelPOIsChanges) changes.Locations = this.toPointerArray(this.state.levelPOIsChanges,this.props.locationClassName);

    ParseReact.Mutation.Set(this.state.level,changes).dispatch().then(this.mutationSaveLocationSuccess,this.mutationSaveError);
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.levelName == undefined && this.state.levelNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.props.dispName + "-Name fehlt",
        modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    var batch = new ParseReact.Mutation.Batch();
    this.createPointerAttributes(batch);

    if(this.state.level == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ createClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({createClass: true});
      }
    } else {
      var levelNameMutation;

      if(this.state.levelNameSet != true)
        levelNameMutation = this.toLocalizedStringSetMutation(this.state.levelName,this.state.levelNameChanges);
      if(levelNameMutation != undefined)
        levelNameMutation.dispatch({batch: batch}).then(function() {
          this.setState({levelNameSet: true});
        }.bind(this));

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ changeClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({changeClass: true});
      }
    }
  },

  mutationSaveLocationSuccess(level) {
    this.replaceState({
      showAlert: true,
      alertMessage: "Gespeichert",
      alertStyle: "success",
      alertDismissAfter: 3000,
      updatePage: false,
      showModal: false,

      levelVisibility: level.visibility,
      levelName: level.name,
      levelPOIs: level.Locations,
    });
  },

  render() {
    return (
      <div>
        <ButtonToolbar style={ { marginBottom: "10px" } } >
          <Button bsStyle='primary' onClick={this.handleSaveClick} >
            <Glyphicon glyph="cloud-upload" /> Stockwerk speichern und zurück zur Liste
          </Button>
          <IndexLinkContainer to={ { pathname: "/campusses/" + this.props.params.id } } >
            <Button bsStyle='default' >
              <Glyphicon glyph="arrow-left" /> Zurück zur Stockwerk-Liste
            </Button>
          </IndexLinkContainer>
        </ButtonToolbar>
        <LevelPage
          header={
            this.props.dispName + (this.props.params.lid == "new"
            ? " hinzufügen"
            : " editieren")
          }
          components={this.props.components}
          visibility={this.state.levelVisibility}
          levelName={this.state.levelName}
          levelPOIs={this.state.levelPOIs}
          onChange={this.handleChange}
          updateLevelPage={this.state.updateLevelPage}
        />
        {
          this.state.showAlert ?
            <Alert
              bsStyle={this.state.alertStyle}
              onDismiss={this.handleAlertDismiss}
              dismissAfter={this.state.alertDismissAfter}
              style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
            >
              {this.state.alertMessage}
            </Alert>
          : undefined
        }
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton >
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

module.exports = LevelForm;
