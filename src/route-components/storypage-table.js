import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import tableMixin from '../mixins/table-mixin';

import ClassTable from '../components/class-table';

import {
  Col, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

var StoryPageTable = React.createClass({

  mixins: [tableMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "StoryPage",
      parseAttributeName: "StoryPages",
      dispName : "StoryPage",
      pluralName: "StoryPages",
      columnNames: ["Sprechblasentext","Kastentext"],
      id: "storyPageList",
    }
  },

  getStoryPageIds(job) {
    var storyPageIds = [];

    if(job && job.StoryPages) {
      for (var i = 0; i < job.StoryPages.length; i++) {
        storyPageIds.push(job.StoryPages[i].objectId);
      }
    }
    return storyPageIds;
  },

  observe(props,state) {
    if(props.job) {
      return {
        storyPages: (new Parse.Query("StoryPage"))
                      .include(["bubbleText","boxText"])
                      .containedIn("objectId",this.getStoryPageIds(props.job))
                      .ascending("createdAt")
      };
    } else {
      return;
    }
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.job) {
      this.setState({ updatePage: true });
    }
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(!this.state.classRoute)
      this.setState({
        classRoute: "/jobs/" + this.props.params.id + "/storypages",
        updatePage: true
      });

    if(
      this.state.initializeClassTableData == true
      && this.data.storyPages != undefined
    )
      this.initializeClassTableData();
  },

  initializeClassTableData() {
    if(this.pendingQueries().length == 0) {
      var classTableColProps = [
        { name: "bubbleText", type: "object" },
        { name: "boxText", type: "object" },
      ];
      var classTableData = this.generateClassTableData(classTableColProps,this.data.storyPages);

      this.setState({
        classTableData: classTableData,
        initializeClassTableData: false,
        updatePage: true
      });
    }
  },

  storyPageDelete() {
    this.closeModal();
    if(this.state.selectedClasses) {
      var batch = new ParseReact.Mutation.Batch();
      var classesToRemove = [];
      var selectedClasses = [];

      this.alertBeforeMutationDispatch("Löschen...");

      for(var i = 0; i < this.state.selectedClasses.length; i++) {
        selectedClasses.push(this.state.selectedClasses[i]);
        if(selectedClasses[i] != undefined) {
          classesToRemove.push({
            className: this.props.parseClassName,
            objectId: this.state.selectedClasses[i]
          });
          ParseReact.Mutation.Destroy(classesToRemove[i])
          .dispatch({batch: batch})
          .then(function(selectedClasses,index) {
            selectedClasses[index] = undefined;
          }.bind(this,selectedClasses,i));
        }
      }
      if(this.state.storyPagesRemoved != true) {
        ParseReact.Mutation.Remove(this.props.job,this.props.parseAttributeName,classesToRemove)
        .dispatch({batch: batch})
        .then(function() {
          this.setState({storyPagesRemoved: true});
        }.bind(this));
      }

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(
          this.mutationDeleteSuccess,
          this.mutationDeleteError.bind(this,selectedClasses)
        );
      } else {
        this.mutationDeleteSuccess();
      }
    }
  },

  render() {
    return (
      <div>
        {
          this.props.job ?
            <ClassTable
              id={this.props.id}
              classRoute={this.state.classRoute}
              displayName={this.props.dispName}
              columnNames={this.props.columnNames}
              data={this.state.classTableData}
              selectedClasses={this.state.selectedClasses}
              onClassChecked={this.handleClassesSelected}
              onDeleteClick={this.handleDeleteClick}
            />
          : <Alert bsStyle='info' style={ { marginBottom: "0" } } >
              <b><Glyphicon glyph="info-sign" />&nbsp;StoryPages können erst nach der Erstellung des Jobs angelegt werden.</b>
            </Alert>
        }
        {
          this.state.showAlert ?
            <Alert
              bsStyle={this.state.alertStyle}
              onDismiss={this.handleAlertDismiss}
              dismissAfter={this.state.alertDismissAfter}
              style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
            >
              {this.state.alertMessage}
            </Alert>
          : undefined
        }
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            <Button bsStyle="primary" onClick={this.storyPageDelete}>
              {this.state.secondButtonText}
            </Button>
            <Button onClick={this.closeModal}>
              Abbrechen
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports = StoryPageTable;
