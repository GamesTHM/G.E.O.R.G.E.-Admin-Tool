import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import formMixin from '../mixins/form-mixin';

import NavSideBar from '../components/nav-sidebar';
import PageContent from '../components/page-content';

import FactionPage from '../views/faction';

import {
  NavItem, Alert, Modal, Button, ButtonToolbar, Glyphicon
} from 'react-bootstrap';

import { LinkContainer } from 'react-router-bootstrap';

var FactionForm = React.createClass({

  mixins: [formMixin, ParseReact.Mixin],

  getDefaultProps() {
    return {
      parseClassName: "Faction",
      dispName : "Fraktion",
      classRoute: "/factions",

      components: [
        { description: "Fraktions-Name", id: "factionName" },
        { description: "Fraktions-Farben", id: "factionColors" },
        { description: "Fraktions-Geschichte", id: "factionLore" },
        { description: "Fraktions-Bild", id: "factionImage" },
        { description: "Fraktions-Icon", id: "factionIcon" },
      ],
    }
  },

  observe() {
    if(this.props.params.id != "new") {
      return {
        faction: (new Parse.Query(this.props.parseClassName))
                  .include(["name","headerimage","logoicon","lore.contentStyle"])
                  .equalTo("objectId",this.props.params.id)
      };
    } else {
      return;
    }
  },

  componentDidMount() {
    if(this.props.params.id == "new") this.setDefaultFactionValues();
  },

  componentDidUpdate() {
    if(this.state.updatePage == true)
      this.setState({updatePage: false});

    if(this.state.createClass == true) {
      this.setState({createClass: false});
      this.createFaction();
    }
    if(this.state.changeClass == true) {
      this.setState({changeClass: false});
      this.changeFaction();
    }

    if( this.state.initializeClassPageData == true
        && this.props.params.id != "new"
        && this.pendingQueries().length == 0
      )
      this.onFactionFetched(this.data.faction[0]);
  },

  onFactionFetched(faction) {
    this.setState({
      faction: faction,
      factionVisibility: faction.visibility,
      factionName: faction.name,
      factionImage: faction.headerimage,
      factionIcon: faction.logoicon,
      factionColors: {
        mainColor: faction.mainColor,
        textColor: faction.textColor
      },
      factionLore: {
        content: faction.lore && faction.lore.content ? faction.lore.content : undefined,
        contentStyle: faction.lore && faction.lore.contentStyle ? faction.lore.contentStyle : undefined
      },

      initializeClassPageData: false,
      updatePage: true,
    });
  },

  setDefaultFactionValues() {
    this.setState({
      faction: undefined,
      factionVisibility: false,
      factionName: undefined,
      factionImage: undefined,
      factionIcon: undefined,
      factionColors: {
        mainColor: "#fff",
        textColor: "#000"
      },
      factionLore: undefined,

      updatePage: true,
    });
  },

  createPointerAttributes(batch) {
    var factionNameMutation = this.toLocalizedStringCreateMutation(this.state.factionName,this.state.factionNameChanges);
    if(factionNameMutation)
      factionNameMutation.dispatch({batch: batch})
        .then(function(localizedString) {
          this.setState({ factionNameChanges: localizedString });
        }.bind(this));

    var factionLoreMutation =
      this.toWebElementCreateMutation(this.state.faction ? this.state.faction.lore : undefined,this.state.factionLoreChanges);
    if(factionLoreMutation)
      factionLoreMutation.dispatch({batch: batch})
        .then(function(webElement) {
          this.setState({ factionLoreChanges: webElement });
        }.bind(this));
  },

  createFaction() {
    var locationACL = new Parse.ACL();
    locationACL.setPublicReadAccess(true);
    locationACL.setRoleReadAccess("Admin",true);
    locationACL.setRoleWriteAccess("Admin",true);
    ParseReact.Mutation.Create(this.props.parseClassName, {
      visibility: this.state.visibilityChanges ? this.state.visibilityChanges : false,
      mainColor: this.state.factionColors ? this.state.factionColors.mainColor : "#ffffff",
      textColor: this.state.factionColors ? this.state.factionColors.textColor : "#000000",
      headerimage: this.state.factionImageChanges,
      logoicon: this.state.factionIconChanges,
      name: this.state.factionNameChanges,
      lore: this.state.factionLoreChanges,
      ACL: locationACL
    }).dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  changeFaction() {
    var changes = {};

    if(this.state.hasOwnProperty("visibilityChanges")) changes.visibility = this.state.visibilityChanges;
    if(this.state.factionColorsChanges && this.state.factionColorsChanges.mainColor)
      changes.mainColor = this.state.factionColorsChanges.mainColor;
    if(this.state.factionColorsChanges && this.state.factionColorsChanges.textColor)
      changes.textColor = this.state.factionColorsChanges.textColor;
    if(this.state.factionImageChanges) changes.headerimage = this.state.factionImageChanges;
    if(this.state.factionIconChanges) changes.logoicon = this.state.factionIconChanges;
    if(this.state.factionNameChanges && this.state.factionNameChanges.objectId) changes.name = this.state.factionNameChanges;
    if(this.state.factionLoreChanges && this.state.factionLoreChanges.objectId) changes.lore = this.state.factionLoreChanges;

    ParseReact.Mutation.Set(this.state.faction,changes)
    .dispatch().then(this.mutationSaveSuccess,this.mutationSaveError);
  },

  handleSaveClick() {
    this.closeModal();
    if(this.state.factionName == undefined && this.state.factionNameChanges == undefined) {
      this.setState({
        showModal: true,
        modalTitle: this.props.dispName + "-Name fehlt",
        modalText: "Ohne " + this.props.dispName + "-Name - ohne mich.",
        showSecondButton: false
      });
      return;
    }

    this.alertBeforeMutationDispatch("Speichern...");

    var batch = new ParseReact.Mutation.Batch();
    this.createPointerAttributes(batch);

    if(this.state.faction == undefined) {
      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ createClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({createClass: true});
      }
    } else {
      var factionNameMutation, factionLoreMutation;

      if(this.state.factionNameSet != true)
        factionNameMutation = this.toLocalizedStringSetMutation(this.state.factionName,this.state.factionNameChanges);
      if(factionNameMutation != undefined)
        factionNameMutation.dispatch({batch: batch}).then(function() {
          this.setState({factionNameSet: true});
        }.bind(this));

      if(this.state.factionLoreSet != true)
        factionLoreMutation =
          this.toWebElementSetMutation(this.state.faction ? this.state.faction.lore : undefined,this.state.factionLoreChanges);
      if(factionLoreMutation)
        factionLoreMutation.dispatch({batch: batch}).then(function() {
          this.setState({factionLoreSet: true});
        }.bind(this));

      if(batch.getNumberOfRequests() != 0) {
        batch.dispatch().then(function() {
          this.setState({ changeClass: true });
        }.bind(this), this.mutationSaveError);
      } else {
        this.setState({changeClass: true});
      }
    }

  },

  render() {
    var sidebarItems = this.props.components.map(function(sidebarItem) {
      return (
        <NavItem
          key={sidebarItem.id}
          href={"#" + sidebarItem.id}
          className="sidebar-item"
        >
          {sidebarItem.description}
        </NavItem>
      );
    });

    return (
      <div className="FactionPage">
        <NavSideBar>
          {sidebarItems}
        </NavSideBar>
        <PageContent>
          <div>
            <ButtonToolbar style={ { marginBottom: "10px" } } >
              <Button bsStyle='primary' onClick={this.handleSaveClick} >
                <Glyphicon glyph="cloud-upload" /> Speichern und zurück zur Liste
              </Button>
              <LinkContainer to={ { pathname: this.props.classRoute } } >
                <Button bsStyle='default' >
                  <Glyphicon glyph="arrow-left" /> Zurück zur Liste
                </Button>
              </LinkContainer>
            </ButtonToolbar>
            <FactionPage
              header={
                this.props.dispName + (this.props.params.id == "new"
                ? " hinzufügen"
                : " editieren")
              }
              components={this.props.components}
              visibility={this.state.factionVisibility}
              factionName={this.state.factionName}
              factionImage={this.state.factionImage}
              factionIcon={this.state.factionIcon}
              factionLore={this.state.factionLore}
              factionColors={this.state.factionColors}
              onChange={this.handleChange}
              updateFactionPage={this.state.updatePage}
            />
          </div>
          {
            this.state.showAlert ?
              <Alert
                bsStyle={this.state.alertStyle}
                onDismiss={this.handleAlertDismiss}
                dismissAfter={this.state.alertDismissAfter}
                style={ { position: "fixed", bottom: "0", zIndex: 1001, maxWidth: "80%" } }
              >
                {this.state.alertMessage}
              </Alert>
            : undefined
          }
        </PageContent>
        <Modal show={this.state.showModal} onHide={this.closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>
              {this.state.modalTitle}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.modalText}
          </Modal.Body>
          <Modal.Footer>
            {
              this.state.showSecondButton &&
              <Button bsStyle="primary" onClick={this.handleSaveClick}>
                Nochmal versuchen
              </Button>
            }
            <Button onClick={this.closeModal}>
              { this.state.showSecondButton ? "Abbrechen" : "OK" }
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

});

module.exports = FactionForm;
