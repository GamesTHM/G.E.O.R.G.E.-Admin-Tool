import React from 'react';
import { Parse } from 'parse';

import NavTopBar from '../components/nav-topbar';

var App = React.createClass({

  // ask for `router` from context
  contextTypes: {
    router: React.PropTypes.object
  },

  handleLogoutClick() {
    Parse.User.logOut();
    this.context.router.push('/');
  },

  render() {
    return (
      <div>
        <NavTopBar
          onLogoutClick={this.handleLogoutClick}
          user={Parse.User.current()}
        />
        {this.props.children}
      </div>
    )
  }
});

module.exports = App;
