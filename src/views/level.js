import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import SmartInput from '../components/smart-input';
import ComboboxWithTooltip from '../components/combobox-with-tooltip';
import Multiselect from '../components/multiselect';

import { Panel, Input, Button, Glyphicon } from 'react-bootstrap';

var LevelPage = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    return {
      levelPOIs: (new Parse.Query("Location")).include("name"),
      campusFloorLevels: (new Parse.Query("CampusFloorLevel")).include("name"),
    };
  },

  getInitialState() {
    return {
      selectedPOIsInitialized: false,
      nameOptionsInitialized: false,
      nameOptions: [],
      levelPOIs: [],
      updateComponent: false
    };
  },

  updateComponentStates(properties) {
    this.setState({
      selectedPOIsInitialized: false,
      selectedPOIs: [],
      levelPOIs: []
    });
  },

  componentDidMount() {
    this.updateComponentStates(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateComponentStates(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextProps.updateLevelPage ||
      nextState.updateComponent
    );
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.pendingQueries().length == 0) {
      var updateComponent = false;

      if(this.state.selectedPOIsInitialized == false) {
        this.initializeSelectedPOIs();
        updateComponent = true;
      } else {
        updateComponent = this.updateLevelPOIs();
      }

      if(this.state.nameOptionsInitialized == false) {
        this.initializeNameOptions();
        updateComponent = true;
      }

      this.setState({ updateComponent: updateComponent });

      if(this.state.updateComponent == true && updateComponent == false)
        this.setState({ updateComponent: false });
    }
  },

  initializeSelectedPOIs() {
    var selectedPOIs = [];

    if(this.props.levelPOIs && this.data.levelPOIs.length != 0) {
      for(var i=0; i < this.props.levelPOIs.length; i++)
        selectedPOIs.push({
          objectId: this.props.levelPOIs[i].objectId,
          name: this.findLevelPOIInList(this.props.levelPOIs[i], this.data.levelPOIs)[0].name.deu
        });
    }

    this.setState({
      selectedPOIs: selectedPOIs,
      levelPOIs: selectedPOIs,
      selectedPOIsInitialized: true
    });
  },

  findLevelPOIInList(levelPOI,list) {
    return (
      list.filter(
        function(poi) {
          return poi.objectId == this.objectId || poi.id == this.objectId;
        },levelPOI
      )
    );
  },

  updateLevelPOIs() {
    var levelPOIs = this.state.levelPOIs.slice(), updateComponent = false;
    var levelPOIFromList;
    var isLevelPOIInList = false;

    for(var i=0 ; i < this.data.levelPOIs.length; i++) {
      levelPOIFromList = this.findLevelPOIInList(this.data.levelPOIs[i],levelPOIs);
      isLevelPOIInList = levelPOIFromList.length != 0;
      if( !isLevelPOIInList && this.data.levelPOIs[i].name )
      {
        updateComponent = true;
        levelPOIs.push({
          objectId: this.data.levelPOIs[i].objectId,
          name: this.data.levelPOIs[i].name.deu
        });
      }
    }
    if(updateComponent)
      this.setState({ levelPOIs: levelPOIs });

    return updateComponent;
  },

  initializeNameOptions() {
    var nameOptionIds = [], nameOptions = [];
    for(var i=0; i < this.data.campusFloorLevels.length; i++) {
      if(nameOptionIds.indexOf(this.data.campusFloorLevels[i].name.objectId) == -1 ) {
        nameOptionIds.push(this.data.campusFloorLevels[i].name.objectId);
        nameOptions.push(this.data.campusFloorLevels[i].name);
      }
    }
    this.setState({nameOptions: nameOptions, nameOptionsInitialized: true});
  },

  onVisibilityChange(checked) {
    this.props.onChange("visibility", checked);
  },

  onNameChange(name) {
    this.props.onChange("levelName", name);
  },

  onMSChange(selection) {
    this.props.onChange("levelPOIs", selection);
  },

  render() {
    return (
      <div className="LevelPage">
        <Panel
          id={this.props.components[0].id}
          header={this.props.header}
          bsStyle='primary'
        >
          <SmartInput
            type="checkbox"
            checked={this.props.visibility}
            label="Freischalten"
            onChange={this.onVisibilityChange}
            checkLocalizedString={false}
          />
          <ComboboxWithTooltip
            id="levelName"
            label="Stockwerk-Bezeichnung"
            data={this.state.nameOptions}
            value={this.props.levelName}
            onChange={this.onNameChange}
          />
          <p><b>Point of Interests w&auml;hlen</b></p>
          <p>F&uuml;r den Fall, dass die Point of Interests noch nicht angelegt wurden, kann diese Auswahl auch &uuml;bersprungen werden.</p>
          <Multiselect
            data={this.state.levelPOIs}
            value={this.state.selectedPOIs}
            onChange={this.onMSChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = LevelPage;
