import React from 'react';
import { Parse } from 'parse';

import GeopointPicker from '../components/geopoint-picker';
import SmartInput from '../components/smart-input';

import { Panel, Input, Button } from 'react-bootstrap';

var PluginPage = React.createClass({

    shouldComponentUpdate(nextProps,nextState) {
        return (
            nextProps.updatePluginPage == true
        );
    },

    onVisibilityChange(checked) {
        this.props.onChange("visibility",checked);
    },

    onNameChange(name) {
        this.props.onChange("pluginName",name);
    },
    onDelete(test){
        this.props.onChange("delete",true);
    },
    onFileChoose(files){
        for (var i = 0, file; file = files[i]; i++) {
            if(file.type != "application/zip")
            {
                console.log("File Type Error");
            }
            else
            {
                name = file.name;
                name = name.substr(0, name.lastIndexOf('.'));

                this.props.onChange("pluginName",name);
                this.props.onChange("visibility",false);
                this.props.onChange("pluginFile", file);
            }
        }
    },

    render() {
        if (this.props.pluginName != undefined && this.props.pluginName.deu != "") {
            return (
                //Es handelt sich um ein vorhandens Plugin
                //Es kann upgedatetet, aktiviert oder deaktiviert werden.
                <div className="PluginPage">
                    <Panel
                        id={this.props.components[0].id}
                        bsStyle='primary'
                        header={this.props.header}
                    >
                        <SmartInput
                            type="checkbox"
                            checked={this.props.pluginVisibility}
                            label="Aktiv"
                            onChange={this.onVisibilityChange}
                            checkLocalizedString={false}
                        />
                        <Button
                            bsStyle='default'
                            onClick={this.onDelete}
                        >
                            Löschen
                        </Button>
                    </Panel>
                </div>
            );
        }
        else {
            return (
                <div className="PluginPage">
                    <Panel
                        id={this.props.components[0].id}
                        bsStyle='primary'
                        header={this.props.header}
                    >

                        <SmartInput
                            type="file"

                            label="Plugin-ZIP"
                            onChange={this.onFileChoose}
                            on
                            checkLocalizedString={false}
                        />
                    </Panel>
                </div>
            );
        }
    }
});

module.exports = PluginPage;
