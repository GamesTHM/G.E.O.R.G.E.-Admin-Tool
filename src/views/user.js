import React from 'react';

import SmartInput from '../components/smart-input';

import { Panel } from 'react-bootstrap';

var UserPage = React.createClass({

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.updateUserPage;
  },

  handleNameChange(name) {
    this.props.onChange("userName",name);
  },

  handlePasswordChange(password) {
    this.props.onChange("password",password);
  },

  render() {
    return (
      <div className="UserPage">
        <Panel
          id={this.props.components[0].id}
          bsStyle='primary'
          header={this.props.header}
        >
          <SmartInput
            type="text"
            value={this.props.userName}
            label="Benutzerame"
            onChange={this.handleNameChange}
            checkLocalizedString={false}
          />
          <SmartInput
            type="text"
            placeholder="Das vorhandene Passwort wird hier nicht angezeigt"
            label="Passwort"
            onChange={this.handlePasswordChange}
            checkLocalizedString={false}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = UserPage;
