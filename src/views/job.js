import React from 'react';
import Parse from 'parse';
import ParseReact from 'parse-react';

import ComboboxWithTooltip from '../components/combobox-with-tooltip';
import ImagePicker from '../components/image-picker';
import SmartInput from '../components/smart-input';
import NumberPicker from '../components/number-picker';

import Action from './action';
import Params from './param';

import { Panel, Row, Col } from 'react-bootstrap';

var JobPage = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    return {
      jobs: (new Parse.Query("Job")).include("description"),
    };
  },

  getInitialState() {
    return {
      descriptionOptions: [],
    };
  },

  updateComponentStates(properties) {
    if(this.refs.jobFinishedAction)
      this.refs.jobFinishedAction.updateState(properties.jobFinishedAction);
  },

  componentDidMount() {
    this.updateComponentStates(this.props);
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.updateJobPage) this.updateComponentStates(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.updateJobPage ||
      this.state.descriptionOptions.length != nextState.descriptionOptions.length;
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.data.jobs.length != 0) {
      var descriptionOptionIds = [], descriptionOptions = [];
      for(var i=0; i < this.data.jobs.length; i++) {
        if(
          this.data.jobs[i].description &&
          descriptionOptionIds.indexOf(this.data.jobs[i].description.objectId) == -1
        ) {
          descriptionOptionIds.push(this.data.jobs[i].description.objectId);
          descriptionOptions.push(this.data.jobs[i].description);
        }
      }
      if(this.state.descriptionOptions.length != descriptionOptions.length)
        this.setState({descriptionOptions: descriptionOptions});
    }
  },

  onPointGainChange(pointGain) {
    this.props.onChange("jobPointGain", pointGain);
  },

  onEnergyGainChange(energyGain) {
    this.props.onChange("jobEnergyGain", energyGain);
  },

  onNameChange(name) {
    this.props.onChange("jobName", name);
  },

  onShortDescriptionChange(shortDescription) {
    this.props.onChange("jobShortDescription", shortDescription);
  },

  onDescriptionChange(description) {
    this.props.onChange("jobDescription", description);
  },

  onParamsChange(params) {
    this.props.onChange("jobParameters", params);
  },

  onActionChange(action) {
    this.props.onChange("jobFinishedAction", action);
  },

  onIconChange(icon) {
    this.props.onChange("jobIcon", icon);
  },

  onTimeFrameChange(timeFrame) {
    this.props.onChange("jobFinishTimeFrame", timeFrame);
  },

  render() {
    return (
      <div className="JobPage">
        <Panel
          id={this.props.components[0].id}
          header={this.props.header}
          bsStyle='primary'
        >
          <Row>
            <Col md={6} style={ { paddingTop: "7px", marginBottom: "20px" } } >
              <b>Belohnung f&uuml;r das Abschlie&szlig;en dieses Jobs:</b>
            </Col>
            <Col md={1} style={ { paddingTop: "7px", marginBottom: "20px", textAlign: "right" } } >
              <b>Punkte</b>
            </Col>
            <Col md={2} >
              <NumberPicker
                min={0}
                step={10}
                value={this.props.jobPointGain}
                onChange={this.onPointGainChange}
              />
            </Col>
            <Col md={1} style={ { paddingTop: "7px", marginBottom: "20px", textAlign: "right" } } >
              <b>Energie</b>
            </Col>
            <Col md={2} >
              <NumberPicker
                min={0}
                step={10}
                value={this.props.jobEnergyGain}
                onChange={this.onEnergyGainChange}
              />
            </Col>
          </Row>
          <SmartInput
            type="text"
            label="Job-Name"
            value={this.props.jobName ? this.props.jobName.deu : undefined }
            onChange={this.onNameChange}
            checkLocalizedString={false}
          />
          <SmartInput
            type="text"
            label="Job-Kurzbeschreibung"
            value={this.props.jobShortDescription ? this.props.jobShortDescription.deu : undefined}
            onChange={this.onShortDescriptionChange}
            checkLocalizedString={false}
          />
          <ComboboxWithTooltip
            id="jobDescription"
            label="Job-Beschreibung"
            data={this.state.descriptionOptions}
            value={this.props.jobDescription}
            onChange={this.onDescriptionChange}
          />
          <Params
            id={this.props.components[1].id}
            header={this.props.components[1].description}
            jobParameters={this.props.jobParameters}
            jobFinishTimeFrame={this.props.jobFinishTimeFrame}
            onChange={this.onParamsChange}
            onTimeFrameChange={this.onTimeFrameChange}
          />
          <Action
            ref="jobFinishedAction"
            id={this.props.components[2].id}
            header={this.props.components[2].description}
            onChange={this.onActionChange}
          />
          <ImagePicker
            id={this.props.components[3].id}
            header={this.props.components[3].description}
            type="Icon"
            activeImage={this.props.jobIcon}
            onChange={this.onIconChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = JobPage;
