import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import WYSIWYGEditor from '../components/wysiwyg-editor';
import SmartInput from '../components/smart-input';

import { Panel, Input, Button, Glyphicon, Alert } from 'react-bootstrap';

import { SelectList } from 'react-widgets';

var Action = React.createClass({

  values: {
    jobActionsMap: {}
  },

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    return {
      jobActions: (new Parse.Query("Action").exists("description")),
      jobActionWebElement: (new Parse.Query("WebElement").equalTo("objectId",state.information)),
      jobActionLocalizedString: (new Parse.Query("LocalizedString").equalTo("objectId",state.information))
    };
  },

  getInitialState() {
    return {
      information: "",
      selectedAction: undefined,
      selectedIndex: 0,
      type: 0,
      localizedString: undefined,
      localizedStringChanges: undefined,
      edit: true,
      webElement: undefined
    };
  },

  updateState(properties) {
    if(properties == undefined) {
      this.setState(this.getInitialState());
    } else if(properties.hasOwnProperty("edit")) {
      this.setState({
        selectedAction : properties.selectedAction ? properties.selectedAction : this.state.selectedAction,
        selectedIndex: properties.selectedIndex != undefined ? properties.selectedIndex : this.state.selectedIndex,
        type: properties.type != undefined ? properties.type : this.state.type,
        localizedString: properties.localizedString ? properties.localizedString : this.state.localizedString,
        localizedStringChanges: properties.localizedStringChanges ? properties.localizedStringChanges : this.state.localizedStringChanges,
        edit: properties.edit != undefined ? properties.edit : this.state.edit,
        webElement: properties.webElement ? properties.webElement : this.state.webElement
      });
    } else if(!properties.hasOwnProperty("header")) {
      this.setState({
        initLocalizedString: true,
        information: properties.information,
        type: properties.type,
        edit: false
      });
    }
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    var nextInformation = nextProps.value ? nextProps.value.information : undefined;
    var thisInformation = this.props.value ? this.props.value.information : undefined;
    var nextActionLocalizedString = nextState.actionLocalizedString && nextState.actionLocalizedString.deu
      ? nextState.actionLocalizedString.deu
      : nextState.actionLocalizedString;
    var thisActionLocalizedString = this.state.actionLocalizedString && this.state.actionLocalizedString.deu
      ? this.state.actionLocalizedString.deu
      : this.state.actionLocalizedString;

    return (
      nextInformation != thisInformation ||
      nextState.information != this.state.information ||
      nextState.selectedIndex != this.state.selectedIndex ||
      nextActionLocalizedString != thisActionLocalizedString ||
      nextState.type != this.state.type ||
      nextState.edit != this.state.edit ||
      nextState.loadWebElement == true
    );
  },

  componentDidUpdate(prevProps,prevState) {
    for(var i = 0; i< this.data.jobActions.length; i++)
      this.values.jobActionsMap[this.data.jobActions[i].information] = i;

    var selectedIndex = this.values.jobActionsMap[this.state.information] != undefined ?
      this.values.jobActionsMap[this.state.information] + 2 : undefined;

    if( selectedIndex && selectedIndex != this.state.selectedIndex )
      this.setState({ selectedIndex: selectedIndex });

    if(this.data.jobActionLocalizedString.length != 0) {
      // this.refs.actionLocalizedString.setValue(this.data.jobActionLocalizedString[0],
      //   this.state.localizedStringChanges ? this.state.localizedStringChanges : this.data.jobActionLocalizedString[0]);
      this.setState({
        actionLocalizedStringInit: this.data.jobActionLocalizedString[0],
        actionLocalizedString: this.state.localizedStringChanges
          ? this.state.localizedStringChanges
          : this.data.jobActionLocalizedString[0]
      });

      if(this.state.localizedString == undefined || this.state.localizedString.deu != this.data.jobActionLocalizedString[0].deu)
        this.setState({
          localizedString: this.data.jobActionLocalizedString[0],
          initLocalizedString: false,
        },!this.state.initLocalizedString ? this.onChange : undefined);
    } else if(this.state.type == 0) {
      // this.refs.actionLocalizedString.setValue(this.state.localizedString,this.state.localizedStringChanges);
      this.setState({
        actionLocalizedStringInit: this.state.localizedString,
        actionLocalizedString: this.state.localizedStringChanges
      });
    }

    if(this.state.loadWebElement) {
      this.refs.jobActionWebElement.setWebElement(this.state.webElement);
      this.setState({loadWebElement: false});
    }
  },

  loadWebElement(){
    this.setState({loadWebElement: true});
  },

  onChange() {
    this.props.onChange({
      selectedAction: this.state.selectedAction,
      selectedIndex: this.state.selectedIndex,
      type: this.state.type,
      localizedString: this.state.localizedString,
      localizedStringChanges: this.state.localizedStringChanges,
      edit: this.state.edit,
      webElement: this.state.webElement
    });
  },

  onActionChange(value) {
    var selectedAction = value >= 2 ? this.data.jobActions[value-2] : undefined;

    this.setState({
      selectedAction: selectedAction,
      type: selectedAction ? selectedAction.type : value,
      localizedString: undefined,
      localizedStringChanges: undefined,
      webElement: undefined,
      information: selectedAction ? selectedAction.information : "",
      selectedIndex: value,
      edit: value == 0 ? true : false,
    },this.onChange);
  },

  onJobActionLocalizedStringChange(value) {
    this.setState({
      type: 0,
      localizedStringChanges: value,
    },this.onChange);
  },

  onJobActionWebElementChange(jobActionWebElement) {
    this.setState({
      type: 1,
      webElement: jobActionWebElement,
    },this.onChange)
  },

  onEditClick() {
    this.setState({
      edit: !this.state.edit,
      localizedStringChanges: undefined,
    },this.onChange);
  },

  render() {
    var jobActions = this.data.jobActions.map(function(action,i) {
      return (
        <option key={action.objectId} value={i} >
          {action.description}
        </option>
      );
    });

    return (
      <Panel
        id={this.props.id}
        header={this.props.header}
        bsStyle="primary"
      >
        <SmartInput
          type='select'
          label='Aktion ausw&auml;hlen'
          selectedIndex={this.state.selectedIndex}
          onChange={this.onActionChange}
        >
          <option value={-2} >Neue Lokalisierungstext-Aktion</option>
          <option value={-1} >Neue Web-Element-Aktion</option>
          {jobActions}
        </SmartInput>
        {
          this.state.type == 0 ?
            <SmartInput
              // ref="actionLocalizedString"
              type="text"
              label="R&uuml;ckmeldung f&uuml;r den Benutzer nach Beendigung des Jobs"
              initialValue={this.state.actionLocalizedStringInit}
              value={this.state.actionLocalizedString}
              onChange={this.onJobActionLocalizedStringChange}
              checkLocalizedString={false}
              disabled={!this.state.edit}
              buttonBefore={
                this.state.selectedIndex != 0 ?
                  <Button active={this.state.edit} onClick={this.onEditClick} >
                    <Glyphicon glyph="edit" /> Editieren
                  </Button>
                : undefined
              }
            />
          : undefined
        }
        {
          this.state.edit == true && this.state.selectedIndex != 0 ?
            <Alert bsStyle='warning' style={ { marginBottom: "0" } } >
              <h4><Glyphicon glyph="alert" />&nbsp;Achtung!</h4>
              Alle Jobs, die diese Aktion verwenden werden von diesen Änderungen betroffen sein.<br/>
              Falls die &Auml;nderungen nur für diesen Job gelten sollen, legen Sie eine neue Aktion an.
            </Alert>
          : undefined
        }
        {
          this.state.type == 1 ?
            <WYSIWYGEditor
              ref="jobActionWebElement"
              id={"jobActionWebElement"}
              header={"Web-Element der Aktion"}
              onChange={this.onJobActionWebElementChange}
              onMounted={this.loadWebElement}
            />
          : undefined
        }
      </Panel>
    );
  },
});

module.exports = Action;
