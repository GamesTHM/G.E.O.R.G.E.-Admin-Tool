import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import DatePicker from '../components/date-picker';
import WYSIWYGEditor from '../components/wysiwyg-editor';
import ImagePicker from '../components/image-picker';
import SmartInput from '../components/smart-input';
import NumberPicker from '../components/number-picker';
import Multiselect from '../components/multiselect';

import { Panel, Input, Row, Col, Button, Modal } from 'react-bootstrap';

var QuestPage = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    if(!state.dontRefresh)
      return {
        questJobs: (new Parse.Query("Job")).include("name").descending("updatedAt"),
        questFactions: (new Parse.Query("Faction")).include("name").select("name"),
        questCampusses: (new Parse.Query("Campus")).include("name").select("name"),
      };
  },

  getInitialState() {
    return {
      releasable: false,
      showModal: false,
      selectedJobsInitialized: false,
      dontRefresh: false,
      questJobs: [],
      questCycleTime: 0,
      updateComponent: false
    };
  },

  updateComponentStates(properties) {
    this.setState({
      releasable: properties.questJobs != undefined,
      visibility: properties.visibility,
      questFeatured: properties.questFeatured,
      questCycleTime: properties.questCycleTime,
      questFactionSelectedIndex: 0,
      questFactionSelectedIndexInitialized: false,
      questCampusSelectedIndex: 0,
      questCampusSelectedIndexInitialized: false,
      questName: properties.questName,
      questShortDescription: properties.questShortDescription,
      selectedJobsInitialized: false,
      questJobs: [],
      selectedJobs: [],
      questTimeFrame: properties.questTimeFrame,
      questIcon: properties.questIcon,
      questImage: properties.questImage
    });
  },

  componentDidMount() {
    this.updateComponentStates(this.props);
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.updateQuestPage) this.updateComponentStates(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.updateQuestPage ||
      this.state.showModal != nextState.showModal ||
      nextState.updateComponent
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.pendingQueries().length == 0) {
      var updateComponent = false;

      if(this.state.selectedJobsInitialized == false) {
        this.initializeSelectedJobs();
        updateComponent = true;
      } else {
        updateComponent = this.updateQuestJobs();
      }

      if( this.state.questFactionSelectedIndexInitialized == false
          && this.props.questFaction) {
        this.initializeQuestFactionSelectedIndex();
        updateComponent = true;
      }
      if(this.state.questCampusSelectedIndexInitialized == false) {
        updateComponent = this.initializeQuestCampusSelectedIndex() || updateComponent;
      }

      this.setState({ updateComponent: updateComponent });

      if(this.state.updateComponent == true && updateComponent == false)
        this.setState({ updateComponent: false });
      if(this.state.dontRefresh == false) this.setState({ dontRefresh: true });
    }
  },

  initializeSelectedJobs() {
    var selectedJobs = [];

    if(this.props.questJobs && this.data.questJobs.length != 0) {
      for(var i=0; i < this.props.questJobs.length; i++)
        selectedJobs.push({
          objectId: this.props.questJobs[i].objectId,
          name: this.findQuestJobInList(this.props.questJobs[i], this.data.questJobs)[0].name.deu
        });
    }

    this.setState({
      selectedJobs: selectedJobs,
      questJobs: selectedJobs,
      releasable: selectedJobs.length != 0,
      selectedJobsInitialized: true
    });
  },

  findQuestJobInList(questJob,list) {
    return (
      list.filter(
        function(job) {
          return job.objectId == this.objectId || job.id == this.objectId;
        },questJob
      )
    );
  },

  updateQuestJobs() {
    var questJobs = this.state.questJobs.slice(), updateComponent = false;
    var questJobFromList;
    var isQuestInList = false;

    for(var i=0 ; i < this.data.questJobs.length; i++) {
      questJobFromList = this.findQuestJobInList(this.data.questJobs[i],questJobs);
      isQuestInList = questJobFromList.length != 0;
      if( !isQuestInList && this.data.questJobs[i].name )
      {
        updateComponent = true;
        questJobs.push({
          objectId: this.data.questJobs[i].objectId,
          name: this.data.questJobs[i].name.deu
        });
      }
    }
    if(updateComponent)
      this.setState({ questJobs: questJobs });

    return updateComponent;
  },

  initializeQuestFactionSelectedIndex() {
    for(var i = 0; i < this.data.questFactions.length; i++) {
      if(this.data.questFactions[i].objectId == this.props.questFaction.objectId) {
        this.setState({
          questFactionSelectedIndex: i+1,
          questFactionSelectedIndexInitialized: true
        });
        break;
      }
    }
  },

  initializeQuestCampusSelectedIndex() {
    var updateComponent = false;

    if(this.props.questCampus) {
      for(var i = 0; i < this.data.questCampusses.length; i++) {
        if(this.data.questCampusses[i].objectId == this.props.questCampus.objectId) {
          this.setState({
            questCampusSelectedIndex: i,
            questCampusSelectedIndexInitialized: true
          });
          updateComponent = true;
          break;
        }
      }
    } else if(this.props.questName) {
      //quest already loaded, but it has no campus
      //automatically "choose" the first option as selected
      this.props.onChange("questCampus",this.data.questCampusses[0]);
      this.setState({
        questCampusSelectedIndex: 0,
        questCampusSelectedIndexInitialized: true
      });
      updateComponent = true;
    } //else the quest was not loaded yet, so don't initialize campus
    return updateComponent;
  },

  onVisibilityChange(checked) {
    if(checked == true && this.state.releasable == false) {
      this.setState({
        showModal: true,
        visibility: false
      });
    } else {
      this.setState({visibility: checked});
      this.props.onChange("visibility", checked);
    }
  },

  onQuestFeaturedChange(checked) {
    this.setState({questFeatured: checked});
    this.props.onChange("questFeatured", checked);
  },

  onCycleTimeChange(cycleTime) {
    this.setState({questCycleTime: cycleTime});
    this.props.onChange("questCycleTime", cycleTime);
  },

  onQuestCampusChange(selectedIndex) {
    this.setState({questCampusSelectedIndex: selectedIndex});
    this.props.onChange("questCampus",this.data.questCampusses[selectedIndex]);
  },

  onQuestFactionChange(selectedIndex) {
    this.setState({questFactionSelectedIndex: selectedIndex});
    this.props.onChange("questFaction", selectedIndex != 0 ? this.data.questFactions[selectedIndex-1] : undefined);
  },

  onNameChange(value) {
    this.setState({ questName: value });
    this.props.onChange("questName", value);
  },

  onShortDescriptionChange(value) {
    this.setState({ questShortDescription: value });
    this.props.onChange("questShortDescription", value);
  },

  onMSChange(selection) {
    var isSomethingSelected = selection != undefined && selection.length != 0;

    this.setState({
      selectedJobs: selection,
      releasable: isSomethingSelected,
      visibility: isSomethingSelected ? this.state.visibility : false,
      updateComponent: !isSomethingSelected && this.state.visibility
    });
    this.props.onChange("questJobs", selection);
    if(selection == undefined || selection.length == 0)
      this.props.onChange("visibility", false);
  },

  onTimeFrameChange(timeFrame) {
    this.setState({questTimeFrame: timeFrame});
    this.props.onChange("questTimeFrame",timeFrame);
  },

  onDescriptionChange(description) {
    this.props.onChange("questDescription", description);
  },

  onImageChange(image) {
    this.setState({questImage: image});
    this.props.onChange("questImage",image);
  },

  onIconChange(icon) {
    this.setState({questIcon: icon});
    this.props.onChange("questIcon",icon);
  },

  closeModal() {
    this.setState({showModal: false});
  },

  render() {
    var questCampusses = this.data.questCampusses.map(function(questCampus) {
      return <option key={questCampus.objectId} >{questCampus.name.deu}</option>
    });

    var questFactions = this.data.questFactions.map(function(questFaction) {
      return <option key={questFaction.objectId} >{questFaction.name.deu}</option>
    });

    return (
      <div className="QuestPage">
        <Panel
          id={this.props.components[0].id}
          header={this.props.header}
          bsStyle='primary'
        >
          <Row>
            <Col md={2} >
              <SmartInput
                type="checkbox"
                label="Freischalten"
                checked={this.state.visibility}
                onChange={this.onVisibilityChange}
              />
            </Col>
            <Col md={2} >
              <SmartInput
                type="checkbox"
                label="Featured"
                checked={this.state.questFeatured}
                onChange={this.onQuestFeaturedChange}
              />
            </Col>
            <Col md={5} style={ { textAlign: "right", padding: "7px" } } >
              Quest-Wiederholungszyklus (0 f&uuml;r keine Wdh.)
            </Col>
            <Col md={2} >
              <NumberPicker
                min={0}
                value={this.state.questCycleTime}
                onChange={this.onCycleTimeChange}
              />
            </Col>
            <Col md={1} style={ { padding: "7px" } } >
              Tag(e)
            </Col>
          </Row>
          <Row>
            <Col md={6} >
              <SmartInput
                type='select'
                label='Campus-Zuordnung'
                selectedIndex={this.state.questCampusSelectedIndex}
                onChange={this.onQuestCampusChange}
              >
                {questCampusses}
              </SmartInput>
            </Col>
            <Col md={6} >
              <SmartInput
                type='select'
                label='Fraktions-Zuordnung (Fraktions-Quest)'
                selectedIndex={this.state.questFactionSelectedIndex}
                onChange={this.onQuestFactionChange}
              >
                <option>Keine Fraktion zuordnen</option>
                {questFactions}
              </SmartInput>
            </Col>
          </Row>
          <SmartInput
            type="text"
            value={this.state.questName ? this.state.questName.deu : undefined}
            label="Quest-Name"
            onChange={this.onNameChange}
            checkLocalizedString={false}
          />
          <SmartInput
            type="text"
            value={this.state.questShortDescription ? this.state.questShortDescription.deu : undefined}
            label="Quest-Kurzbeschreibung"
            onChange={this.onShortDescriptionChange}
            checkLocalizedString={false}
          />
          <p><b>Jobs w&auml;hlen</b></p>
          <p>F&uuml;r den Fall, dass die Jobs noch nicht angelegt wurden, kann diese Auswahl auch &uuml;bersprungen werden.</p>
          <Multiselect
            data={this.state.questJobs}
            value={this.state.selectedJobs}
            onChange={this.onMSChange}
          />
          <DatePicker
            id={this.props.components[1].id}
            header={this.props.components[1].description}
            wrapComponentIntoPanel={true}
            timeFrame={this.state.questTimeFrame}
            onChange={this.onTimeFrameChange}
          />
          <WYSIWYGEditor
            id={this.props.components[2].id}
            header={this.props.components[2].description}
            webElement={this.props.questDescription}
            template={this.props.questDescription ? undefined : "quest"}
            onChange={this.onDescriptionChange}
          />
          <ImagePicker
            id={this.props.components[3].id}
            header={this.props.components[3].description}
            type="Image"
            activeImage={this.state.questImage}
            onChange={this.onImageChange}
          />
          <ImagePicker
            id={this.props.components[4].id}
            header={this.props.components[4].description}
            type="Icon"
            showUnsetButton={true}
            activeImage={this.state.questIcon}
            onChange={this.onIconChange}
          />
        </Panel>
        <Modal show={this.state.showModal} onHide={this.closeModal} >
          <Modal.Header closeButton>
            <Modal.Title>
              Freischalten nicht m&ouml;glich
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Die Quest kann nicht freigeschaltet werden, wenn keine Jobs zugewiesen wurden.<br/>
            Sie kann allerdings im nicht freigeschalteten Modus gespeichert werden.
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.closeModal} >
              OK
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
});

module.exports = QuestPage;
