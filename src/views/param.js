import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import DatePicker from '../components/date-picker';
import ComboboxWithTooltip from '../components/combobox-with-tooltip';
import SmartInput from '../components/smart-input';

import Action from './action';

import {
  Panel, Row, Col, Label, Input, Button, Glyphicon
} from 'react-bootstrap';

var Params = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    return {
      pois: (new Parse.Query("Location")).include(["name","Campus.name"]),
      params: (new Parse.Query("Parameter"))
                .containedIn("type",[1000,1001,1002])
                .include("incorrectValueAction")
                .limit(1000),
    };
  },

  getInitialState() {
    return {
      codeAdvanced: false,
      poiAdvanced: false,
      useCode: false,
      usePoi: false,
      poiSelectedIndex: 0,
      unset: [],
      remove: [],
      update: true,
    };
  },

  updateState(properties) {
    this.replaceState(this.getInitialState());

    if( properties.jobFinishTimeFrame != undefined &&
       (properties.jobFinishTimeFrame.startDate != undefined ||
        properties.jobFinishTimeFrame.endDate != undefined) ) {
      // this.refs.useTimeFrame.getInputDOMNode().checked = true;
      this.setState({
        useTimeFrame: true,
        jobFinishTimeFrame: properties.jobFinishTimeFrame
      });
    } else {
      // this.refs.useTimeFrame.getInputDOMNode().checked = false;
      this.setState({useTimeFrame: false});
    }

    // if(this.refs.useCode) this.refs.useCode.checked = false;
    // if(this.refs.usePoi) this.refs.usePoi.checked = false;

    // this.refs.code.setValue(undefined,undefined);
    this.setState({ displayedCode: undefined });
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextState.useCode != this.state.useCode ||
      nextState.isCodeOptional != this.state.isCodeOptional ||
      nextState.usePoi != this.state.usePoi ||
      nextState.isPoiOptional != this.state.isPoiOptional ||
      nextState.codeAdvanced != this.state.codeAdvanced ||
      nextState.poiAdvanced != this.state.poiAdvanced ||
      nextState.poiSelectedIndex != this.state.poiSelectedIndex ||
      nextState.jobFinishTimeFrame != this.state.jobFinishTimeFrame ||
      nextState.useTimeFrame != this.state.useTimeFrame ||
      nextState.update == true
    );
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.state.update) this.setState({update: false});

    if(this.data.pois.length != 0 && this.data.params.length != 0) {
      if(this.state.paramMap == undefined || this.state.poiToParamMap == undefined || this.state.poiMap == undefined) {
        var paramMaps, paramMap, poiMap;
        paramMaps = this.initParamMapsAndUserInfoOptions();
        paramMap = paramMaps[0];
        poiMap = this.createPoiMap(paramMaps[1]);
        if(this.props.jobParameters != undefined) {
          var initStateValues = this.getInitStateValuesFromJobParams(poiMap,paramMap);
          this.setState(initStateValues, this.onInitStateValuesSet);
        }
      } else {
        this.checkUnsetOrRemove();
      }
    }
  },

  updateCodeAdvancedValues() {
    if(this.state.codeAdvanced == true) {
      // this.refs.codeUserInfo.updateState({value: this.state.codeUserInfo ? this.state.codeUserInfo : this.state.codeUserInfoInit});
      this.refs.codeIncorrectValueAction.updateState(
        this.state.codeIncorrectValueAction ? this.state.codeIncorrectValueAction : this.state.codeIncorrectValueActionInit
      );
    }
  },

  updatePoiAdvancedValues() {
    if(this.state.poiAdvanced == true) {
      // this.refs.poiUserInfo.updateState({value: this.state.poiUserInfo ? this.state.poiUserInfo : this.state.poiUserInfoInit});
      this.refs.poiIncorrectValueAction.updateState(
        this.state.poiIncorrectValueAction ? this.state.poiIncorrectValueAction : this.state.poiIncorrectValueActionInit
      );
    }
  },

  onInitStateValuesSet() {
    this.checkUnsetOrRemove();

    // this.refs.usePoi.checked = this.state.usePoi;
    // this.refs.useCode.checked = this.state.useCode;

    // this.refs.code.setValue(this.state.codeInit,this.state.codeInit);
    // this.setState({ displayedCode: this.state.codeInit });
  },

  checkUnsetOrRemove() {
    function findIndex(array,key,value) {
      for(var i = 0; i< array.length; i++) {
        if(array[i][key] == value) return i;
      }
      return -1;
    }

    if((this.state.usePoi == false || this.state.poiSelectedIndex == 0) &&
        this.state.useCode == false && this.props.jobParameters) {
      var updateState = this.state.remove.length != 0;
      var unset = this.state.unset.slice();

      if(unset.indexOf("Parameters") == -1 ) {
        updateState = true;
        unset.push("Parameters");
      }
      if(updateState) this.setState({ remove: [], unset: unset },this.onChange);
    } else if(this.state.useCode == false && this.state.usePoi == true && this.state.codeParamIndex != undefined) {
      var updateState = false;
      var remove = this.state.remove.slice();
      var ParameterIndex = findIndex(remove,"field","Parameters");

      if(ParameterIndex != -1) {
        if(remove[ParameterIndex].items == undefined) remove[ParameterIndex].items = [];
        var itemIndex = findIndex(remove[ParameterIndex].items,
          "objectId",this.props.jobParameters[this.state.codeParamIndex].objectId);
        if(itemIndex == -1) {
          updateState = true;
          remove[ParameterIndex].items.push(this.props.jobParameters[this.state.codeParamIndex]);
        }
      } else {
        updateState = true;
        remove.push({
          field: "Parameters",
          items: [this.props.jobParameters[this.state.codeParamIndex]]
        });
      }
      if(updateState) this.setState({ remove: remove },this.onChange);
    } else if(this.state.usePoi == false && this.state.poiParamIndex != undefined &&
              this.state.useCode == true && (this.state.code || this.state.codeInit) ) {
      var updateState = false;
      var remove = this.state.remove.slice();
      var ParameterIndex = findIndex(remove,"field","Parameters");

      if(ParameterIndex != -1) {
        if(remove[ParameterIndex].items == undefined) remove[ParameterIndex].items = [];
        var itemIndex = findIndex(remove[ParameterIndex].items,
          "objectId",this.props.jobParameters[this.state.poiParamIndex].objectId);
        if(itemIndex == -1) {
          updateState = true;
          remove[ParameterIndex].items.push(this.props.jobParameters[this.state.poiParamIndex]);
        }
      } else {
        updateState = true;
        remove.push({
          field: "Parameters",
          items: [this.props.jobParameters[this.state.poiParamIndex]]
        });
      }
      if(updateState) this.setState({ remove: remove },this.onChange);
    }

    if(((this.state.usePoi == true && this.state.poiSelectedIndex != 0) ||
        (this.state.useCode == true && (this.state.code || this.state.codeInit) )) && this.props.jobParameters) {
      var unset = this.state.unset.slice();

      if(unset.indexOf("Parameters") != -1 ) {
        unset.splice(unset.indexOf("Parameters"),1);
        this.setState({ unset: unset },this.onChange);
      }
    }

    if( (this.state.usePoi == true && this.state.poiParam == undefined &&
        this.state.useCode == true && this.props.jobParameters) ||
        (this.state.useCode == true && this.state.codeParamIndex != undefined && this.state.poiParamIndex == undefined) ||
        (this.state.usePoi == true && this.state.poiParam == undefined && this.state.poiParamIndex != undefined &&
        this.state.useCode == false && this.state.codeParamIndex == undefined)) {
      var remove = this.state.remove.slice();
      var ParameterIndex = findIndex(remove,"field","Parameters");

      if(ParameterIndex != -1) {
        remove.splice(ParameterIndex,1);
        this.setState({ remove: remove },this.onChange);
      }
    } else if(this.state.useCode == true && this.state.codeParamIndex != undefined) {
      var remove = this.state.remove.slice();
      var ParameterIndex = findIndex(remove,"field","Parameters");

      if(ParameterIndex != -1) {
        var itemIndex = findIndex(remove[ParameterIndex].items,
          "objectId",this.props.jobParameters[this.state.codeParamIndex].objectId);
        if(itemIndex != -1) {
          remove[ParameterIndex].items.splice(itemIndex,1);
          this.setState({ remove: remove },this.onChange);
        }
      }
    }
  },

  initParamMapsAndUserInfoOptions() {
    var userInfoOptions = [];
    var userInfoOptionsMap = {};
    var poiToParamMap = {};
    var paramMap = {};

    for(var i = 0; i < this.data.params.length; i++) {
      paramMap[this.data.params[i].objectId] = this.data.params[i];
      if( this.data.params[i].type == 1000 && poiToParamMap[this.data.params[i].correctValue] != undefined)
        console.log("Param with location " + this.data.params[i].correctValue + " already exists");
      poiToParamMap[this.data.params[i].correctValue] = this.data.params[i];
      if( this.data.params[i].LocalizedString &&
          userInfoOptionsMap[this.data.params[i].LocalizedString.objectId] == undefined) {
        userInfoOptionsMap[this.data.params[i].LocalizedString.objectId] = 1;
        userInfoOptions.push(this.data.params[i].LocalizedString);
      }
    }

    this.setState({
      userInfoOptions: userInfoOptions,
      paramMap: paramMap,
      poiToParamMap: poiToParamMap,
    });

    return [paramMap,poiToParamMap];
  },

  createPoiMap(poiToParamMap) {
    var j = 0, poiMap = {}, pois = [];

    for(var i = 0; i < this.data.pois.length; i++) {
      if(poiToParamMap[this.data.pois[i].objectId] == undefined)
        console.log(this.data.pois[i].name.deu + " has no Parameter object");
      else {
        poiMap[this.data.pois[i].objectId] = j++;
        pois.push(this.data.pois[i]);
      }
    }
    this.setState({
      poiMap: poiMap,
      pois: pois,
      update: true
    });

    return poiMap;
  },

  getInitStateValuesFromJobParams(poiMap,paramMap) {
    var initStateValues = {};
    var param;//, prevJobParameter;

    for(var i = 0; i < this.props.jobParameters.length; i++) {
      param = paramMap[this.props.jobParameters[i].objectId];
      if(param && param.type == 1000) {
        initStateValues.usePoi = true;
        initStateValues.poiParamIndex = i;
        initStateValues.poiSelectedIndexInit = poiMap[paramMap[param.objectId].correctValue] + 1;
        initStateValues.poiSelectedIndex = poiMap[paramMap[param.objectId].correctValue] + 1;
        initStateValues.isPoiOptionalInit = param.optional;
        // initStateValues.isPoiOptional = param.optional;
        initStateValues.poiUserInfoInit = param.LocalizedString;
        initStateValues.poiIncorrectValueActionInit = param.incorrectValueAction;
      }
      else if(param && param.type == 1001) {
        initStateValues.useCode = true;
        initStateValues.codeParamIndex = i;
        initStateValues.codeInit = param.correctValue;
        initStateValues.displayedCode = param.correctValue;
        initStateValues.isCodeOptionalInit = param.optional;
        // initStateValues.isCodeOptional = param.optional;
        initStateValues.codeUserInfoInit = param.LocalizedString;
        initStateValues.codeIncorrectValueActionInit = param.incorrectValueAction;
      }
    }
    return initStateValues;
  },

  onChange() {
    this.props.onChange({
      correctCode: this.state.code,
      codeParam: this.state.codeParam,
      poiParam: this.state.poiParam,
      isCodeOptional: this.state.isCodeOptional,
      isPoiOptional: this.state.isPoiOptional,
      codeUserInfo: this.state.codeUserInfo,
      poiUserInfo: this.state.poiUserInfo,
      codeIncorrectValueAction: this.state.codeIncorrectValueAction,
      poiIncorrectValueAction: this.state.poiIncorrectValueAction,
      unsetFields: this.state.unset,
      removeArrayItems: this.state.remove,
    });
  },

  onUseCodeChange(e) {
    if(e.target.checked != true) {
      this.setState({
        codeUserInfo: undefined,
        codeIncorrectValueAction: undefined,
        isCodeOptional: undefined,
      },this.onChange);
    }
    // this.refs.code.setValue(this.state.codeInit,this.state.codeInit);

    this.setState({
      code: undefined,
      displayedCode: this.state.codeInit,
      codeParam: undefined,
      useCode: e.target.checked,
      codeAdvanced: false,
    },this.onChange);
  },

  onUsePoiChange(e) {
    if(e.target.checked != true) {
      this.setState({
        poiUserInfo: undefined,
        poiIncorrectValueAction: undefined,
        poiSelectedIndex: this.state.poiSelectedIndexInit,
        isPoiOptional: undefined,
      },this.onChange);
    }

    this.setState({
      usePoi: e.target.checked,
      poiAdvanced: false,
      poiParam: undefined,
    },this.onChange);
  },

  onUseTimeFrameChange(checked) {
    this.onTimeFrameChange(undefined);
    this.setState({useTimeFrame: checked});
  },

  onCodeChange(code) {
    var codeParam, lowerCaseCode = code.toLowerCase();

    if(lowerCaseCode && lowerCaseCode != this.state.codeInit && this.state.codeParamIndex != undefined)
      codeParam = this.props.jobParameters[this.state.codeParamIndex]
    else codeParam = undefined;

    this.setState({
      code: lowerCaseCode != this.state.codeInit ? lowerCaseCode : undefined,
      displayedCode: code,
      codeParam: codeParam,
    },this.onChange);
  },

  onPoiChange(value) {
    var poiParam = value != 0 && value != undefined && value != this.state.poiSelectedIndexInit ?
      this.state.poiToParamMap[this.state.pois[value-1].objectId] : undefined;

    this.setState({
      poiParam: poiParam,
      poiSelectedIndex: value,
      poiUserInfoInit: poiParam ? poiParam.LocalizedString : undefined,
      poiUserInfo: undefined,
      poiIncorrectValueActionInit: poiParam ? poiParam.incorrectValueAction : undefined,
      poiIncorrectValueAction: undefined
    },function() {
      this.onChange();
      this.updatePoiAdvancedValues();
    }.bind(this));
  },

  onTimeFrameChange(timeFrame) {
    this.props.onTimeFrameChange(timeFrame);
    this.setState({jobFinishTimeFrame: timeFrame});
  },

  onCodeAdvancedClick() {
    this.setState({codeAdvanced: !this.state.codeAdvanced},this.updateCodeAdvancedValues);
  },

  onPoiAdvancedClick() {
    this.setState({poiAdvanced: !this.state.poiAdvanced},this.updatePoiAdvancedValues);
  },

  onCodeUserInfoChange(localizedString) {
    var codeParam;

    if(this.state.codeParam == undefined && this.state.codeParamIndex != undefined)
      codeParam = this.props.jobParameters[this.state.codeParamIndex];

    this.setState({
      codeParam: codeParam ? codeParam : this.state.codeParam,
      codeUserInfo: localizedString,
    },this.onChange);
  },

  onPoiUserInfoChange(localizedString) {
    var poiParam;

    if(this.state.poiParam == undefined && this.state.poiParamIndex != undefined)
      poiParam = this.props.jobParameters[this.state.poiParamIndex];

    this.setState({
      poiParam: poiParam ? poiParam : this.state.poiParam,
      poiUserInfo: localizedString,
    },this.onChange);
  },

  onCodeActionChange(actionChanges) {
    var codeParam;

    if(this.state.codeParam == undefined && this.state.codeParamIndex != undefined)
      codeParam = this.props.jobParameters[this.state.codeParamIndex];

    this.setState({
      codeParam: codeParam ? codeParam : this.state.codeParam,
      codeIncorrectValueAction: actionChanges,
    },this.onChange);
  },

  onPoiActionChange(actionChanges) {
    var poiParam;

    if(this.state.poiParam == undefined && this.state.poiParamIndex != undefined)
      poiParam = this.props.jobParameters[this.state.poiParamIndex];

    this.setState({
      poiParam: poiParam ? poiParam : this.state.poiParam,
      poiIncorrectValueAction: actionChanges,
    },this.onChange);
  },

  onIsCodeOptionalChange(e) {
    var codeParam;

    if(this.state.codeParam == undefined && this.state.codeParamIndex != undefined)
      codeParam = this.props.jobParameters[this.state.codeParamIndex];

    this.setState({
      isCodeOptional: e.target.checked,
      isCodeOptionalChanges: e.target.checked,
      codeParam: codeParam ? codeParam : this.state.codeParam,
    },this.onChange);
  },

  onIsPoiOptionalChange(e) {
    var poiParam;

    if(this.state.poiParam == undefined && this.state.poiParamIndex != undefined)
      poiParam = this.props.jobParameters[this.state.poiParamIndex];

    this.setState({
      isPoiOptional: e.target.checked,
      isPoiOptionalChanges: e.target.checked,
      poiParam: poiParam ? poiParam : this.state.poiParam,
    },this.onChange);
  },

  render() {
    var pois;

    if(this.state.pois != undefined)
      pois = this.state.pois.map(function(poi,i) {
        return (
          <option key={poi.objectId} value={i} >
            {poi.name.deu + " (" + poi.Campus.name.deu + ")"}
          </option>
        );
      });

    return (
      <Panel
        id={this.props.id}
        header={this.props.header}
        bsStyle='primary'
      >
        <Row>
          <Col
            md={6}
            style={
              this.state.codeAdvanced ?
                {
                  borderColor:"black",
                  borderStyle:"solid",
                  borderRadius:"10px",
                  borderWidth: "thin"
                }
              : { }
            }
          >
            <SmartInput
              // ref="code"
              type="text"
              label="Code"
              value={this.state.displayedCode}
              disabled={!this.state.useCode}
              onChange={this.onCodeChange}
              addonBefore={
                <input
                  // ref="useCode"
                  type="checkbox"
                  checked={this.state.useCode}
                  onChange={this.onUseCodeChange}
                />
              }
              buttonAfter={
                <Button
                  active={this.state.codeAdvanced}
                  disabled={!this.state.useCode}
                  onClick={this.onCodeAdvancedClick}
                >
                  <Glyphicon glyph="cog" /> Erweitert
                </Button>
              }
            />
            {
              this.state.codeAdvanced ?
                <SmartInput
                  // ref="codeOptional"
                  type="checkbox"
                  label="Optionaler Parameter"
                  checked={
                    this.state.isCodeOptional != undefined ?
                      this.state.isCodeOptional
                    : this.state.isCodeOptionalInit
                  }
                  onChange={this.onIsCodeOptionalChange}
                />
              : undefined
            }
            {
              this.state.codeAdvanced ?
                <ComboboxWithTooltip
                  id="codeUserInfo"
                  label="Information f&uuml;r den Spieler"
                  // ref="codeUserInfo"
                  data={this.state.userInfoOptions}
                  value={this.state.codeUserInfo ? this.state.codeUserInfo : this.state.codeUserInfoInit}
                  onChange={this.onCodeUserInfoChange}
                />
              : undefined
            }
            {
              this.state.codeAdvanced ?
                <Action
                  ref="codeIncorrectValueAction"
                  header={"Aktion, wenn Spieler den Code falsch eingegeben hat"}
                  onChange={this.onCodeActionChange}
                />
              : undefined
            }
          </Col>
          <Col
            md={6}
            style={
              this.state.poiAdvanced ?
                {
                  borderColor:"black",
                  borderStyle:"solid",
                  borderRadius:"10px",
                  borderWidth: "thin"
                }
              : { }
            } >
            <SmartInput
              type='select'
              label='POI, an dem sich der Spieler befinden muss.'
              disabled={!this.state.usePoi}
              selectedIndex={this.state.poiSelectedIndex}
              onChange={this.onPoiChange}
              addonBefore={
                <input
                  // ref="usePoi"
                  type="checkbox"
                  checked={this.state.usePoi}
                  onChange={this.onUsePoiChange}
                />
              }
              buttonAfter={
                <Button
                  active={this.state.poiAdvanced}
                  disabled={!this.state.usePoi}
                  onClick={this.onPoiAdvancedClick}
                >
                  <Glyphicon glyph="cog" /> Erweitert
                </Button>
              }
            >
              <option value={-1} > POI auswählen </option>
              {pois}
            </SmartInput>
            {
              this.state.poiAdvanced ?
                <SmartInput
                  // ref="poiOptional"
                  type="checkbox"
                  label="Optionaler Parameter"
                  checked={
                    this.state.isPoiOptional != undefined ?
                      this.state.isPoiOptional
                    : this.state.isPoiOptionalInit
                  }
                  onChange={this.onIsPoiOptionalChange}
                />
              : undefined
            }
            {
              this.state.poiAdvanced ?
                <ComboboxWithTooltip
                  id="poiUserInfo"
                  label="Information f&uuml;r den Spieler"
                  // ref="poiUserInfo"
                  data={this.state.userInfoOptions}
                  value={this.state.poiUserInfo ? this.state.poiUserInfo : this.state.poiUserInfoInit}
                  onChange={this.onPoiUserInfoChange}
                />
              : undefined
            }
            {
              this.state.poiAdvanced ?
                <Action
                  ref="poiIncorrectValueAction"
                  header={"Aktion, wenn Spieler sich nicht am POI befindet"}
                  onChange={this.onPoiActionChange}
                />
              : undefined
            }
          </Col>
        </Row>
        <Row>
          <Col md={3} >
            <SmartInput
              // ref="useTimeFrame"
              type="checkbox"
              label={<b>Abgabezeitraum</b>}
              checked={this.state.useTimeFrame}
              onChange={this.onUseTimeFrameChange}
            />
          </Col>
        </Row>
        <Row>
          <DatePicker
            header="Job-Abgabezeitraum"
            showCheckbox={true}
            timeFrame={this.state.jobFinishTimeFrame}
            disabled={!this.state.useTimeFrame}
            onChange={this.onTimeFrameChange}
          />
        </Row>
      </Panel>
    );
  },
});

module.exports = Params;
