import React from 'react';

import SmartInput from '../components/smart-input';
import ImagePicker from '../components/image-picker';

import { Panel } from 'react-bootstrap';

var StoryPagePage = React.createClass({

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.updateStoryPagePage;
  },

  handleBubbleTextChange(bubbleText) {
    this.props.onChange("storyPageBubbleText",bubbleText);
  },

  handleBoxTextChange(boxText) {
    this.props.onChange("storyPageBoxText",boxText);
  },

  handleBackgroundChange(image) {
    this.props.onChange("storyPageBackground",image);
  },

  handleNarratorChange(image) {
    this.props.onChange("storyPageNarrator",image);
  },

  render() {
    return (
      <div>
        <Panel
          id={this.props.components[0].id}
          bsStyle='primary'
          header={this.props.header}
        >
          <SmartInput
            type="text"
            value={this.props.storyPageBubbleText ? this.props.storyPageBubbleText.deu : undefined}
            label="Sprechblasentext"
            onChange={this.handleBubbleTextChange}
            checkLocalizedString={false}
          />
          <SmartInput
            type="text"
            value={this.props.storyPageBoxText ? this.props.storyPageBoxText.deu : undefined}
            label="Kastentext"
            onChange={this.handleBoxTextChange}
            checkLocalizedString={false}
          />
          <ImagePicker
            id={this.props.components[1].id}
            header={this.props.components[1].description}
            type="Image"
            activeImage={this.props.storyPageBackground}
            onChange={this.handleBackgroundChange}
          />
          <ImagePicker
            id={this.props.components[2].id}
            header={this.props.components[2].description}
            type="Image"
            activeImage={this.props.storyPageNarrator}
            onChange={this.handleNarratorChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = StoryPagePage;
