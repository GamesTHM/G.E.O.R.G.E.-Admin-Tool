import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import SmartInput from '../components/smart-input';
import Multiselect from '../components/multiselect';

import { Panel } from 'react-bootstrap';

var RolePage = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    var queries = {
      users: (new Parse.Query("User")).limit(1000),
      roles: (new Parse.Query("_Role")),
    };

    if(props.roleUsers) queries["roleUsers"] = props.roleUsers.query();
    if(props.roleRoles) queries["roleRoles"] = props.roleRoles.query();

    return queries;
  },

  getInitialState() {
    return {
      multiselectsInitialized: false,
      allRoles: [],
      allUsers: [],
    };
  },

  updateComponentStates(properties) {
    this.setState({
      multiselectsInitialized: false,
      selectedRoles: [],
      allRoles: [],
      selectedUsers: [],
      allUsers: []
    });
  },

  componentDidMount() {
    this.updateComponentStates(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateComponentStates(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextProps.updateRolePage ||
      nextState.allRoles.length != this.state.allRoles.length ||
      nextState.allUsers.length != this.state.allUsers.length
    );
  },

  getItemFromList(item, list) {
    function containsItem(item) {
      return item.objectId == this.objectId || item.id == this.objectId || this.id == item.objectId;
    }

    var itemsFromList = list.filter(containsItem, item)

    if(itemsFromList.length == 0) return undefined;
    else return itemsFromList[0];
  },

  initializeMultiselects() {
    var selectedUsers, selectedRoles;

    selectedRoles = this.data.roleRoles ? this.data.roleRoles : [];
    selectedUsers = [];

    if(this.data.roleUsers) {
      for(var i=0; i < this.data.roleUsers.length; i++)
        selectedUsers.push({
          objectId: this.data.roleUsers[i].objectId,
          name: this.data.roleUsers[i].username
        });
    }

    this.setState({
      selectedRoles: selectedRoles,
      allRoles: selectedRoles,
      selectedUsers: selectedUsers,
      allUsers: selectedUsers,
      multiselectsInitialized: true
    });
  },

  addRemainingItemsToArray(array, src, nameField) {
    var updateState = false;

    for(var i=0 ; i < src.length; i++) {
      if( this.getItemFromList(src[i],array) == undefined ) {
        updateState = true;
        array.push({
          objectId: src[i].objectId,
          name: src[i][nameField]
        });
      }
    }
    return updateState;
  },

  addRemainingItemsToMultiselects() {
    var allRoles = this.state.allRoles.slice();
    var updateRoles = this.addRemainingItemsToArray(allRoles,this.data.roles,"name");
    var allUsers = this.state.allUsers.slice();
    var updateUsers = this.addRemainingItemsToArray(allUsers,this.data.users,"username");
    var updateState = updateRoles || updateUsers;

    if(updateState)
      this.setState({
        allRoles: allRoles,
        allUsers: allUsers,
      });
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.pendingQueries().length == 0) {
      var fetchedRoleHasUsersOrRoles =
        this.data.roleUsers && this.data.roleUsers.length != 0 ||
        this.data.roleRoles && this.data.roleRoles.length != 0;
      if(
        this.state.multiselectsInitialized == false &&
        fetchedRoleHasUsersOrRoles
      ) {
        this.initializeMultiselects();
      } else {
        this.addRemainingItemsToMultiselects();
      }
    }
  },

  handleNameChange(name) {
    this.props.onChange("roleName",name);
  },

  getItemsNotInList(array1,array2) {
    var items = [];

    for (var i = 0; i < array1.length; i++) {
      if ( this.getItemFromList(array1[i],array2) == undefined )
      items.push(array1[i]);
    }

    return items.length != 0 ? items : undefined;
  },

  getItemsToAdd(newSelection,currentSelection) {
    return this.getItemsNotInList(newSelection,currentSelection);
  },

  getItemsToRemove(newSelection,currentSelection) {
    return this.getItemsNotInList(currentSelection,newSelection);
  },

  onUsersChange(selection) {
    var usersToAdd = this.getItemsToAdd(selection,this.state.selectedUsers);
    var usersToRemove = this.getItemsToRemove(selection,this.state.selectedUsers);

    this.props.onChange("roleUsers",{
      usersToAdd: usersToAdd,
      usersToRemove: usersToRemove
    });
  },

  onRolesChange(selection) {
    var rolesToAdd = this.getItemsToAdd(selection,this.state.selectedRoles);
    var rolesToRemove = this.getItemsToRemove(selection,this.state.selectedRoles);

    this.props.onChange("roleRoles",{
      rolesToAdd: rolesToAdd,
      rolesToRemove: rolesToRemove
    });
  },

  render() {
    return (
      <div className="RolePage">
        <Panel
          id={this.props.components[0].id}
          bsStyle='primary'
          header={this.props.header}
        >
          <SmartInput
            type="text"
            value={this.props.roleName}
            label="Rollen-Name"
            onChange={this.handleNameChange}
            checkLocalizedString={false}
          />
          <p><b>Benutzer w&auml;hlen</b></p>
          <Multiselect
            data={this.state.allUsers}
            value={this.state.selectedUsers}
            onChange={this.onUsersChange}
          />
          <p><b>Rollen w&auml;hlen</b></p>
          <p>Die zugewiesenen Rollen bekommen die gleichen Rechte, wie diese Rolle</p>
          <Multiselect
            data={this.state.allRoles}
            value={this.state.selectedRoles}
            onChange={this.onRolesChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = RolePage;
