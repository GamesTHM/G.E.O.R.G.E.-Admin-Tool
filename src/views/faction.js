import React from 'react';

import ImagePicker from '../components/image-picker';
import FactionColorsPicker from '../components/faction-colors-picker';
import WYSIWYGEditor from '../components/wysiwyg-editor';
import SmartInput from '../components/smart-input';

import { Panel } from 'react-bootstrap';

var FactionPage = React.createClass({

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.updateFactionPage;
  },

  onVisibilityChange(checked) {
    this.props.onChange("visibility",checked);
  },

  onNameChange(name) {
    this.props.onChange("factionName",name);
  },

  onColorsChange(colors) {
    this.props.onChange("factionColors",colors);
  },

  handleEditorChange(factionLore) {
    this.props.onChange("factionLore",factionLore);
  },

  handleImageChange(image) {
    this.props.onChange("factionImage",image);
  },

  handleIconChange(icon) {
    this.props.onChange("factionIcon",icon);
  },

  render() {
    return (
      <div className="FactionPage">
        <Panel
          id={this.props.components[0].id}
          bsStyle='primary'
          header={this.props.header}
        >
          <SmartInput
            type="checkbox"
            checked={this.props.visibility}
            label="Freischalten"
            onChange={this.onVisibilityChange}
            checkLocalizedString={false}
          />
          <SmartInput
            type="text"
            value={this.props.factionName ? this.props.factionName.deu : undefined}
            label="Fraktions-Name"
            onChange={this.onNameChange}
            checkLocalizedString={false}
          />
          <FactionColorsPicker
            id={this.props.components[1].id}
            header={this.props.components[1].description}
            mainColor={
              this.props.factionColors
              ? this.props.factionColors.mainColor
              : "#fff"
            }
            textColor={
              this.props.factionColors
              ? this.props.factionColors.textColor
              : "#000"
            }
            onChange={this.onColorsChange}
          />
          <WYSIWYGEditor
            id={this.props.components[2].id}
            header={this.props.components[2].description}
            webElement={this.props.factionLore}
            template={this.props.factionLore ? undefined : "faction"}
            onChange={this.handleEditorChange}
          />
          <ImagePicker
            id={this.props.components[3].id}
            header={this.props.components[3].description}
            type="Image"
            activeImage={this.props.factionImage}
            onChange={this.handleImageChange}
          />
          <ImagePicker
            id={this.props.components[4].id}
            header={this.props.components[4].description}
            type="Icon"
            activeImage={this.props.factionIcon}
            onChange={this.handleIconChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = FactionPage;
