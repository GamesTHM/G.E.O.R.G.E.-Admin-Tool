import React from 'react';
import { Parse } from 'parse';

import GeopointPicker from '../components/geopoint-picker';
import SmartInput from '../components/smart-input';

import { Panel, Input } from 'react-bootstrap';

var CampusPage = React.createClass({

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextProps.updateCampusPage == true
    );
  },

  onVisibilityChange(checked) {
    this.props.onChange("aktiv",checked);
  },

  onNameChange(name) {
    this.props.onChange("campusName",name);
  },

  onGPChange(gp) {
    this.props.onChange("campusArea",gp);
  },

  onLevelsChange() {
    this.setState(this.getInitialState());
  },

  render() {
    return (
      <div className="CampusPage">
        <Panel
          id={this.props.components[0].id}
          bsStyle='primary'
          header={this.props.header}
        >
          <SmartInput
            type="checkbox"
            checked={this.props.visibility}
            label="Freischalten"
            onChange={this.onVisibilityChange}
            checkLocalizedString={false}
          />
          <SmartInput
            type="text"
            value={
              this.props.campusName
              ? this.props.campusName.deu
              : undefined
            }
            label="Campus-Name"
            onChange={this.onNameChange}
            checkLocalizedString={false}
          />
          <GeopointPicker
            id={this.props.components[1].id}
            header={this.props.components[1].description}
            geoPoints={this.props.geoPoints}
            show2ndInput={true}
            onChange={this.onGPChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = CampusPage;
