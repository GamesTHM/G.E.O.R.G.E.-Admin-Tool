import React from 'react';
import { Parse } from 'parse';

import GeopointPicker from '../components/geopoint-picker';
import SmartInput from '../components/smart-input';

import { Panel, Input, Button } from 'react-bootstrap';

//require('../pluginsystem/pluginInstaller.js');

var content = "";
var PluginSettings = React.createClass({

    shouldComponentUpdate(nextProps,nextState) {
        return (
            nextProps.updatePluginPage == true
        );
    },

    onVisibilityChange(checked) {
        this.props.onChange("visibility",checked);
    },

    onNameChange(name) {
        this.props.onChange("pluginName",name);
    },
    onDelete(test){
        this.props.onChange("delete",true);
    },
    onFileChoose(files){
        for (var i = 0, file; file = files[i]; i++) {
            if(file.type != "application/zip")
            {
                console.log("File Type Error");
            }
            else
            {
                name = file.name;
                name = name.substr(0, name.lastIndexOf('.'));

                this.props.onChange("pluginName",name);
                this.props.onChange("visibility",false);
                this.props.onChange("pluginFile", file);
            }
        }
    },

    render() {
        if (this.props.pluginName != undefined && this.props.pluginName.deu != "") {
            try {
                var name = this.props.pluginName + "/main";
                if(this.props.pluginVisibility)
                {
                    var PluginSetting = require('../pluginsystem/plugin/' + name + '.js');

                    PluginSetting = new PluginSetting();
                    content = PluginSetting.renderSettings();
                }
                else
                {
                    content = "Plugin Deaktiviert - Keine Anpassungen möglich!";
                }
            }
            catch(err)
            {
                content += " -- "+ err.message;
            }
            return (
                //Es handelt sich um ein vorhandens Plugin
                //Es kann upgedatetet, aktiviert oder deaktiviert werden.
                <div className="PluginSettings">
                    <Panel
                        id={this.props.components[1].id}
                        bsStyle='primary'
                        header={this.props.header}
                    >
                        {content}
                    </Panel>
                </div>
            );
        }
        else
            return(
                <div className="PluginSettings">
                </div>
            );
    }
});

module.exports = PluginSettings;
