import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import GeopointPicker from '../components/geopoint-picker';
import WYSIWYGEditor from '../components/wysiwyg-editor';
import ImagePicker from '../components/image-picker';
import ComboboxWithTooltip from '../components/combobox-with-tooltip';
import SmartInput from '../components/smart-input';
import PoiLocationPicker from '../components/poi-location-picker';

import { Panel, Input, Row, Col } from 'react-bootstrap';

var PoiPage = React.createClass({

  mixins: [ParseReact.Mixin],

  values: {},

  observe(props,state) {
    return {
      pois: (new Parse.Query("Location")).include(["name","address"]),
    };
  },

  getInitialState() {
    return {
      addressOptions: []
    };
  },

  updateComponentStates(properties) {
    this.setState({
      poiCampusSelectedIndex: 0,
      poiCampusSelectedIndexInitialized: false,
      poiCampusFloorLevelSelectedIndex: 0,
      poiCampusFloorLevelSelectedIndexInitialized: false,
    });
  },

  componentDidMount() {
    this.updateComponentStates(this.props);
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.updatePoiPage) this.updateComponentStates(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.updatePoiPage ||
      this.state.addressOptions.length != nextState.addressOptions.length;
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.data.pois.length != 0) {
      var addressOptionsIds = [], addressOptions = [];

      for(var i = 0; i < this.data.pois.length; i++) {
        if( this.data.pois[i].address && addressOptionsIds.indexOf(this.data.pois[i].address.objectId) == -1) {
          addressOptionsIds.push(this.data.pois[i].address.objectId);
          addressOptions.push(this.data.pois[i].address);
        }
      }

      if(this.state.addressOptions.length != addressOptions.length)
        this.setState({addressOptions: addressOptions});
    }
  },

  onVisibilityChange(checked) {
    this.props.onChange("visibility", checked);
  },

  onAttackableChange(checked) {
    this.props.onChange("poiAttackable", checked);
  },

  onNameChange(name) {
    this.props.onChange("poiName", name);
  },

  onLocationDataChange(locationData) {
    this.props.onChange("poiLocationData", locationData);
  },

  onAddressChange(address) {
    this.props.onChange("poiAddress", address);
  },

  onBeaconDataChange() {
    this.props.onChange("poiBeaconData", {
      beaconuuid: this.values.beaconuuid,
      beaconmayor: this.values.beaconmayor,
      beaconminor: this.values.beaconminor
    });
  },

  onBeaconUuidChange(beaconuuid) {
    this.values.beaconuuid = beaconuuid;
    this.onBeaconDataChange();
  },

  onBeaconMayorChange(beaconmayor) {
    this.values.beaconmayor = beaconmayor;
    this.onBeaconDataChange();
  },

  onBeaconMinorChange(beaconminor) {
    this.values.beaconminor = beaconminor;
    this.onBeaconDataChange();
  },

  onGPChange(gp) {
    this.props.onChange("poiPosition",gp);
  },

  onLoreChange(lore) {
    this.props.onChange("poiLore", lore);
  },

  onInformationChange(information) {
    this.props.onChange("poiInformation", information);
  },

  onImageChange(image) {
    this.props.onChange("poiImage",image);
  },

  onIconChange(icon) {
    this.props.onChange("poiIcon",icon);
  },

  render() {
    return (
      <div className="PoiPage">
        <Panel
          id={this.props.components[0].id}
          header={this.props.header}
          bsStyle='primary'
        >
          <Row>
            <Col md={2} >
              <SmartInput
                type="checkbox"
                checked={this.props.visibility}
                label="Freischalten"
                onChange={this.onVisibilityChange}
              />
            </Col>
            <Col md={2} >
              <SmartInput
                type="checkbox"
                checked={this.props.poiAttackable}
                label="Angreifbar"
                onChange={this.onAttackableChange}
              />
            </Col>
          </Row>
          <SmartInput
            type="text"
            label="POI-Name"
            value={this.props.poiName}
            onChange={this.onNameChange}
            checkLocalizedString={false}
          />
          <PoiLocationPicker
            campusId={this.props.campusId}
            poiId={this.props.poiId}
            onChange={this.onLocationDataChange}
          />
          <ComboboxWithTooltip
            id="poiAddress"
            label="POI-Adresse"
            data={this.state.addressOptions}
            value={this.props.poiAddress}
            onChange={this.onAddressChange}
          />
          <Row>
            <Col md={6} >
              <SmartInput
                type="text"
                label="Beacon UUID"
                value={
                  this.props.poiBeaconData && this.props.poiBeaconData.beaconuuid
                  ? this.props.poiBeaconData.beaconuuid
                  : ""
                }
                onChange={this.onBeaconUuidChange}
                checkLocalizedString={false}
              />
            </Col>
            <Col md={3} >
              <SmartInput
                type="text"
                label="Beacon Major"
                value={
                  this.props.poiBeaconData && this.props.poiBeaconData.beaconmayor
                  ? this.props.poiBeaconData.beaconmayor
                  : ""
                }
                onChange={this.onBeaconMayorChange}
                checkLocalizedString={false}
              />
            </Col>
            <Col md={3} >
              <SmartInput
                type="text"
                label="Beacon Minor"
                value={
                  this.props.poiBeaconData && this.props.poiBeaconData.beaconminor
                  ? this.props.poiBeaconData.beaconminor
                  : ""
                }
                onChange={this.onBeaconMinorChange}
                checkLocalizedString={false}
              />
            </Col>
          </Row>
          <GeopointPicker
            id={this.props.components[1].id}
            header={this.props.components[1].description}
            geoPoints={this.props.poiPosition}
            onChange={this.onGPChange}
          />
          <WYSIWYGEditor
            id={this.props.components[2].id}
            header={this.props.components[2].description}
            webElement={this.props.poiLore}
            template={this.props.poiLore ? undefined : "poi"}
            onChange={this.onLoreChange}
          />
          <WYSIWYGEditor
            id={this.props.components[3].id}
            header={this.props.components[3].description}
            webElement={this.props.poiInformation}
            template={this.props.poiInformation ? undefined : "poi"}
            onChange={this.onInformationChange}
          />
          <ImagePicker
            id={this.props.components[4].id}
            header={this.props.components[4].description}
            type="Image"
            activeImage={this.props.poiImage}
            onChange={this.onImageChange}
          />
          <ImagePicker
            id={this.props.components[5].id}
            header={this.props.components[5].description}
            type="Image"
            activeImage={this.props.poiIcon}
            onChange={this.onIconChange}
          />
        </Panel>
      </div>
    );
  }
});

module.exports = PoiPage;
