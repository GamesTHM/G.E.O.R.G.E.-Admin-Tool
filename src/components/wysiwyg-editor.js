import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import { Panel, Input } from 'react-bootstrap';

// var TinyMCE = require('react-tinymce');
import TinyMCEInput from 'react-tinymce-input';

var WYSIWYGEditor = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    if(!state.dontRefresh)
      return {
        webTemplates: (new Parse.Query("WebElement").exists("description").include("contentStyle")),
        images: (new Parse.Query("Image")),
        icons: (new Parse.Query("Icon"))
      };
  },

  getInitialState() {
    return {
      content: " ",
      // contentStyle: undefined,
      contentCssUrl: undefined,
      contentWillChange: false,
      dontRefresh: false,
      webTemplateLoaded: false,
      remountTinyMCE: false
    };
  },

  componentDidMount() {
    if(this.props.template == undefined) {
      var contentInit = this.props.webElement ? this.props.webElement.content : undefined;
      // var contentStyleInit = this.props.webElement ? this.props.webElement.contentStyle : undefined;
      var contentCssUrlInit = this.props.webElement && this.props.webElement.contentStyle &&
        this.props.webElement.contentStyle.file ? this.props.webElement.contentStyle.file.url() : undefined;

      this.setState({
        contentInit: contentInit ? contentInit : " ",
        content: contentInit ? contentInit : " ",
        // contentStyleInit: contentStyleInit,
        // contentStyle: contentStyleInit,
        contentCssUrlInit: contentCssUrlInit,
        contentCssUrl: contentCssUrlInit //this.props.webElement.contentStyle
      });
    }

    if(this.props.onMounted) this.props.onMounted();
  },

  componentWillReceiveProps(nextProps) {
    if(nextProps.template == undefined) {
      var nextContentCssUrl = nextProps.webElement && nextProps.webElement.contentStyle &&
        nextProps.webElement.contentStyle.file ? nextProps.webElement.contentStyle.file.url() : undefined;
      var thisContentCssUrl = this.props.webElement && this.props.webElement.contentStyle &&
        this.props.webElement.contentStyle.file ? this.props.webElement.contentStyle.file.url() : undefined;

      var nextWebElementContent = nextProps.webElement ? nextProps.webElement.content : " ";
      var thisWebElementContent = this.props.webElement ? this.props.webElement.content : " ";

      if( nextWebElementContent != thisWebElementContent ||
          nextContentCssUrl != thisContentCssUrl ||
          nextProps.template != this.props.template ) {
        this.setState({
          content: nextWebElementContent,
          // contentStyle: nextProps.webElement ? nextProps.webElement.contentStyle : undefined,
          contentCssUrl: nextContentCssUrl, //nextProps.webElement.contentStyle
          contentWillChange: true
        });
      } else {
        this.setState({contentWillChange: false});
      }

      if(this.refs.template && this.refs.template.getInputDOMNode()) this.refs.template.getInputDOMNode().selectedIndex = 0;
    } else if(nextProps.template != this.props.template) {
      if(this.pendingQueries().length == 0) this.loadTemplateOnInit(nextProps.template);
      else if(this.state.webTemplateLoaded != false) this.setState({webTemplateLoaded: false});
    }
  },

  shouldComponentUpdate(nextProps,nextState) {
    var nextWebElementContent = nextProps.webElement ? nextProps.webElement.content : undefined;
    var thisWebElementContent = this.props.webElement ? this.props.webElement.content : undefined;

    return  nextWebElementContent != thisWebElementContent ||
            // nextProps.webElement.contentStyle != this.props.webElement.contentStyle ||
            nextState.content != this.state.content ||
            // nextState.contentStyle != this.state.contentStyle;
            nextState.contentWillChange == true ||
            nextState.remountTinyMCE != this.state.remountTinyMCE;
  },

  componentDidUpdate() {
    if(this.pendingQueries().length == 0 && this.state.dontRefresh == false) this.setState({ dontRefresh: true });

    if(this.pendingQueries().length == 0 && this.state.webTemplateLoaded == false) this.loadTemplateOnInit(this.props.template);

    if(this.state.remountTinyMCE == true) this.setState({ remountTinyMCE : false });
  },

  setWebElement(webElement) {
    if(webElement) {
      this.setState({
        content: webElement.content ? webElement.content : " ",
        // contentStyle: webElement.contentStyle,
        contentCssUrl: webElement.contentStyle && webElement.contentStyle.file ? webElement.contentStyle.file.url() : undefined
      });
    }
  },

  loadTemplateOnInit(template){
    switch(template){
      case "quest":
        this.loadTemplate(2);
        this.refs.template.getInputDOMNode().selectedIndex = 2+1;
        break;
      case "poi":
        this.loadTemplate(1);
        this.refs.template.getInputDOMNode().selectedIndex = 1+1;
        break;
      case "faction":
        this.loadTemplate(0);
        this.refs.template.getInputDOMNode().selectedIndex = 0+1;
        break;
    }
    this.setState({webTemplateLoaded: true});
  },

  loadTemplate(index) {
    var nextContentCssUrl = this.data.webTemplates[index].contentStyle &&
      this.data.webTemplates[index].contentStyle.file ?
        this.data.webTemplates[index].contentStyle.file.url() :
        undefined;

    this.setState({
      content: this.data.webTemplates[index].content,
      // contentStyle: this.data.webTemplates[index].contentStyle,
      contentCssUrl: nextContentCssUrl,
      remountTinyMCE: true
    });
  },

  handleTemplateChange(e) {
    if(e.target.value == -1) {
      this.setState({
        content: this.state.contentInit,
        // contentStyle: this.state.contentStyleInit,
        contentCssUrl: this.state.contentCssUrlInit,
        remountTinyMCE: true
      });
    }
    else {
      this.loadTemplate(e.target.value);
    }

    this.props.onChange(undefined);
  },

  handleEditorChange(content) {
    if(this.state.contentWillChange == true) this.setState({contentWillChange: false});
    else
      this.props.onChange({
        content: content, //e.target.getContent(),
        // contentStyle: this.state.contentStyle //e.target.contentStyles.length == 2 ? e.target.contentStyles[0] : undefined
      });
  },

  render() {
    var templates = this.data.webTemplates.map(function(template,i) {
      return (
        <option key={template.objectId} value={i} >
          {template.description}
        </option>
      );
    });

    var images = this.data.images.map(function(image) {
      return ({
        title: image.image.name().substr(image.image.name().lastIndexOf("-")+1),
        value: image.image.url()
      });
    });

    var icons = this.data.icons.map(function(icon) {
      return ({
        title: icon.icon.name().substr(icon.icon.name().lastIndexOf("-")+1),
        value: icon.icon.url()
      });
    });

    var imageList = images.concat(icons);

    var pqs = this.pendingQueries();

    return (
      <Panel
        id={this.props.id}
        header={this.props.header}
        bsStyle='primary'
      >
        <Input ref="template" type='select' label='Template auswählen' onChange={this.handleTemplateChange} >
          <option value={-1} >Keins (lädt ursprünglichen Webseiteninhalt)</option>
          {templates}
        </Input>
        {
          pqs != undefined &&
          pqs.length == 0 &&
          (!this.props.template || this.state.webTemplateLoaded) &&
          this.state.remountTinyMCE == false ?
            <TinyMCEInput
              value={this.state.content}
              tinymceConfig={ {
                content_css: this.state.contentCssUrl,
                content_style: "body.mce-content-body{background-color: rgba(0,0,100,0.7)}",
                height: '500px',
                language: "de",
                plugins: [
                    "advlist autolink lists link image charmap hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "template paste textcolor colorpicker textpattern imagetools"
                ],
                toolbar:  "undo redo | styleselect | bold italic | forecolor backcolor | " +
                          "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media code",
                contextmenu: 'link image inserttable | cell row column | deletetable | code | cut copy paste',
                image_list: imageList,
                image_advtab: true,
              } }
              onChange={this.handleEditorChange}
              // onUndo={this.handleEditorChange}
              // onRedo={this.handleEditorChange}
            />
          : undefined
        }
      </Panel>
    );
  },

});

module.exports = WYSIWYGEditor;
