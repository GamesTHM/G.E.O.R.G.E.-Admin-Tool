import React from 'react';

import {
  Nav, Navbar, NavBrand, NavItem, NavDropdown,
  MenuItem, Glyphicon, OverlayTrigger, Tooltip
} from 'react-bootstrap';

import { IndexLink, Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';

var NavTopBar = React.createClass({
  render() {
    return (
      <Navbar inverse fixedTop>
        <NavBrand><IndexLink to="/" >G.E.O.R.G.E.</IndexLink></NavBrand>
        <Nav>
          <NavDropdown id='users-and-roles' title='Benutzer und Rollen'>
            <LinkContainer to={ { pathname: "/users" } }>
              <NavItem>Benutzer</NavItem>
            </LinkContainer>
            <LinkContainer to={ { pathname: "/roles" } }>
              <NavItem>Rollen</NavItem>
            </LinkContainer>
          </NavDropdown>
          <LinkContainer to={ { pathname: "/factions" } }>
            <NavItem>Fraktionen</NavItem>
          </LinkContainer>
          <NavDropdown id='locations' title='Standorte'>
            <LinkContainer to={ { pathname: "/campusses" } }>
              <MenuItem>Campusse</MenuItem>
            </LinkContainer>
            <LinkContainer to={ { pathname: "/pois" } }>
              <MenuItem>Point of Interests</MenuItem>
            </LinkContainer>
          </NavDropdown>
          <NavDropdown id='quests' title='Aufgaben'>
            <LinkContainer to={ { pathname: "/quests" } }>
              <MenuItem>Quests</MenuItem>
            </LinkContainer>
            <LinkContainer to={ { pathname: "/jobs" } }>
              <MenuItem>Jobs</MenuItem>
            </LinkContainer>
          </NavDropdown>
          <NavDropdown id='plugins' title='Plugins'>
            <LinkContainer to={ { pathname: "/plugins" } }>
              <MenuItem>Verwalten</MenuItem>
            </LinkContainer>
          </NavDropdown>
          {/*
          <NavDropdown id='localization' title='Lokalisierung'>
            <LinkContainer to={ { pathname: "/localization/texts" } }>
              <MenuItem>Texte</MenuItem>
            </LinkContainer>
            <LinkContainer to={ { pathname: "/localization/webelements" } }>
              <MenuItem>Web-Elemente</MenuItem>
            </LinkContainer>
          </NavDropdown>
          <LinkContainer to={ { pathname: "/stats" } }>
            <NavItem>Statistiken</NavItem>
          </LinkContainer>
          */}
        </Nav>
        <Nav right>
          {/*
          <OverlayTrigger placement="bottom" overlay={<Tooltip>Login</Tooltip>} delayShow={300} delayHide={150}>
            <NavItem eventKey={10} href='#login' ><Glyphicon glyph="log-in" /></NavItem>
          </OverlayTrigger>
          */}
          {
            this.props.user
            ? <NavItem>Eingeloggt als:&nbsp;<b>{this.props.user.getUsername()}</b></NavItem>
            : undefined
          }
          {
            this.props.user
            ? <NavItem onClick={this.props.onLogoutClick} ><Glyphicon glyph="log-out" />&nbsp; Logout</NavItem>
            : <LinkContainer to={ { pathname: "/login" } }>
                <NavItem><Glyphicon glyph="log-in" />&nbsp; Login</NavItem>
              </LinkContainer>
          }
        </Nav>
      </Navbar>
    );
  }
});

module.exports = NavTopBar;
