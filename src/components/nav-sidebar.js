import React from 'react';

import { Col, Nav } from 'react-bootstrap';

var NavSideBar = React.createClass({
  render() {
    return (
      <Col md={2} id="sidebar" >
        <Nav bsStyle="link" stacked>
          {this.props.children}
        </Nav>
      </Col>
    );
  }
});

module.exports = NavSideBar;
