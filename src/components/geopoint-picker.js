import React from 'react';

import { Panel, Row, Col, Label, Input } from 'react-bootstrap';

var ReactLeaflet = require('react-leaflet/lib/index');

var Map = ReactLeaflet.Map;
var Marker = ReactLeaflet.Marker;
var Popup = ReactLeaflet.Popup;
var TileLayer = ReactLeaflet.TileLayer;
var Rectangle = ReactLeaflet.Rectangle;

ReactLeaflet.setIconDefaultImagePath('/img/leaflet');

const position = [50.474748, 8.618867];

var GeopointPicker = React.createClass({

  getInitialState() {
    return {
      mapClicked: false,
      activeInput: "geopoint1",
      geopoint1: undefined,
      geopoint2: undefined,
      geopoint1latlng: [0,1],
      geopoint2latlng: [1,0]
    };
  },

  updateState(properties) {
    if(properties.geoPoints && properties.geoPoints[0] && properties.geoPoints[0].lat) {
      this.setState({
        geopoint1: properties.geoPoints[0].lat + ", " + properties.geoPoints[0].lng,
        geopoint1latlng: properties.geoPoints[0]
      });
    } else {
      this.setState({
        geopoint1: undefined,
        geopoint1latlng: [0,1]
      });
    }
    if(properties.geoPoints && properties.geoPoints[1] && properties.geoPoints[1].lat) {
      this.setState({
        geopoint2: properties.geoPoints[1].lat + ", " + properties.geoPoints[1].lng,
        geopoint2latlng: properties.geoPoints[1]
      });
    } else {
      this.setState({
        geopoint2: undefined,
        geopoint2latlng: [1,0]
      });
    }
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.state.geopoint1) {
      this.setDOMValues("1",this.state.geopoint1latlng);
    }
    if(this.state.geopoint2) {
      this.setDOMValues("2",this.state.geopoint2latlng);
    }

    if(this.state.mapClicked == true) {
      this.setState({mapClicked: false});
      if(this.props.show2ndInput && this.state.geopoint1 && this.state.geopoint2) {
        this.props.onChange([
          this.state.geopoint1latlng,
          this.state.geopoint2latlng
        ]);
      } else if(!this.props.show2ndInput && this.state.geopoint1){
        this.props.onChange(this.state.geopoint1latlng);
      }
    }
  },

  onFocus(inputNr) {
    this.setState({
      activeInput: "geopoint" + inputNr
    });
  },

  setDOMValues(nr, latlng) {
    this.refs["marker"+nr].getLeafletElement().setLatLng(latlng);
    this.refs["input"+nr].value = latlng.lat + ", " + latlng.lng;
    this.refs["marker"+nr].getLeafletElement().options.draggable = true; //doesn't work :-(
  },

  handleMapClick(e) {
    this.setState({
      mapClicked: true,
      [this.state.activeInput]: e.latlng.lat + ", " + e.latlng.lng,
      [this.state.activeInput+"latlng"]: e.latlng
    });
  },

  handleChange(nr,e) {
    var latlng = e.target.value.split(",");
    this.setState({
      mapClicked: true,
      ["geopoint" + nr]: e.target.value,
      ["geopoint" + nr +"latlng"]: new L.LatLng(latlng[0],latlng[1])
    });
  },

  render() {
    var inputs = this.props.show2ndInput ?
      <div>
        <Input
          value={this.state.geopoint1}
          ref="input1"
          type='text'
          label={"Nordöstlichsten Punkt auswählen"}
          onFocus={ this.onFocus.bind(this,1) }
          onChange={ this.handleChange.bind(this,1) }
        />
        <Input
          value={this.state.geopoint2}
          ref="input2"
          type='text'
          label={"Südwestlichsten Punkt auswählen"}
          onFocus={ this.onFocus.bind(this,2) }
          onChange={ this.handleChange.bind(this,2) }
        />
      </div> :
      <Input
        value={this.state.geopoint1}
        ref="input1"
        type='text'
        label={"POI-Position auswählen"}
        onFocus={ this.onFocus.bind(this,1) }
        onChange={ this.handleChange.bind(this,1) }
      />

    var rectangle = this.state.geopoint1latlng.lat && this.state.geopoint2latlng.lat ?
      <Rectangle bounds={[this.state.geopoint1latlng,this.state.geopoint2latlng]} /> : undefined;

    return (
      <Panel
        id={this.props.id}
        header={this.props.header}
        bsStyle='primary'
      >
        <Row>
          <Col md={6}>
            { this.props.show2ndInput ?
                <div>
                  <p>Hier wird der Campusbereich festgelegt. Wählen Sie dazu den nordöstlichsten und den südwestlichsten Punkt auf der Karte.</p>
                  <ol>
                    <li>Klicken Sie in das Feld für das Sie die Koordinate auswählen wollen</li>
                    <li>Klicken Sie auf einen Punkt auf der Karte</li>
                  </ol>
                  <p>Die Koordinaten werden dann automatisch in das Feld eingetragen.</p>
                  <p>Hinweis: Wenn noch nicht in ein Feld geklickt wurde, werden die Koordinaten für den nordöstlichsten Punkt eingetragen.</p>
                </div>
              : <p>Klicken Sie auf einen Punkt auf der Karte. Die Koordinaten werden dann in das Feld eingetragen.</p>
            }
            {inputs}
          </Col>
          <Col md={6}>
            <Map center={position} zoom={10} style={ {height: "400px"} } onClick={this.handleMapClick} >
              <TileLayer
                url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              />
              <Marker ref="marker1" position={this.state.geopoint1latlng} />
              <Marker ref="marker2" position={this.state.geopoint2latlng} />
              {rectangle}
            </Map>
          </Col>
        </Row>
      </Panel>
    );
  },

});

module.exports = GeopointPicker;
