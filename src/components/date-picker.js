import React from 'react';

import {
  Panel, Col, Label, Input, Alert, Glyphicon
} from 'react-bootstrap';

import { DateTimePicker } from 'react-widgets';

var Globalize = require('globalize');
var globalizeLocalizer = require('react-widgets/lib/localizers/globalize');

require('../../node_modules/globalize/lib/cultures/globalize.culture.de-DE');

globalizeLocalizer(Globalize);

var DatePicker = React.createClass({

  getInitialState() {
    return {
      startDate: undefined,
      endDate: undefined,
      compareTimeOnly: undefined
    };
  },

  updateState(properties) {
    if(properties.showCheckbox && properties.timeFrame)
      this.refs.compareTimeOnly.getInputDOMNode().checked = properties.timeFrame.compareTimeOnly;

    if(properties.timeFrame) {
      this.setState({
        startDate: properties.timeFrame.startDate,
        endDate: properties.timeFrame.endDate,
        compareTimeOnly: properties.timeFrame.compareTimeOnly
      });
    } else {
      this.setState(this.getInitialState());
    }
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  handleStartDateChange(startDate) {
    this.setState({ startDate: startDate ? startDate : undefined});
    this.props.onChange({
      startDate: startDate ? startDate : undefined,
      endDate: this.state.endDate,
      compareTimeOnly: this.state.compareTimeOnly
    });
  },

  handleEndDateChange(endDate) {
    this.setState({ endDate: endDate ? endDate : undefined });
    this.props.onChange({
      startDate: this.state.startDate,
      endDate: endDate ? endDate : undefined,
      compareTimeOnly: this.state.compareTimeOnly
    });
  },

  handleCompareTimeOnlyChange(e) {
    this.setState({compareTimeOnly: e.target.checked});
    this.props.onChange({
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      compareTimeOnly: e.target.checked
    });
  },

  render() {
    var compareTimeOnlyCheckBox =
      this.props.showCheckbox ?
        <Col md={4} style={ { paddingTop: "35px" } }>
          <Input
            ref="compareTimeOnly"
            type="checkbox"
            label="Nur Uhrzeit vergleichen"
            value={this.state.compareTimeOnly}
            disabled={this.props.disabled}
            onChange={this.handleCompareTimeOnlyChange}
          />
        </Col>
      : undefined;
    var content =
      <div>
        <Col md={compareTimeOnlyCheckBox ? 4 : 6} >
          <h4><Label>Start-Datum</Label></h4>
          <DateTimePicker
            defaultValue={undefined}
            value={this.state.startDate}
            calendar={!this.state.compareTimeOnly}
            // time={this.state.useTimeStart}
            culture={"de-DE"}
            disabled={this.props.disabled}
            onChange={this.handleStartDateChange}
          />
        </Col>
        <Col md={compareTimeOnlyCheckBox ? 4 : 6} >
          <h4><Label>End-Datum</Label></h4>
          <DateTimePicker
            defaultValue={undefined}
            value={this.state.endDate}
            calendar={!this.state.compareTimeOnly}
            // time={this.state.useTimeEnd}
            culture={"de-DE"}
            disabled={this.props.disabled}
            onChange={this.handleEndDateChange}
          />
        </Col>
      </div>;

    return (
      <div>
        {
          this.props.wrapComponentIntoPanel ?
            <Panel id={this.props.id} header={this.props.header} bsStyle='primary' >
              {
                this.state.startDate == undefined || this.state.endDate == undefined ?
                  <Alert bsStyle='warning' style={ { marginBottom: "0" } } >
                    <b><Glyphicon glyph="alert" />&nbsp;Achtung!</b>
                    &nbsp; Wenn kein Zeitraum festgelegt wird, wird die Quest nicht im Spiel angezeigt.
                  </Alert>
                : undefined
              }
              {content}
              {compareTimeOnlyCheckBox}
            </Panel>
          : <div>
              {content}
              {compareTimeOnlyCheckBox}
            </div>
        }
      </div>
    );
  },

});

module.exports = DatePicker;
