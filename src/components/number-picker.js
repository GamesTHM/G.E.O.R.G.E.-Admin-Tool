import React from 'react';

import { NumberPicker } from 'react-widgets';

var MyNumberPicker = React.createClass({

  getInitialState() {
    return {
      min: 0,
      step: 1,
      value: 0
    };
  },

  updateState(properties) {
    this.setState({
      min: properties.min != undefined ? properties.min : 0,
      step: properties.step != undefined ? properties.step : 1,
      value: properties.value != undefined ? properties.value : 0
    })
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextProps.min != this.props.min ||
      nextProps.step != this.props.step ||
      nextProps.value != this.props.value ||
      nextState.min != this.state.min ||
      nextState.step != this.state.step ||
      nextState.value != this.state.value
    );
  },

  passChangeToParent() {
    this.props.onChange(this.state.value);
  },

  handleChange(value) {
    this.setState({ value: value },this.passChangeToParent);
  },

  render() {
    return (
      <NumberPicker
        min={this.state.min}
        step={this.state.step}
        value={this.state.value}
        onChange={this.handleChange}
      />
    );
  },
});

module.exports = MyNumberPicker;
