var React = require('react');
var Parse = require('parse').Parse;
var ParseReact = require('parse-react');

var Row = require('react-bootstrap').Row;
var Col = require('react-bootstrap').Col;

var DropdownButton = require('react-bootstrap').DropdownButton;
var MenuItem = require('react-bootstrap').MenuItem;

var Input = require('react-bootstrap').Input;

var Panel = require('react-bootstrap').Panel;
var Label = require('react-bootstrap').Label;

var LocalizedString = React.createClass({
	
	mixins: [ParseReact.Mixin],
	
	observe: function() {
		return {
			localizedStrings: (new Parse.Query(this.props.parseClassName)).include(this.props.attributeName)
		};
	},
	
	getInitialState: function() {
		return { 
			index: 0,
			def: "",
			de : "",
			en : ""
		};
	},
	
	componentWillReceiveProps: function(nextProps) {
		this.refs.select.getInputDOMNode().selectedIndex = nextProps.values.index;
		this.setState(nextProps.values);
	},
	
	loadLocalizedStrings: function(event) {
	//loadLocalizedStrings: function(eventKey, href, target) {
		if(event.target.value == 0){
			this.setState(this.getInitialState());
			this.props.onChange("all",
			{
				index: event.target.selectedIndex,
				"name-def": "",
				"name-de": "",
				"name-en": ""
			});
		}
		else {
			this.setState(
			{
				index: event.target.selectedIndex,
				def: this.data.localizedStrings[Number(event.target.value)-1][this.props.attributeName]["default"],
				de: this.data.localizedStrings[Number(event.target.value)-1][this.props.attributeName]["deu"],
				en: this.data.localizedStrings[Number(event.target.value)-1][this.props.attributeName]["en"]
			});			
			this.props.onChange("all",
			{
				index: event.target.selectedIndex,
				"name-def": this.data.localizedStrings[Number(event.target.value)-1][this.props.attributeName]["default"],
				"name-de": this.data.localizedStrings[Number(event.target.value)-1][this.props.attributeName]["deu"],
				"name-en": this.data.localizedStrings[Number(event.target.value)-1][this.props.attributeName]["en"]
			});		
		}
	},
	
	updateState: function(event) {
		switch(event.target.dataset.lang) {
			case "default":
				this.setState( { "def": event.target.value } );
				this.props.onChange("name-def",{ "name-def": event.target.value });
				break;
			case "deu":
				this.setState( { "de": event.target.value } );
				this.props.onChange("name-de",{ "name-de": event.target.value });
				break;
			case "en":
				this.setState( { "en": event.target.value } );
				this.props.onChange("name-en",{ "name-en": event.target.value });
				break;
		}
	},
	
	handleIndexChanged: function(event) {
		this.setState({ index: event.target.selectedIndex });
	},
	
	render: function() {
		var LocalizedStringNodes = this.data.localizedStrings.map(function (parseLocalizedString,i) {
		  return (
			<option key={parseLocalizedString.objectId} value={i+1} >
				{parseLocalizedString[this.props.attributeName].deu}
			</option>
		  );
		},this);
		
		return (
			<Panel id={this.props.id} header={this.props.placeholders.de} bsStyle='primary'>
				<Row>
					<Col md={4}>
						<Input ref="select" type='select' label={this.props.placeholders.de + " auswählen"} selectedIndex={this.state.index} onChange={this.loadLocalizedStrings} >
							<option value={0} >
								{this.props.dropdownDescription}
							</option>
							{LocalizedStringNodes}
						</Input>
					</Col>
				</Row>
				<Row>
					<Col md={4}>
						<h4><Label bsStyle="info">Standard</Label></h4>
						<input type="text" className="form-control" onChange={this.updateState} data-lang="default"
							placeholder={this.props.placeholders.en} 
							value={this.state["def"]} />
					</Col>
					<Col md={4}>
						<h4><Label bsStyle="info">Deutsch</Label></h4>
						<input type="text" className="form-control" onChange={this.updateState} data-lang="deu"
							placeholder={this.props.placeholders.de} 
							value={this.state.de} />
					</Col>
					<Col md={4}>
						<h4><Label bsStyle="info">Englisch</Label></h4>
						<input type="text" className="form-control" onChange={this.updateState} data-lang="en"
							placeholder={this.props.placeholders.en} 
							value={this.state.en} />
					</Col>
				</Row>
			</Panel>
		);
	}
});

module.exports = LocalizedString;