import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import {
  Panel, Row, Col, Label,
  Thumbnail, Pagination, Glyphicon, ProgressBar,
  Button, ButtonGroup
} from 'react-bootstrap';

import Dropzone from 'react-dropzone';

import SmartInput from './smart-input';

var update = require('react-addons-update');

var ImagePicker = React.createClass({

  mixins: [ParseReact.Mixin],

  observe() {
    return {
      images: (new Parse.Query(this.props.type)).descending("updatedAt")
    };
  },

  getDefaultProps() {
    return {
      parseServerURL: "http://george.mnd.thm.de:1337/parse",
      parseAppId: "george.games.thm.de",
      parseMKey: "6YunNDY4&Hf2R2sH4JmP7BZJHZgvH]",
      parseRestKey: "bBw(fk2/wVu3j#8X"
    };
  },

  getInitialState() {
    return {
      activePage: 1,
      loadingFiles: [],
      imagesPerRow: this.props.type=="Image" ? 4 : 12,
      numRows: this.props.type=="Image" ? 1 : 2,
      imagesPerRowSelectedIndex: this.props.type=="Image" ? 0 : 2,
      rowSelectedIndex: this.props.type=="Image" ? 0 : 1,
      numPages: 1,
    };
  },

  componentWillReceiveProps(nextProps) {
    this.setState({ activeImage: nextProps.activeImage });
  },

  componentDidUpdate(prevProps,prevState) {
    if( this.pendingQueries().length == 0 &&
        Math.ceil(this.data.images.length/this.state.imagesPerRow/this.state.numRows) != this.state.numPages)
      this.setState({ numPages: Math.ceil(this.data.images.length/this.state.imagesPerRow/this.state.numRows) });
  },

  onChange() {
    this.props.onChange(this.state.activeImage);
  },

  handleUnsetImageClicked() {
    this.setState({ activeImage: undefined },this.onChange);
  },

  handleClick(image,index,event) {
    if(this.state.activeImage == undefined || image.objectId != this.state.activeImage.objectId) {
      this.setState({ activeImage: image },this.onChange);
    }
  },

  handleSelect(event, selectedEvent) {
    if(selectedEvent.eventKey >= 1  && selectedEvent.eventKey <= this.state.numPages)
      this.setState({ activePage: selectedEvent.eventKey });
  },

  onDrop(files) {
    var uploadFileRequest =[];

    this.setState({ loadingFiles : [] });

    function updateProgress(index,event) {
      if(event.lengthComputable) {
        var json = {};
        json[index] = { uploadProgress: { $set: event.loaded / event.total * 100 } };
        var loadingFilesUpdated = update(this.state.loadingFiles, json);

        this.setState({ loadingFiles: loadingFilesUpdated });
      }
    }

    var loadingFilesUpdated;

    for(var i=0; i < files.length; i++) {
      loadingFilesUpdated = update(this.state.loadingFiles, {
        $push :[{
          name: files[i].name,
          uploadProgress: 0,
          progressBarText: '%(percent)s%',
          progressBarStyle: undefined,
          // loading: true,
          associated: false,
          visible: true
        }]
      });
      this.setState({ loadingFiles : loadingFilesUpdated });

      uploadFileRequest[i] = new XMLHttpRequest();
      uploadFileRequest[i].onreadystatechange=function(index) {
        if(uploadFileRequest[index].readyState==4 && uploadFileRequest[index].status==201 ) {
          var associateFileRequest = new XMLHttpRequest();

          var json = {};
          json[index] = { uploadProgress: { $set: 100 } };
          var loadingFilesUpdated = update(this.state.loadingFiles, json);

          this.setState({ loadingFiles: loadingFilesUpdated });

          associateFileRequest.onreadystatechange=function(index) {
            if(associateFileRequest.readyState==4 && associateFileRequest.status==201) {
              var json = {};
              json[index] = {
                associated: { $set: true },
                progressBarStyle: { $set: "success" }
              };
              var loadingFilesUpdated = update(this.state.loadingFiles, json);

              this.setState({ loadingFiles: loadingFilesUpdated });

              this.refreshQueries();

              setTimeout(this.removeProgress.bind(this, index), 3000);
            } else if(associateFileRequest.readyState==4) {
              this.onImageUploadFailed(index);
              setTimeout(this.removeProgress.bind(this, index), 3000);
            }
          }.bind(this,index);

          var jsonResponse = JSON.parse(uploadFileRequest[index].response);
          associateFileRequest.open("POST",this.props.parseServerURL + "/classes/" + this.props.type,true);
          associateFileRequest.setRequestHeader("X-Parse-Application-Id",this.props.parseAppId);
          associateFileRequest.setRequestHeader("X-Parse-REST-API-Key",this.props.parseRestKey);
          associateFileRequest.setRequestHeader("X-Parse-Session-Token",Parse.User.current().getSessionToken());
          associateFileRequest.setRequestHeader("Content-type","application/json");
          associateFileRequest.send('{  "' + this.props.type.toLowerCase() + '":{  "name": "' + jsonResponse.name + '","__type": "File" }  }');
        } else if(uploadFileRequest[index].readyState==4) {
          this.onImageUploadFailed(index);
          setTimeout(this.removeProgress.bind(this, index), 3000);
        }
      }.bind(this,i);
      uploadFileRequest[i].upload.addEventListener("progress", updateProgress.bind(this,i), false);
      uploadFileRequest[i].open("POST",this.props.parseServerURL + "/files/" + files[i].name,true);
      uploadFileRequest[i].setRequestHeader("X-Parse-Application-Id",this.props.parseAppId);
      uploadFileRequest[i].setRequestHeader("X-Parse-REST-API-Key",this.props.parseRestKey);
      uploadFileRequest[i].setRequestHeader("X-Parse-Session-Token",Parse.User.current().getSessionToken());
      uploadFileRequest[i].send(files[i]);

    }
  },

  onImageUploadFailed(index) {
    var json = {};
    json[index] = {
      progressBarText: { $set: "Upload fehlgeschlagen" },
      progressBarStyle: { $set: "danger" }
    };
    var loadingFilesUpdated = update(this.state.loadingFiles, json);

    this.setState({ loadingFiles: loadingFilesUpdated });
  },

  removeProgress(index) {
    var json = {};

    json[index] = { visible: { $set: false } };
    var loadingFilesUpdated = update(this.state.loadingFiles, json);
    this.setState({ loadingFiles: loadingFilesUpdated });
  },

  onImagesPerRowChange(selectedIndex,value) {
    var numPages = Math.ceil(this.data.images.length/Number(value)/this.state.numRows);

    if(numPages < this.state.activePage) this.setState({activePage: numPages});

    this.setState({
      imagesPerRowSelectedIndex: selectedIndex,
      imagesPerRow: Number(value)
    });
  },

  onRowChange(selectedIndex,value) {
    var numPages = Math.ceil(this.data.images.length/this.state.imagesPerRow/Number(value));
    if(numPages < this.state.activePage) this.setState({activePage: numPages});

    this.setState({
      rowSelectedIndex: selectedIndex,
      numRows: Number(value)
    });
  },

  render() {
    var nImages = this.state.imagesPerRow*this.state.numRows;
    var colMd = 12/this.state.imagesPerRow;
    var activeImageObjectId = this.state.activeImage
    ? this.state.activeImage.objectId
    : undefined;

    var imageNodes = this.data.images.slice((this.state.activePage-1)*nImages,this.state.activePage*nImages).map(function (image,i) {
      var indexAtFileName = image[this.props.type.toLowerCase()].name().lastIndexOf("-") != -1
        ? image[this.props.type.toLowerCase()].name().lastIndexOf("-")
        : image[this.props.type.toLowerCase()].name().indexOf("_");
      return (
        <Col
          key={image.objectId}
          md={colMd}
          style={ { paddingLeft: "5px", paddingRight: "5px" } }
        >
          <Thumbnail
            src={image[this.props.type.toLowerCase()].url()}
            onClick={this.handleClick.bind(this,image,i)}
            className={
              image.objectId == activeImageObjectId ? "active" : undefined
            }
          >
          { this.state.imagesPerRow != 12 ?
              <p className="text-center" >
                Name: { image[this.props.type.toLowerCase()].name()
                        .substr(indexAtFileName+1)
                        .split(".")[0] }
              </p>
            : undefined
          }
          </Thumbnail>
        </Col>
      );
    },this);

    var imageRows = [], i= 0;
    while(imageNodes.length != 0) {
      imageRows[i++] = imageNodes.splice(0,this.state.imagesPerRow);
    }
    imageNodes = imageRows.map(function(imageNode,i) {
      return(
        <Row key={i} style={ { paddingLeft: "5px", paddingRight: "5px" } } >
          {imageNode}
        </Row>
      );
    });

    var fileLoadingProgresses = this.state.loadingFiles.map(function(loadingFile,index) {
      return(
        <Row key={index} style={ !loadingFile.visible ? {display:"none"} : {} } >
          <Col md={12} >
            Lade Bild {loadingFile.name} hoch:
          </Col>
          <Col md={12} >
            <ProgressBar
              now={loadingFile.uploadProgress}
              label={loadingFile.progressBarText}
              // style={ !loadingFile.loading ? {display:"none"} : {} }
              bsStyle={loadingFile.progressBarStyle}
            />
          </Col>
        </Row>
      );
    },this);

    var indexAtActiveImageFileName;

    if(this.state.activeImage) {
      indexAtActiveImageFileName = this.state.activeImage[this.props.type.toLowerCase()].name().lastIndexOf("-") != -1
      ? this.state.activeImage[this.props.type.toLowerCase()].name().lastIndexOf("-")
      : this.state.activeImage[this.props.type.toLowerCase()].name().indexOf("_")
    }

    return (
      <Panel
        id={this.props.id}
        header={this.props.header}
        bsStyle='primary'
      >
        <Row style={ { height: "300px" } } >
          <Col md={4} mdOffset={4} >
            <h4 style={ { textAlign: "center" } } >
              <Label>Ausgew&auml;hltes Bild</Label>
            </h4>
            {
              this.state.activeImage
              ? <Thumbnail
                  style={ { maxHeight: "240px" } }
                  src={this.state.activeImage[this.props.type.toLowerCase()].url()}
                >
                  <p className="text-center" >
                    Name: {
                      this.state.activeImage[this.props.type.toLowerCase()]
                        .name()
                        .substr(indexAtActiveImageFileName+1)
                        .split(".")[0]
                    }
                  </p>
                </Thumbnail>
              : <h4 className="text-center" style={ { marginTop: "120px" } } > Kein Bild ausgew&auml;hlt. </h4>
            }
            {
              this.state.activeImage && this.props.showUnsetButton &&
              <Button className="center-block" onClick={this.handleUnsetImageClicked}>
                Zurücksetzen auf "kein Bild"
              </Button>
            }
          </Col>
        </Row>
        <Row>
          <Col md={8} >
            <Pagination
              prev={true}
              next={true}
              first={true}
              last={true}
              ellipsis={true}
              items={Math.ceil(this.data.images.length/this.state.numRows/this.state.imagesPerRow)}
              maxButtons={Math.min(10,this.state.numPages)}
              activePage={this.state.activePage}
              onSelect={this.handleSelect}
            />
          </Col>
          <Col md={2} >
            <SmartInput
              type='select'
              label='Bilder pro Zeile'
              selectedIndex={this.state.imagesPerRowSelectedIndex}
              onChange={this.onImagesPerRowChange}
            >
              <option value={4}>4</option>
              <option value={6}>6</option>
              <option value={12}>12</option>
            </SmartInput>
          </Col>
          <Col md={2} >
            <SmartInput
              type='select'
              label='Zeilen'
              selectedIndex={this.state.rowSelectedIndex}
              onChange={this.onRowChange}
            >
              <option value={1} >1</option>
              <option value={2} >2</option>
              <option value={3} >3</option>
              <option value={4} >4</option>
              <option value={5} >5</option>
            </SmartInput>
          </Col>
        </Row>
        <div>
          {imageNodes}
        </div>
        <Dropzone
          onDrop={this.onDrop}
          style={ { width:"100%", height:"200px", borderWidth: 2, borderColor: '#666', borderStyle: 'dashed', borderRadius: 5 } }
          activeStyle = { { borderStyle: 'solid', backgroundColor: '#eee' } }
        >
          <img src="/img/cloud-upload.png"
            style={ { marginLeft:"auto",marginRight:"auto", height:"150px", width:"150px", display:"block" } } />
          <p className="text-center"> Bilder per Drag & Drop hier reinziehen oder hier klicken. </p>
        </Dropzone>
        {fileLoadingProgresses}
      </Panel>
    );
  },

});

module.exports = ImagePicker;
