import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import { Input } from 'react-bootstrap';

var SmartInput = React.createClass({

  mixins: [ParseReact.Mixin],

  observe(props,state) {
    return {
      localizedString: (new Parse.Query("LocalizedString")).equalTo("deu",state.checkLocalizedString),
    };
  },

  getInitialState() {
    return {
      type: undefined,
      initialValue: "",
      value: "",
      placeholder: undefined,
      initialChecked: undefined,
      checked: undefined,
      initialSelectedIndex: undefined,
      selectedIndex: undefined,
      label: undefined,
      disabled: undefined,
      buttonBefore: undefined,
      checkLocalizedString: "",
    };
  },

  updateState(properties) {
    var initialValue = properties.initialValue && properties.initialValue.deu
      ? properties.initialValue.deu
      : properties.initialValue;
    var value = properties.value && properties.value.deu
      ? properties.value.deu
      : properties.value;
    value = value ? value : initialValue;

    this.setState({
      type: properties.type,
      initialValue: initialValue,
      value: value,
      placeholder: properties.placeholder,
      initialChecked: properties.checked,
      checked: properties.checked,
      initialSelectedIndex: properties.selectedIndex,
      selectedIndex: properties.selectedIndex,
      label: properties.label,
      disabled: properties.disabled,
      addonBefore: properties.addonBefore,
      buttonBefore: properties.buttonBefore,
      addonAfter: properties.addonAfter,
      buttonAfter: properties.buttonAfter
    });
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  componentDidUpdate() {
    if(this.refs.input && this.refs.input.getInputDOMNode())
      this.refs.input.getInputDOMNode().selectedIndex = this.state.selectedIndex;

    if(this.data.localizedString.length != 0)
      this.props.onChange(
        this.data.localizedString[0].deu != this.state.initialValue ?
          this.data.localizedString[0]
        : undefined
      );
  },

  onChange(e) {
    if (this.state.type == "text") {
      this.props.onChange(
          e.target.value != this.state.initialValue &&
          e.target.value.length != 0
              ? e.target.value
              : undefined
      );
      this.setState({value: e.target.value});

      if (this.props.checkLocalizedString == true) {
        if (this.timeOut) clearTimeout(this.timeOut);
        this.timeOut = setTimeout(function (value) {
          this.setState({checkLocalizedString: value});
        }.bind(this, e.target.value), 500);
      }
    } else if (this.state.type == "checkbox") {
      this.props.onChange(
          e.target.checked != this.state.initialChecked ?
              e.target.checked
              : undefined
      );
      this.setState({checked: e.target.checked});
    }
    else if(this.state.type == "file") {
      this.props.onChange(e.target.files);
    }
    else { // type is select
      this.props.onChange(
          e.target.selectedIndex != this.state.initialSelectedIndex ?
              e.target.selectedIndex
              : undefined,
          e.target.value
      );
      this.setState({selectedIndex: e.target.selectedIndex});
    }
  },

  render() {
    var showWarning = this.data.localizedString.length != 0 &&
      this.data.localizedString[0].deu != this.state.initialValue;

    return (
      <Input
        ref="input"
        type={this.state.type}
        value={this.state.value}
        placeholder={this.state.placeholder}
        checked={this.state.checked}
        bsStyle={ showWarning ? "warning" : undefined }
        label={
          showWarning ?
            this.state.label + " existiert bereits"
          : this.state.label
        }
        disabled={this.state.disabled}
        addonBefore={this.state.addonBefore}
        buttonBefore={this.state.buttonBefore}
        addonAfter={this.state.addonAfter}
        buttonAfter={this.state.buttonAfter}
        hasFeedback={showWarning}
        onChange={this.onChange}
      >
        {this.props.children}
      </Input>
    );
  },
});

module.exports = SmartInput;
