import React from 'react';

import { ButtonToolbar, Button, Glyphicon, Table, Alert } from 'react-bootstrap';

import { Link } from 'react-router';
import { LinkContainer } from 'react-router-bootstrap';

var ClassTable = React.createClass({

  getInitialState() {
    return {
      selectedClasses: []
    };
  },

  componentWillReceiveProps(nextProps) {
    this.setState({ selectedClasses: nextProps.selectedClasses });
  },

  handleClassClick(parseClass) {
    this.props.onClassClick(parseClass);
    this.props.onClassChecked([parseClass.objectId]);
  },

  handleToggleCheck(classId,e) {
    var selectedClasses = this.state.selectedClasses.slice();
    var selectFactionsIndex = selectedClasses.indexOf(classId);

    if(selectFactionsIndex != -1) {
      selectedClasses.splice(selectFactionsIndex,1);
    } else {
      selectedClasses.push(classId);
    }

    this.setState({selectedClasses: selectedClasses});
    this.props.onClassChecked(selectedClasses);
  },

  handleToggleCheckAll(e) {
    var allChecked, selectedClasses;

    if( this.state.allChecked != true ) {
      allChecked = true;
      selectedClasses = this.props.data.map(function(classRow) {
        for (var i = 0; i < classRow.length; i++) {
          if(classRow[i].type == "object") return classRow[i].id;
        }
      });
    } else {
      allChecked = false;
      selectedClasses = [];
    }

    this.setState({
      allChecked: allChecked,
      selectedClasses: selectedClasses
    });
    this.props.onClassChecked(selectedClasses);
  },

  handleDeleteClick() {
    if(this.state.selectedClasses.length != 0) this.props.onDeleteClick();
  },

  handleColumnHeaderClicked(columnIndex) {
    console.log("column " + columnIndex + " clicked");
  },

  render() {
    var classNodes;

    if(this.props.data) {
      classNodes = this.props.data.map(function(classRow,i) {
        var objId;
        var classRowNodes = this.props.data[i].map(function(classCol,j) {
          switch(classCol.type) {
            case "bool":
              return(
                <td key={"Col" + (j+1)}
                  style={ { textAlign: "center" } }
                >
                  <Glyphicon glyph={ classCol.value ? "check" : "unchecked" } />
                </td>
              );
            case "string":
              return(
                <td key={"Col" + (j+1)} >
                  {
                    classCol.value != undefined
                    ? classCol.value
                    : <span style={ { "color": "orange" } } >Nicht zugewiesen</span>
                  }
                </td>
              );
            case "object":
              objId = classCol.id;
              return(
                <td key={"Col" + (j+1)} >
                  <Link to={ this.props.classRoute + "/" + classCol.id } >
                    {
                      classCol.value
                      ? (classCol.value.deu ? classCol.value.deu : classCol.value)
                      : <span style={ { "color": "orange" } } >Nicht zugewiesen</span>
                    }
                  </Link>
                </td>
              );
            case "pointer":
              return(
                <td key={"Col" + (j+1)} >
                  {
                    classCol.value != undefined
                    ? <Link to={
                        classCol.value.parentId
                        ? classCol.classRoute
                            .replace(":pid",classCol.value.parentId)
                            .replace(":id",classCol.value.objectId)
                        : classCol.classRoute
                            .replace(":id",classCol.value.objectId)
                      } >
                        {classCol.value.name.deu}
                      </Link>
                    : <span style={ { "color": "red" } } >Nicht zugeordnet</span>
                  }
                </td>
              );
            case "array":
              var objectLinks = [];
              if(classCol.values && classCol.values[0]) {
                for(var k = 0; k < classCol.values.length; k++) {
                  objectLinks.push(
                    <Link key={"Link" + k}
                    to={classCol.classRoute.replace(":pid",objId).replace(":id",classCol.ids[k])}
                    style={{ "float": "left" }}
                    >
                    {classCol.values[k]}
                    </Link>
                  );
                  if(k != classCol.values.length-1)
                  objectLinks.push(<span key={"Link" + k + "seperator"} style={ {"float": "left" } }>,&nbsp;</span>);
                }
              }
              return(
                <td key={"Col" + (j+1)} >
                  {
                    objectLinks.length != 0
                    ? objectLinks
                    : <span style={ { "color": "orange" } } >Keine zugeordnet</span>
                  }
                </td>
              );
          }
        },this);

        return (
          <tr key={objId}
            style={ {
              backgroundColor: this.state.selectedClasses.indexOf(objId) == -1 ? "#fff" : "#ccc",
              color: this.state.selectedClasses.indexOf(objId) == -1 ? "#000" : "#fff"
            } }
          >
            <td>
              <Glyphicon
                glyph={ this.state.selectedClasses.indexOf(objId) == -1 ? "unchecked" : "check" }
                onClick={ this.handleToggleCheck.bind(this,objId) }
              />
            </td>
            {classRowNodes}
          </tr>
        );
      },this);
    }

    var columnHeaders = this.props.columnNames.map(function(columnName,i) {
      return (
        <th key={"th"+i}
          onClick={this.handleColumnHeaderClicked.bind(this,i)}
          style={
            this.props.data && this.props.data[0] &&
            this.props.data[0][i].type == "pointer"
            ? { minWidth: "125px" }
            :  undefined
          }
        >
          {columnName}
        </th>
      )
    },this);

    return (
      <div id={this.props.id} style={ {position: "inherit"} } >
        <ButtonToolbar>
          <LinkContainer to={ { pathname: this.props.classRoute + "/new" } } >
            <Button>
              <Glyphicon glyph="plus" /> {this.props.displayName} hinzuf&uuml;gen
            </Button>
          </LinkContainer>
          <Button disabled={this.state.selectedClasses.length == 0} onClick={this.handleDeleteClick} >
            <Glyphicon glyph="trash" /> Ausgew&auml;hlte l&ouml;schen
          </Button>
        </ButtonToolbar>
        <div style={
          {
            position: "inherit",
            top: "96px",
            left: "15px",
            bottom: "5px",
            right: "15px",
            marginTop: "10px",
            overflowY: "scroll"
          }
        } >
          { classNodes && classNodes.length != 0 ?
              <Table bordered hover>
                <thead>
                  <tr>
                    <th style={{width: "30px"}} >
                      <Glyphicon
                        glyph={ this.state.allChecked != true ? "unchecked" : "check" }
                        onClick={this.handleToggleCheckAll}
                      />
                    </th>
                    {columnHeaders}
                  </tr>
                </thead>
                <tbody>
                  {classNodes}
                </tbody>
              </Table>
            : this.props.data && this.props.data.length === 0 ?
              <Alert bsStyle='info' style={ { marginBottom: "0" } } >
                <b><Glyphicon glyph="info-sign" />&nbsp;Es ist noch kein(e) {this.props.displayName} vorhanden</b>
              </Alert>
            : undefined
          }
        </div>
      </div>
    );
  }
});

module.exports = ClassTable;
