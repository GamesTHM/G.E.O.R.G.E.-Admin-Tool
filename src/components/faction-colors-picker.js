import React from 'react';

import { Panel, Row, Col, Label } from 'react-bootstrap';

import ColorPicker from 'react-color';

var FactionColorsPicker = React.createClass({

  getInitialState() {
    return {
      mainColor: this.props.mainColor ? this.props.mainColor : '#000000',
      textColor: this.props.textColor ? this.props.textColor : '#FFFFFF',
    };
  },

  componentWillReceiveProps(nextProps) {
    this.setState({
      mainColor: nextProps.mainColor,
      textColor: nextProps.textColor
    });
  },

  handleMainColorChange(color) {
    var mainColor = "#" + color.hex;

    this.props.onChange({
      mainColor: mainColor,
      textColor: this.state.textColor
    });

    this.setState({
      mainColor: mainColor,
    });
  },

  handleTextColorChange(color) {
    var textColor = "#" + color.hex;

    this.props.onChange({
      mainColor: this.state.mainColor,
      textColor: textColor
    });

    this.setState({
      textColor: textColor,
    });
  },

  render() {
    return (
      <Panel
        id={this.props.id}
        header={this.props.header}
        bsStyle='primary'
      >
        <Row>
          <Col
            ref="mainPrev"
            md={3}
            mdOffset={3}
            style={
              {
                borderRadius: "3px 0 0 3px",
                marginRight: "1px",
                color: this.state.textColor,
                backgroundColor: this.state.mainColor
              }
            }
          >
            Vorschau
          </Col>
          <Col
            ref="secondPrev"
            md={3}
            style={
              {
                borderRadius: "0 3px 3px 0",
                height: "20px",
                backgroundColor: this.state.textColor
              }
            } />
        </Row>
        <Row>
          <Col md={6} >
            <div style={ { "float" : "right" } } >
              <h4><Label>Hauptfarbe</Label></h4>
            </div>
            <div style={ { "float" : "right", "clear": "right" } } >
              <ColorPicker
                type="chrome"
                color={this.state.mainColor}
                onChange={this.handleMainColorChange}
              />
            </div>
          </Col>
          <Col md={6} >
            <h4><Label>Textfarbe</Label></h4>
            <ColorPicker
              type="chrome"
              color={this.state.textColor}
              onChange={this.handleTextColorChange}
            />
          </Col>
        </Row>
      </Panel>
    );
  },
});

module.exports = FactionColorsPicker;
