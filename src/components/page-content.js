import React from 'react';

import { Col } from 'react-bootstrap';

var PageContent = React.createClass({
  render() {
    return (
      <Col md={10} mdOffset={2}
        style={
          {
            position: "fixed",
            top:"56px",
            bottom:0,
            overflowY:"scroll"
          }
        }
        data-spy="scroll"
        data-target="#sidebar"
        data-offset="50"
      >
        {this.props.children}
      </Col>
    );
  }
});

module.exports = PageContent;
