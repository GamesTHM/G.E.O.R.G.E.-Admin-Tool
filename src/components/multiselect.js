import React from 'react';

import { Multiselect } from 'react-widgets';

var MyMultiselect = React.createClass({

  getInitialState() {
    return {
      data: [],
    };
  },

  updateState(properties) {
    this.setState({
      data: properties.data != undefined ? properties.data : [],
      value: properties.value
    })
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextProps.data != this.props.data ||
      nextProps.value != this.props.value ||
      nextState.data != this.state.data ||
      nextState.value != this.state.value
    );
  },

  passChangeToParent() {
    this.props.onChange(this.state.value);
  },

  handleChange(value) {
    this.setState({ value: value },this.passChangeToParent);
  },

  render() {
    return (
      <Multiselect
        valueField='objectId' textField='name'
        data={this.state.data}
        value={this.state.value}
        onChange={this.handleChange}
        style={{marginBottom:"20px"}}
      />
    );
  },
});

module.exports = MyMultiselect;
