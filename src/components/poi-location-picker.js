import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import { Row, Col } from 'react-bootstrap';

import SmartInput from '../components/smart-input';

var PoiLocationPicker = React.createClass({

  mixins: [ParseReact.Mixin],

  getInitialState() {
    return {
      poiCampusSelectedIndex: 0,
      campusFloorLevelSelectedIndex: 0,
      campusFloorLevels: []
    }
  },

  observe(props,state) {
    return {
      campusses: (new Parse.Query("Campus")).include("name"),
      campusFloorLevels: (new Parse.Query("CampusFloorLevel")).include("name"),
    };
  },

  updateComponentStates(properties) {
    this.setState({
      currentPoiId: properties.poiId,
      currentCampusId: properties.campusId,
      determineInitialValues: this.state.currentPoiId != properties.poiId
        || this.state.determineInitialValues
    });
  },

  componentWillReceiveProps(nextProps) {
    this.updateComponentStates(nextProps);
  },

  determineInitialValues() {
    var poiCampusSelectedIndex = 0, campusFloorLevelSelectedIndex = 0;
    var campusFloorLevels = [];

    if(this.state.currentCampusId != undefined) {
      for(var i = 0; i < this.data.campusses.length; i++) {
        if(this.data.campusses[i].objectId == this.state.currentCampusId) {
          poiCampusSelectedIndex = i+1;
          break;
        }
      }

      campusFloorLevels = this.determineCampusFloorLevels(poiCampusSelectedIndex);
    }

    if(this.state.currentPoiId != undefined) {
      for(var i = 0; i < campusFloorLevels.length; i++) {
        if(campusFloorLevels[i].Locations != undefined) {
          for(var j = 0; j < campusFloorLevels[i].Locations.length; j++) {
            if(campusFloorLevels[i].Locations[j].objectId == this.state.currentPoiId) {
              campusFloorLevelSelectedIndex = i+1;
              break;
            }
          }
        }
      }
    }

    this.setState({
      poiCampusSelectedIndex: poiCampusSelectedIndex,
      poiCampusSelectedIndexInit: poiCampusSelectedIndex,
      campusFloorLevels: campusFloorLevels,
      campusFloorLevelSelectedIndex: campusFloorLevelSelectedIndex,
      campusFloorLevelSelectedIndexInit: campusFloorLevelSelectedIndex,
      currentCampusFloorLevel: campusFloorLevels[campusFloorLevelSelectedIndex-1],
      determineInitialValues: false
    });
  },

  determineCampusFloorLevels(poiCampusSelectedIndex) {
    var campusFloorLevels = [];

    if(this.data.campusses[poiCampusSelectedIndex-1].CampusFloorLevels != undefined) {
      for(var i = 0; i < this.data.campusFloorLevels.length; i++) {
        for(var j = 0; j < this.data.campusses[poiCampusSelectedIndex-1].CampusFloorLevels.length; j++) {
          if(this.data.campusFloorLevels[i].objectId == this.data.campusses[poiCampusSelectedIndex-1].CampusFloorLevels[j].objectId) {
            campusFloorLevels.push(this.data.campusFloorLevels[i]);
          }
        }
      }
    }
    return campusFloorLevels;
  },

  componentDidUpdate(prevProps,prevState) {
    if(this.state.determineInitialValues == true && this.pendingQueries().length == 0)
      this.determineInitialValues();
  },

  onChange() {
    this.props.onChange({
      campus: this.data.campusses[this.state.poiCampusSelectedIndex-1],
      campusChanged: this.state.poiCampusSelectedIndex != this.state.poiCampusSelectedIndexInit,
      campusFloorLevel: this.state.campusFloorLevelSelectedIndex != 0
        ? this.state.campusFloorLevels[this.state.campusFloorLevelSelectedIndex-1]
        : undefined,
      currentCampusFloorLevel: this.state.currentCampusFloorLevel,
      campusFloorLevelChanged: this.state.campusFloorLevelSelectedIndex != this.state.campusFloorLevelSelectedIndexInit ||
        this.state.poiCampusSelectedIndex != this.state.poiCampusSelectedIndexInit
    });
  },

  onPoiCampusChange(selectedIndex) {
    this.setState({
      poiCampusSelectedIndex: selectedIndex,
      campusFloorLevelSelectedIndex: 0,
      campusFloorLevels: this.determineCampusFloorLevels(selectedIndex),
    },this.onChange);
  },

  onCampusFloorLevelChange(selectedIndex) {
    this.setState({campusFloorLevelSelectedIndex: selectedIndex}, this.onChange);
  },

  render() {
    var campusses = this.data.campusses.map(function(campus) {
      return <option key={campus.objectId} >{campus.name.deu}</option>
    });

    var campusFloorLevels = this.state.campusFloorLevels.map(function(campusFloorLevel) {
      return <option key={campusFloorLevel.objectId} >{campusFloorLevel.name.deu}</option>
    });

    return(
      <Row>
        <Col md={6} >
          <SmartInput
            type='select'
            label='Campus-Zuordnung'
            selectedIndex={this.state.poiCampusSelectedIndex}
            onChange={this.onPoiCampusChange}
          >
            <option disabled hidden>Campus auswählen</option>
            {campusses}
          </SmartInput>
        </Col>
        <Col md={6} >
          <SmartInput
            type='select'
            label='Stockwerk-Zuordnung'
            disabled={this.state.poiCampusSelectedIndex == 0}
            selectedIndex={this.state.campusFloorLevelSelectedIndex}
            onChange={this.onCampusFloorLevelChange}
          >
            <option disabled hidden>
              {this.state.poiCampusSelectedIndex == 0 ? 'Zuerst Campus auswählen' : 'Stockwerk auswählen'}
            </option>
            {campusFloorLevels}
          </SmartInput>
        </Col>
      </Row>
    );
  }
});

module.exports = PoiLocationPicker;
