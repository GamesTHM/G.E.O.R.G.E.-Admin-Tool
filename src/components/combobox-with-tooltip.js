import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

import {
  Row, Col, OverlayTrigger, Tooltip, Button, Glyphicon
} from 'react-bootstrap';

import { Combobox } from 'react-widgets';

var ComboboxWithTooltip = React.createClass({

  getInitialState() {
    return {
      value: undefined,
      data: undefined
    };
  },

  updateState(properties) {
    this.setState({
      value: properties.value,
      data: properties.data
    })
  },

  componentDidMount() {
    this.updateState(this.props);
  },

  componentWillReceiveProps(nextProps) {
    this.updateState(nextProps);
  },

  shouldComponentUpdate(nextProps,nextState) {
    return nextProps.hasOwnProperty("value");
  },

  onChange() {
    this.props.onChange(this.state.value);
  },

  onValueChange(value) {
    this.setState({ value: value },this.onChange);
  },

  render() {
    return (
      <div>
        <b>{this.props.label}</b>
        <Row>
          <Col md={11} style={ { paddingRight: "0" } } >
            {
              this.state.data != undefined ?
                <Combobox
                  valueField='objectId' textField='deu'
                  data={this.state.data}
                  value={this.state.value}
                  onChange={this.onValueChange}
                  style={ {marginTop:"5px", marginBottom:"20px"} }
                />
              : undefined
            }
          </Col>
          <Col md={1} style={ { padding: "0" } } >
            <OverlayTrigger
              placement='top'
              overlay={
                <Tooltip id={this.props.id}>
                  Es kann sowohl ein neuer Text eingegeben, als auch ein existierender ausgewählt werden
                </Tooltip>
              }
            >
              <Button bsStyle='default' style={ {marginTop: "5px"} } >
                <Glyphicon glyph="question-sign" />
              </Button>
            </OverlayTrigger>
          </Col>
        </Row>
      </div>
    );
  },
});

module.exports = ComboboxWithTooltip;
