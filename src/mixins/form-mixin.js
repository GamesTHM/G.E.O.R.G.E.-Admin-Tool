import React from 'react';
import { Parse } from 'parse';
import ParseReact from 'parse-react';

var formMixin = {

  contextTypes: {
    router: React.PropTypes.object
  },

  getInitialState() {
    return {
      updatePage: false,
      showAlert: false,
      showModal: false,
      initializeClassPageData: true,
    };
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextState.updatePage == true ||
      nextState.showAlert != this.state.showAlert ||
      nextState.showModal != this.state.showModal ||
      nextState.alertMessage != this.state.alertMessage ||
      nextState.createClass == true ||
      nextState.changeClass == true
    );
  },

  mutationSuccess(message) {
    this.replaceState({
      showAlert: true,
      alertMessage: message,
      alertStyle: "success",
      alertDismissAfter: 3000,
      showModal: false,
      updatePage: false,
    });
  },

  mutationSaveSuccess() {
    this.mutationSuccess("Gespeichert");
  },

  mutationError(title,text,mode) {
    this.setState({
      showAlert: false,
      showModal: true,
      modalTitle: title,
      modalText: text,
      modalMode: mode,
      showSecondButton: true,
    });
  },

  mutationSaveError(error) {
    this.mutationError("Speichern fehlgeschlagen","Beim Speichern ist etwas schief gegangen.","save");
  },

  alertBeforeMutationDispatch(message) {
    this.setState({
      showAlert: true,
      alertMessage: message,
      alertStyle: "warning",
      alertDismissAfter: 3000,
    });
  },

  toLocalizedStringCreateMutation(localizedString, changes) {
    if( localizedString == undefined && changes != undefined && changes.id == undefined)
    {
      return (
        ParseReact.Mutation.Create("LocalizedString",{
          "default": changes,
          deu: changes,
          en: changes
        })
      );
    } else {
      return undefined;
    }
  },

  toLocalizedStringSetMutation(localizedString, changes) {
    if( localizedString != undefined && localizedString.objectId != undefined &&
        changes != undefined && changes.objectId == undefined)
    {
      return (
        ParseReact.Mutation.Set(localizedString,{
          deu: changes,
        })
      );
    } else {
      return undefined;
    }
  },

  toWebElementCreateMutation(webElement, changes) {
    if( webElement == undefined && changes != undefined && changes.id == undefined)
    {
      return (
        ParseReact.Mutation.Create("WebElement",{
          content: changes.content,
          // contentStyle: changes.contentStyle
        })
      );
    } else {
      return undefined;
    }
  },

  toWebElementSetMutation(webElement,changes) {
    if( webElement != undefined && webElement.objectId != undefined &&
        changes != undefined && changes.objectId == undefined) {
      if(changes.content != undefined){// || changes.contentStyle != undefined) {
        // var tmpChanges = {};
        // if(changes.content) tmpChanges.content = changes.content;
        // if(changes.contentStyle) tmpChanges.contentStyle = changes.contentStyle;
        // return ParseReact.Mutation.Set(webElement,tmpChanges);
        return ParseReact.Mutation.Set(webElement,{content: changes.content});
      }
      // return (
        // ParseReact.Mutation.Set(webElement,{
          // content: changes.content,
          // contentStyle: changes.contentStyle
        // })
      // );
    } else return undefined;
  },

  toPointerArray(jsonArray, className) {
    var Class = Parse.Object.extend(className);
    if(jsonArray != undefined) {
      var oneClass, classes = [];
      for(var i=0; i<jsonArray.length; i++) {
        oneClass = new Class();
        oneClass.id = jsonArray[i].objectId;
        oneClass.objectId = jsonArray[i].objectId;
        classes.push(oneClass);
      }
      return classes;
    }
    return undefined;
  },

  handleChange(attrName,value) {
    var changes = {};

    changes[attrName+"Changes"] = value;
    this.setState(changes);
  },

  goBackToTable() {
    this.context.router.push(this.props.classRoute);
  },

  handleAlertDismiss() {
    if(this.state.alertStyle == "success") {
      this.setState({ showAlert: false }, this.goBackToTable);
    } else {
      this.setState({ showAlert: false });
    }
  },

  closeModal() {
    this.setState({ showModal: false });
  },

};

module.exports = formMixin;
