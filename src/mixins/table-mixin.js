﻿import { Parse } from 'parse';
import ParseReact from 'parse-react';

var tableMixin = {

  getInitialState() {
    return {
      updatePage: false,
      showAlert: false,
      showModal: false,
      initializeClassTableData: true,
      selectedClasses: []
    };
  },

  shouldComponentUpdate(nextProps,nextState) {
    return (
      nextState.updatePage == true ||
      nextState.showAlert != this.state.showAlert ||
      nextState.alertMessage != this.state.alertMessage ||
      nextState.showModal != this.state.showModal
    );
  },

  createArrayDictionary(array) {
    var arrayDictionary = {};

    for(var i = 0; i < array.length; i++)
      arrayDictionary[array[i].objectId] = array[i].name.deu;

    return arrayDictionary;
  },

  generateClassTableData(classTableColProps, dataSource, arraySource) {
    var classTableData = [], classTableRowData;
    var arrayDictionary = arraySource ? this.createArrayDictionary(arraySource) : undefined;

    for(var i = 0; i < dataSource.length; i++) {
      classTableRowData = [];
      for(var j = 0, values, ids; j < classTableColProps.length; j++) {
        switch (classTableColProps[j].type) {
          case "object":
            classTableRowData.push({
              type: classTableColProps[j].type,
              id: dataSource[i].objectId,
              value: dataSource[i][classTableColProps[j].name]
            });
            break;
          case "pointer":
            classTableRowData.push({
              type: classTableColProps[j].type,
              classRoute: classTableColProps[j].classRoute,
              value: dataSource[i][classTableColProps[j].name]
            });
            break;
          case "array":
            if(dataSource[i][classTableColProps[j].name]) {
              for(var k = 0; k < dataSource[i][classTableColProps[j].name].length; k++) {
                if(k==0) {
                  values = [arrayDictionary[dataSource[i][classTableColProps[j].name][k].objectId]];
                  ids = [dataSource[i][classTableColProps[j].name][k].objectId];
                } else {
                  values.push(arrayDictionary[dataSource[i][classTableColProps[j].name][k].objectId]);
                  ids.push(dataSource[i][classTableColProps[j].name][k].objectId);
                }
              }
            } else {
              values = [];
              ids = [];
            }
            classTableRowData.push({
              type: classTableColProps[j].type,
              classRoute: classTableColProps[j].classRoute,
              values: values,
              ids: ids
            });
            break;
          default:
            classTableRowData.push({
              type: classTableColProps[j].type,
              value: dataSource[i][classTableColProps[j].name]
            });
        }
      }
      classTableData.push(classTableRowData);
    }
    return classTableData;
  },

  handleClassesSelected(selectedClasses) {
    this.setState({
      selectedClasses: selectedClasses,
    });
  },

  handleDeleteClick() {
    this.setState({
      showModal: true,
      modalTitle: "Ausgewählte " + this.props.pluralName + " löschen?",
      modalText: "Sollen die ausgewählten " + this.props.pluralName + " wirklich gelöscht werden?",
      secondButtonText: "Löschen",
    });
  },

  mutationSuccess(message) {
    this.replaceState({
      showAlert: true,
      alertMessage: message,
      alertStyle: "success",
      alertDismissAfter: 3000,
      showModal: false,
      updatePage: false,
      selectedClasses: [],
      initializeClassTableData: true
    });
  },

  mutationDeleteSuccess() {
    this.mutationSuccess("Gelöscht");
  },

  mutationError(title,text) {
    this.setState({
      showAlert: false,
      showModal: true,
      modalTitle: title,
      modalText: text,
      secondButtonText: "Nochmal versuchen"
    });
  },

  mutationDeleteError(selectedClasses,error) {
    this.setState({selectedClasses: selectedClasses});
    this.mutationError("Löschen fehlgeschlagen","Beim Löschen ist etwas schief gegangen.");
  },

  alertBeforeMutationDispatch(message) {
    this.setState({
      showAlert: true,
      alertMessage: message,
      alertStyle: "warning",
      alertDismissAfter: 3000,
    });
  },

  deleteSelectedClasses() {
    this.closeModal();
    if(this.state.selectedClasses) {
      var batch = new ParseReact.Mutation.Batch();
      var selectedClasses = [];

      this.alertBeforeMutationDispatch("Löschen...");

      for(var i = 0; i < this.state.selectedClasses.length; i++) {
        selectedClasses.push(this.state.selectedClasses[i]);
        if(selectedClasses[i] != undefined) {
          ParseReact.Mutation.Destroy({
            className: this.props.parseClassName,
            objectId: selectedClasses[i]
          }).dispatch({batch: batch})
            .then(function(selectedClasses,index) {
              selectedClasses[index] = undefined;
            }.bind(this,selectedClasses,i));
        }
      }
      batch.dispatch().then(this.mutationDeleteSuccess,this.mutationDeleteError.bind(this,selectedClasses));
    }
  },

  handleAlertDismiss() {
    this.setState({ showAlert: false });
  },

  closeModal() {
    this.setState({ showModal: false });
  },

};

module.exports = tableMixin;
