var appID = 'george.games.thm.de';
var jsKey = 'N4%-_ACs=sAnZr[2';
var restKey = 'bBw(fk2/wVu3j#8X';
var serverUrlParse = 'http://george.mnd.thm.de:1337/parse';

var updateTime = 60000;

import Parse from 'parse/node'
import http from 'http'
import fstream from 'fstream'
import unzip from 'unzip'
import fs from 'fs'
import mkdirp from 'mkdirp'
import Minizip from 'node-minizip'

//init();

var pluginInstaller = React.createClass({
    init()
    {
        try {
            Parse.serverURL = serverUrlParse;
            Parse.initialize(appID, jsKey);
            checkUpdates();
            setInterval(checkUpdates, updateTime);
        }
        catch (err) {
            log('--->error in function: init error:' + err + '!');
        }
    },
    loadScript(url, callback)
    {
        // Adding the script tag to the head as suggested before
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    },
    downloadPluginFromParse(name, url, folder, plugin)
    {
        try {
            var file = fs.createWriteStream(name + '.zip');
            var request = http.get(url, function (response) {
                response.pipe(file);
                file.on('finish', function () {
                    unzipToFoler(name + ".zip", folder, plugin);
                });
            });
        }
        catch (err) {
            log('--->error in function: downloadPluginFromParse error:' + err + '!', plugin);
        }
    },
    checkInstalled(name, plugin)
    {
        try {
            log("Check if '" + name + "' id:'" + plugin.id + "' Plugin installed.");
            path = './plugin/' + name + '/'
            if (fs.existsSync(path)) {
                if (plugin.get('aktiv') == true) {
                    log("Check if is an update");
                    infos = openPluginInformations(name);

                    if (infos.VersionNumber >= plugin.get('versionNumber')) {
                        log("no update available!", plugin);
                    }
                    else {
                        log("Install Update", plugin);
                        var pluginUpdate = require(path + 'main.js');
                        pluginUpdate.update();
                    }

                }
                else {
                    log("Inaktives Plugin gefunden");
                    deleteFolder(path);
                }

            }
            else {
                if (plugin.get('aktiv') == true) {
                    log("Plugin not installed!");
                    path = './plugin/';
                    installPluginFromParse(plugin, path, name);
                }
                else {
                    log("Plugin inaktiv!", plugin);
                }
            }
        }
        catch (err) {
            log('--->error in function: checkInstalled error:' + err + '!');
        }
    },
    updatePluginInformationsToParse(plugin, name)
    {
        try {
            log("Update Plugin information");
            infos = openPluginInformations(name);

            if (infos != undefined) {
                plugin.set("author", infos.Autor);
                plugin.set("desc", infos.Desc);
                plugin.set("version", infos.Version);
                plugin.set("versionNumber", infos.VersionNumber);
                plugin.set("url", infos.url);
                plugin.set("name", infos.name);
            }

            plugin.save(null, {
                success: function (plugin) {
                    log("Plugin information Updated!");
                }
            });
        }
        catch (err) {
            log('--->error in function: updatePluginInformationsToParse error:' + err + '!', plugin);
        }
    },
    installPluginFromParse(plugin, folder, name)
    {
        try {
            log("Install Plugin '" + plugin.id + "'!", plugin);

            file = plugin.get('file');
            if (file == undefined) {
                log("Nothing to download!");
                return;
            }
            else {
                file = file.url();
                log("Download Plugin '" + file + "'!", plugin);
                downloadPluginFromParse('./download/' + plugin.id, file, folder, plugin);
            }

        }
        catch (err) {
            log('--->error in function: installPluginFromParse error:' + err + '!', plugin);
        }
    },
    checkUpdates()
    {
        try {
            log("Check for Upades in Plugin Table");

            var Plugins = Parse.Object.extend("Plugin");
            var query = new Parse.Query(Plugins);
            query.include('name');
            query.include('file');
            query.include('aktiv');
            query.find({
                success: function (results) {
                    log("Found " + results.length + " Plugins.");
                    // Do something with the returned Parse.Object values
                    for (var i = 0; i < results.length; i++) {
                        var currentPlugin = results[i];
                        name = currentPlugin.get('name');
                        checkInstalled(name, currentPlugin);
                    }
                },
                error: function (error) {
                    log("Error: " + error.code + " " + error.message);
                }
            });
        }
        catch (err) {
            log('--->error in function: checkUpdates error:' + err + '!');
        }
    },
    finishInstallation(path, name, plugin)
    {
        try {
            log('finish install plugin!');
            var PluginUpdate = require(path + name + '/main.js');
            pluginUpdate = new PluginUpdate();
            pluginUpdate.install();
            log("install success", plugin);
        }
        catch (err) {
            //installation konnte nicht abgeschlossen werden. Include Missing
            message = '--->error in function: finishInstallation error:' + err + '!';
            log(message, plugin);
            //deleteFolder(path+'/'+name);
        }
    },
    unzipToFoler(file, folder, plugin) {
        try {
            log('unzip ' + file + ' to ' + folder + '.');
            Minizip.unzip(file, folder, function (err) {
                if (err)
                    log(err, plugin);
                else {
                    log('unzip successfully.');
                    name = plugin.get('name');
                    finishInstallation(folder, name, plugin);
                    updatePluginInformationsToParse(plugin, name);
                    deleteFolderRecursive('./plugin/__MACOSX');
                    deleteFile(file);
                }
            });
        }
        catch (err) {
            message = '--->error in function: unzipToFoler error:' + err + '!';
            log(message, plugin);
        }
    },
    deleteFile(file)
    {
        try {
            const fs = require('fs');
            fs.unlinkSync(file);
            log(file + ' successfully deleted');
        }
        catch (err) {
            log('--->error in function: deleteFile error:' + err + '!');
        }
    },
    deleteFolder(folder)
    {
        try {
            deleteFolderRecursive(folder);
            log('folder ' + folder + ' successfully deleted');
        }
        catch (err) {
            log('--->error in function: deleteFolder error:' + err + '!');
        }
    },
    deleteFolderRecursive(path) {
        try {
            if (fs.existsSync(path)) {
                fs.readdirSync(path).forEach(function (file, index) {
                    var curPath = path + "/" + file;
                    if (fs.lstatSync(curPath).isDirectory()) { // recurse
                        deleteFolderRecursive(curPath);
                    } else { // delete file
                        fs.unlinkSync(curPath);
                    }
                });
                fs.rmdirSync(path);
            }
        }
        catch (err) {
            log('--->error in function: deleteFolderRecursive error:' + err + '!');
        }
    },
    openPluginInformations(name)
    {
        try {
            folder = './plugin/' + name + '/';
            file = 'information.json';

            var content = fs.readFileSync(folder + file);
            var jsonContent = JSON.parse(content);

            return jsonContent;
        }

        catch (err) {
            log('--->error in function: openPluginInformations error:' + err + '!');
        }
    },

    log(message, plugin)
    {
        if (plugin != undefined) {
            plugin.set("message", message);
            plugin.save();
        }
        console.log(message);
    },
    render() {
        return (
            <div className="pluginInstaller">
            </div>
        );
    }
});

module.exports = pluginInstaller;
