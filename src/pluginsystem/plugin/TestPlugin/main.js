import React from 'react';
import SmartInput from '../../../components/smart-input';
import {Panel, Input, Button} from 'react-bootstrap';

module.exports = function Testplugin() {
    this.install = function () {
        console.log('Test Plugin install start....');
    },
        this.update = function () {
            console.log('Test Plugin update start....');
        },
        this.remove = function () {
            console.log('Test Plugin remove start....');
        },
        this.renderSettings = function () {
            return (
                <div>
                    <h2>Daniels Testplugin Version 1.0</h2>
                    <p>Dieses Plugin stellt Funktionen zur erweiterung von George und 3S bereit.</p>
                    <p>Es kann mit folgender Syntax in George verwendet werden:</p>
                    <p>:-P</p>
                    <img src="http://www.giggeler-it.de/images/logo.png" alt="" width="345" height="122"></img>
                    <SmartInput
                        type="checkbox"
                        checked="false"
                        label="Aktiv"
                        checkLocalizedString={false}
                    />
                    <Button
                        bsStyle='default'
                        onClick={this.install}
                    >
                        Installieren
                    </Button>
                    <Button
                        bsStyle='default'
                        onClick={this.update}
                    >
                        Updaten
                    </Button>
                    <Button
                        bsStyle='default'
                        onClick={this.settingsSaveClick}
                    >
                        Konfiguration updaten
                    </Button>
                </div>);
        }
        this.settingsSaveClick = function () {

        }
}
