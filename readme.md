# G.E.O.R.G.E.-Admin-Tool
Das Administrationswerkzeug um Inhalte von G.E.O.R.G.E. (**G**eograpical **E**ducational **O**rientation **R**esearch **G**ame **E**ngine) zu verwalten.
## Anforderungen zum Weiterentwickeln
Um dieses Projekt weiterentwickeln zu können sind diverse Kenntnisse, Applikationen und NPM-Pakete erforderlich.
### Kenntnisse
- Parse
 - [Datenstruktur](https://dashboard.parse.com/apps/secret-science-society/browser/)
 - [JavaScript-SDK](https://www.parse.com/docs/js/guide)
- [React](http://facebook.github.io/react/docs/getting-started.html)
- [Parse-React](https://github.com/ParsePlatform/ParseReact)
- [NPM](https://docs.npmjs.com/)
- Git

### Applikationen
- [Node.js](https://nodejs.org/en/)
- [Git](https://git-scm.com/) bzw. eine Git-GUI (z. B. [GitHub-Desktop](https://desktop.github.com/))
- JavaScript-IDE bzw. -Editor (z. B. [Atom](https://atom.io/))
 
### Plugin-System
- [Konzept](https://gitlab.com/GamesTHM/G.E.O.R.G.E.-Admin-Tool/wikis/plugin-concept)
- [Beispiel Plugin]()

### NPM
NPM wird mit Node.js installiert, bekommt aber häufiger Updates, weshalb man es auch direkt nach der Installation von Node.js updaten sollte.
#### NPM updaten
1. NPM-Version vorher und nachher prüfen mit `npm -v`
1. Update durchführen mit `sudo npm install -g npm` bzw. `npm i -g npm`
1. Wenn die Version vorher und nachher gleich ist, siehe [hier](https://github.com/npm/npm/wiki/Troubleshooting)

#### Lokale Pakete installieren
Um das Projekt bauen zu können, werden noch einige lokale Pakete benötigt. Welche das genau sind, kann in der **package.json** eingesehen werden.  
Diese können mit `npm install` oder `npm i` installiert werden.

## Projekt-Struktur
Das Projekt wird aufgrund der Verwendung von *React* komponentenweise entwickelt. Um die Entwicklung zu vereinfachen wurde *JSX* verwendet. *JSX* ist eine JavaScript-Syntax-Erweiterung, mit der man die Komponenten im *JavaScript*-Code *HTML*-ähnlich als Tags schreiben und ineinander verschachteln kann. Der in *JSX* geschriebene Code wird mittels *webpack* bzw. der **webpack.config.js** in *JS*-Code transpiliert.  
Für jede Verwaltungsseite gibt es eine Tabellen- und eine Formular-Komponente. Die Komponenten wurden nach dem Schema **\*-table.js** und **\*-form.js** benannt und liegen im **src/route-components** Ordner. Die Tabellen-Komponenten verwenden primär die class-table Komponente und stellen Controller-Funktionalitäten, wie das Vorbereiten der Tabellendaten und das Löschen von Einträgen, bereit. Die Formular-Komponenten verwenden primär die entsprechende Komponente aus dem Ordner **src/views** und stellen Controller-Funktionalitäten, wie das Laden und Speichern von Datensätzen, bereit. Sie sind wiederum aus Komponenten, wie z. B.  der Seitenleiste und den Eingabekomponenten aufgebaut. Diese Komponenten befinden sich im **src/components/** Ordner.  
Da die Hauptseiten mehrere gemeinsame Funktionalitäten haben, wurde für diese ein sogenanntes Mixin entwickelt, welches von jeder Hauptseite eingebunden wird: Das **MainPageMixin**, welches im Ordner **src/mixins/** liegt. Ein Mixin ist eine Spezialität von *React*, mit der man React-Komponenten um Funktionalitäten (Methoden) erweitern kann.  

## Development- und Production-Server
**TL; DR: **  
Development-Server starten:  
Windows:  
`set NODE_ENV=&&npm start`  
Linux:  
`env NODE_ENV= npm start`  

Production-Server starten:  
Windows:  
`set NODE_ENV=production&&npm start`  
Linux:  
`env NODE_ENV=production npm start`

Um dieses Projekt weiterentwickeln und testen zu können wird mittels den Paketen *webpack*, *webpack-dev-server* und *http-server* und entsprechenden Konfigurationen in **package.json** und **webpack.config.js**  mit *webpack* ein Bundle aus allen benötigten Komponenten und NPM-Modulen generiert und ein lokaler Development-Server gestartet.  
Um ein Bundle generieren zu können, braucht *webpack* eine Root-Komponente. Die Root-Komponente dieses Projekts ist **app.js** und befindet sich direkt im **src/** Ordner.
Speichert man Änderungen an den Komponenten wird das Bundle automatisch neu generiert und vom Server geladen.  
Um den Development-Server (*webpack-dev-server*) zu starten sollte folgender Befehl verwendet werden:  
Windows:  
`set NODE_ENV=&&npm start`  
Linux:  
`env NODE_ENV= npm start`  
Durch `set NODE_ENV=` wird sichergestellt, dass die Umgebungsvariable NODE_ENV wieder gelöscht wird, für den Fall, dass sie durch das Starten des Production-Servers auf 'production' gesetzt wurde.  
Der Production-Server wird dementsprechend wie folgt gestartet:  
Windows:  
`set NODE_ENV=production&&npm start`  
Linux:  
`env NODE_ENV=production npm start`  
Nach einem Deployment dieses Projekts sollte auf dem entsprechenden Server dieser Befehl ausgeführt werden, um das Admin-Tool live zu schalten.
### Wichtig!
Beim ersten Deployment muss vorher noch der Befehl `npm install` ausgeführt werden, um die benötigten NPM-Pakete installieren zu lassen. Außerdem muss einmalig nach dem Ausführen dieses Befehls in der Datei **node_modules/parse-react/lib/browser/MutationBatch.js** die Zeile 71 ersetzt werden:  
Alter Code:
```js
var copy = { method: req.method, path: '/1/' + req.path };
```
Neuer Code:
```js
var copy = { method: req.method, path: '/parse/' + req.path };
```
Grund dafür ist, dass sich der Pfad zur Parse-REST-API durch den Umzug auf einen eigenen Parse-Server geändert hat.
